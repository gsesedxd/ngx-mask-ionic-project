/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { ElementRef, Inject, Injectable, Renderer2 } from '@angular/core';
import { config } from './config';
import { DOCUMENT } from '@angular/common';
import { MaskApplierService } from './mask-applier.service';
import { NgControl } from '@angular/forms';
export class MaskService extends MaskApplierService {
    /**
     * @param {?} document
     * @param {?} _config
     * @param {?} _elementRef
     * @param {?} _renderer
     * @param {?} _ngControl
     */
    constructor(document, _config, _elementRef, _renderer, _ngControl) {
        super(_config);
        this.document = document;
        this._config = _config;
        this._elementRef = _elementRef;
        this._renderer = _renderer;
        this._ngControl = _ngControl;
        this.maskExpression = '';
        this.isNumberValue = false;
        this.showMaskTyped = false;
        this.maskIsShown = '';
        this.onTouch = () => { };
        this.setFormElement(_elementRef.nativeElement);
        setTimeout(() => {
            if (this._formElement.localName !== 'input') {
                /** @type {?} */
                const inputEl = this._elementRef.nativeElement.querySelector('input');
                if (inputEl != null) {
                    this.setFormElement(inputEl);
                }
                else {
                    console.warn('mask-service: Could not find Input Element.  Please make sure one is present.');
                }
            }
            this._ngControl.valueChanges.subscribe((value) => {
                this._onControlValueChange(value);
            });
        });
    }
    /**
     * @param {?} el
     * @return {?}
     */
    setFormElement(el) {
        this._formElement = el;
    }
    /**
     * @param {?} inputValue
     * @param {?} maskExpression
     * @param {?=} position
     * @param {?=} cb
     * @return {?}
     */
    applyMask(inputValue, maskExpression, position = 0, cb = () => { }) {
        this.maskIsShown = this.showMaskTyped
            ? this.maskExpression.replace(/[0-9]/g, '_')
            : '';
        if (!inputValue && this.showMaskTyped) {
            return this.prefix + this.maskIsShown;
        }
        /** @type {?} */
        const result = super.applyMask(inputValue, maskExpression, position, cb);
        this.unmaskedValue = this.getUnmaskedValue(result);
        return this._applyMaskResult(result);
    }
    /**
     * @param {?=} position
     * @param {?=} cb
     * @return {?}
     */
    applyValueChanges(position = 0, cb = () => { }) {
        /** @type {?} */
        const maskedInput = this.applyMask(this._formElement.value, this.maskExpression, position, cb);
        this._formElement.value = maskedInput;
        if (this._formElement === this.document.activeElement) {
            return;
        }
        this.clearIfNotMatchFn();
    }
    /**
     * @return {?}
     */
    showMaskInInput() {
        if (this.showMaskTyped) {
            this.maskIsShown = this.maskExpression.replace(/[0-9]/g, '_');
        }
    }
    /**
     * @return {?}
     */
    clearIfNotMatchFn() {
        if (this.clearIfNotMatch === true &&
            this.maskExpression.length !== this._formElement.value.length) {
            this.setValue('');
            this.applyMask(this._formElement.value, this.maskExpression);
        }
    }
    /**
     * @param {?} value
     * @return {?}
     */
    setValue(value) {
        this.unmaskedValue = this.getUnmaskedValue(value);
        this._ngControl.control.setValue(value);
    }
    /**
     * @param {?} name
     * @param {?} value
     * @return {?}
     */
    setFormElementProperty(name, value) {
        if (this._formElement) {
            this._renderer.setProperty(this._formElement, name, value);
        }
    }
    /**
     * @param {?} result
     * @return {?}
     */
    getUnmaskedValue(result) {
        /** @type {?} */
        const resultNoSuffixOrPrefix = this._removeSufix(this._removePrefix(result));
        /** @type {?} */
        let changeValue = resultNoSuffixOrPrefix;
        if (Array.isArray(this.dropSpecialCharacters)) {
            changeValue = this._removeMask(resultNoSuffixOrPrefix, this.dropSpecialCharacters);
        }
        else if (this.dropSpecialCharacters) {
            changeValue = this._removeMask(resultNoSuffixOrPrefix, this.maskSpecialCharacters);
            changeValue = this.isNumberValue ? Number(changeValue) : changeValue;
        }
        return changeValue;
    }
    /**
     * @private
     * @param {?} value
     * @param {?} specialCharactersForRemove
     * @return {?}
     */
    _removeMask(value, specialCharactersForRemove) {
        return value
            ? value.replace(this._regExpForRemove(specialCharactersForRemove), '')
            : value;
    }
    /**
     * @private
     * @param {?} value
     * @return {?}
     */
    _removePrefix(value) {
        if (!this.prefix) {
            return value;
        }
        return value ? value.replace(this.prefix, '') : value;
    }
    /**
     * @private
     * @param {?} value
     * @return {?}
     */
    _removeSufix(value) {
        if (!this.sufix) {
            return value;
        }
        return value ? value.replace(this.sufix, '') : value;
    }
    /**
     * @private
     * @param {?} specialCharactersForRemove
     * @return {?}
     */
    _regExpForRemove(specialCharactersForRemove) {
        return new RegExp(specialCharactersForRemove.map((item) => `\\${item}`).join('|'), 'gi');
    }
    /**
     * @private
     * @param {?} result
     * @return {?}
     */
    _applyMaskResult(result) {
        if (!this.showMaskTyped) {
            return result;
        }
        /** @type {?} */
        const resLen = result.length;
        /** @type {?} */
        const prefNmask = this.prefix + this.maskIsShown;
        /** @type {?} */
        const ifMaskIsShown = prefNmask.slice(resLen);
        return result + ifMaskIsShown;
    }
    /**
     * @private
     * @param {?} value
     * @return {?}
     */
    _onControlValueChange(value) {
        /*
              Because we are no longer working with the ControlValueAccessor (since it doesn't play nice with Ionic).
              We need logic here to track changes made programmatically to the form value.  Specifically changes
              done OUTSIDE of the mask. Since changes done inside the mask may also fire off this method
              we need to do some jiu jitsu to ensure we are ignoring those changes.
            */
        /** @type {?} */
        const newValue = this.getUnmaskedValue(value);
        if (this.unmaskedValue === newValue) {
            return;
        }
        /** @type {?} */
        let unmaskedSubstring = null;
        // This method (value change) fires off before a Keydown or Input event, so we need to subtract
        // the latest change and compare to our previous (known) value.
        if (this.unmaskedValue != null) {
            /** @type {?} */
            const v = this.unmaskedValue.toString();
            unmaskedSubstring = v.substring(0, v.length - 1);
        }
        if (newValue !== unmaskedSubstring) {
            /** @type {?} */
            const nv = newValue != null ? newValue.toString() : null;
            /** @type {?} */
            const v = this.applyMask(nv, this.maskExpression);
            this.setValue(v);
        }
    }
}
MaskService.decorators = [
    { type: Injectable }
];
/** @nocollapse */
MaskService.ctorParameters = () => [
    { type: undefined, decorators: [{ type: Inject, args: [DOCUMENT,] }] },
    { type: undefined, decorators: [{ type: Inject, args: [config,] }] },
    { type: ElementRef },
    { type: Renderer2 },
    { type: NgControl }
];
if (false) {
    /** @type {?} */
    MaskService.prototype.maskExpression;
    /** @type {?} */
    MaskService.prototype.isNumberValue;
    /** @type {?} */
    MaskService.prototype.showMaskTyped;
    /** @type {?} */
    MaskService.prototype.maskIsShown;
    /**
     * @type {?}
     * @private
     */
    MaskService.prototype._formElement;
    /**
     * @type {?}
     * @private
     */
    MaskService.prototype.unmaskedValue;
    /** @type {?} */
    MaskService.prototype.onTouch;
    /**
     * @type {?}
     * @private
     */
    MaskService.prototype.document;
    /**
     * @type {?}
     * @protected
     */
    MaskService.prototype._config;
    /**
     * @type {?}
     * @private
     */
    MaskService.prototype._elementRef;
    /**
     * @type {?}
     * @private
     */
    MaskService.prototype._renderer;
    /**
     * @type {?}
     * @private
     */
    MaskService.prototype._ngControl;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWFzay5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbmd4LW1hc2staW9uaWMvIiwic291cmNlcyI6WyJsaWIvbWFzay5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sRUFBRSxVQUFVLEVBQUUsU0FBUyxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzFFLE9BQU8sRUFBRSxNQUFNLEVBQVcsTUFBTSxVQUFVLENBQUM7QUFDM0MsT0FBTyxFQUFFLFFBQVEsRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBQzNDLE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLHdCQUF3QixDQUFDO0FBQzVELE9BQU8sRUFBRSxTQUFTLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUczQyxNQUFNLE9BQU8sV0FBWSxTQUFRLGtCQUFrQjs7Ozs7Ozs7SUFRakQsWUFFNEIsUUFBYSxFQUNiLE9BQWdCLEVBQ2xDLFdBQXVCLEVBQ3ZCLFNBQW9CLEVBQ3BCLFVBQXFCO1FBRTdCLEtBQUssQ0FBQyxPQUFPLENBQUMsQ0FBQztRQU5XLGFBQVEsR0FBUixRQUFRLENBQUs7UUFDYixZQUFPLEdBQVAsT0FBTyxDQUFTO1FBQ2xDLGdCQUFXLEdBQVgsV0FBVyxDQUFZO1FBQ3ZCLGNBQVMsR0FBVCxTQUFTLENBQVc7UUFDcEIsZUFBVSxHQUFWLFVBQVUsQ0FBVztRQWJ4QixtQkFBYyxHQUFHLEVBQUUsQ0FBQztRQUNwQixrQkFBYSxHQUFHLEtBQUssQ0FBQztRQUN0QixrQkFBYSxHQUFHLEtBQUssQ0FBQztRQUN0QixnQkFBVyxHQUFHLEVBQUUsQ0FBQztRQUdqQixZQUFPLEdBQUcsR0FBRyxFQUFFLEdBQUUsQ0FBQyxDQUFDO1FBVXhCLElBQUksQ0FBQyxjQUFjLENBQUMsV0FBVyxDQUFDLGFBQWEsQ0FBQyxDQUFDO1FBRS9DLFVBQVUsQ0FBQyxHQUFHLEVBQUU7WUFDZCxJQUFJLElBQUksQ0FBQyxZQUFZLENBQUMsU0FBUyxLQUFLLE9BQU8sRUFBRTs7c0JBQ3JDLE9BQU8sR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLGFBQWEsQ0FBQyxhQUFhLENBQUMsT0FBTyxDQUFDO2dCQUNyRSxJQUFJLE9BQU8sSUFBSSxJQUFJLEVBQUU7b0JBQ25CLElBQUksQ0FBQyxjQUFjLENBQUMsT0FBTyxDQUFDLENBQUM7aUJBQzlCO3FCQUFNO29CQUNMLE9BQU8sQ0FBQyxJQUFJLENBQ1YsK0VBQStFLENBQ2hGLENBQUM7aUJBQ0g7YUFDRjtZQUVELElBQUksQ0FBQyxVQUFVLENBQUMsWUFBWSxDQUFDLFNBQVMsQ0FBQyxDQUFDLEtBQWEsRUFBRSxFQUFFO2dCQUN2RCxJQUFJLENBQUMscUJBQXFCLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDcEMsQ0FBQyxDQUFDLENBQUM7UUFDTCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7Ozs7O0lBRU0sY0FBYyxDQUFDLEVBQW9CO1FBQ3hDLElBQUksQ0FBQyxZQUFZLEdBQUcsRUFBRSxDQUFDO0lBQ3pCLENBQUM7Ozs7Ozs7O0lBRU0sU0FBUyxDQUNkLFVBQWtCLEVBQ2xCLGNBQXNCLEVBQ3RCLFdBQW1CLENBQUMsRUFDcEIsS0FBZSxHQUFHLEVBQUUsR0FBRSxDQUFDO1FBRXZCLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLGFBQWE7WUFDbkMsQ0FBQyxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsT0FBTyxDQUFDLFFBQVEsRUFBRSxHQUFHLENBQUM7WUFDNUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztRQUNQLElBQUksQ0FBQyxVQUFVLElBQUksSUFBSSxDQUFDLGFBQWEsRUFBRTtZQUNyQyxPQUFPLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQztTQUN2Qzs7Y0FFSyxNQUFNLEdBQUcsS0FBSyxDQUFDLFNBQVMsQ0FBQyxVQUFVLEVBQUUsY0FBYyxFQUFFLFFBQVEsRUFBRSxFQUFFLENBQUM7UUFDeEUsSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsTUFBTSxDQUFDLENBQUM7UUFFbkQsT0FBTyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDdkMsQ0FBQzs7Ozs7O0lBRU0saUJBQWlCLENBQ3RCLFdBQW1CLENBQUMsRUFDcEIsS0FBZSxHQUFHLEVBQUUsR0FBRSxDQUFDOztjQUVqQixXQUFXLEdBQW9CLElBQUksQ0FBQyxTQUFTLENBQ2pELElBQUksQ0FBQyxZQUFZLENBQUMsS0FBSyxFQUN2QixJQUFJLENBQUMsY0FBYyxFQUNuQixRQUFRLEVBQ1IsRUFBRSxDQUNIO1FBQ0QsSUFBSSxDQUFDLFlBQVksQ0FBQyxLQUFLLEdBQUcsV0FBVyxDQUFDO1FBQ3RDLElBQUksSUFBSSxDQUFDLFlBQVksS0FBSyxJQUFJLENBQUMsUUFBUSxDQUFDLGFBQWEsRUFBRTtZQUNyRCxPQUFPO1NBQ1I7UUFDRCxJQUFJLENBQUMsaUJBQWlCLEVBQUUsQ0FBQztJQUMzQixDQUFDOzs7O0lBRU0sZUFBZTtRQUNwQixJQUFJLElBQUksQ0FBQyxhQUFhLEVBQUU7WUFDdEIsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLE9BQU8sQ0FBQyxRQUFRLEVBQUUsR0FBRyxDQUFDLENBQUM7U0FDL0Q7SUFDSCxDQUFDOzs7O0lBRU0saUJBQWlCO1FBQ3RCLE9BQU8sQ0FBQyxHQUFHLENBQUMsc0JBQXNCLENBQUMsQ0FBQztRQUNwQyxJQUNFLElBQUksQ0FBQyxlQUFlLEtBQUssSUFBSTtZQUM3QixJQUFJLENBQUMsY0FBYyxDQUFDLE1BQU0sS0FBSyxJQUFJLENBQUMsWUFBWSxDQUFDLEtBQUssQ0FBQyxNQUFNLEVBQzdEO1lBQ0EsSUFBSSxDQUFDLFFBQVEsQ0FBQyxFQUFFLENBQUMsQ0FBQztZQUNsQixJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQztTQUM5RDtJQUNILENBQUM7Ozs7O0lBRU0sUUFBUSxDQUFDLEtBQWE7UUFDM0IsSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDbEQsSUFBSSxDQUFDLFVBQVUsQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQzFDLENBQUM7Ozs7OztJQUNNLHNCQUFzQixDQUFDLElBQVksRUFBRSxLQUF1QjtRQUNqRSxJQUFJLElBQUksQ0FBQyxZQUFZLEVBQUU7WUFDckIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFlBQVksRUFBRSxJQUFJLEVBQUUsS0FBSyxDQUFDLENBQUM7U0FDNUQ7SUFDSCxDQUFDOzs7OztJQUVNLGdCQUFnQixDQUFDLE1BQWM7O2NBQzlCLHNCQUFzQixHQUFHLElBQUksQ0FBQyxZQUFZLENBQzlDLElBQUksQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFDLENBQzNCOztZQUNHLFdBQVcsR0FBb0Isc0JBQXNCO1FBRXpELElBQUksS0FBSyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMscUJBQXFCLENBQUMsRUFBRTtZQUM3QyxXQUFXLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FDNUIsc0JBQXNCLEVBQ3RCLElBQUksQ0FBQyxxQkFBcUIsQ0FDM0IsQ0FBQztTQUNIO2FBQU0sSUFBSSxJQUFJLENBQUMscUJBQXFCLEVBQUU7WUFDckMsV0FBVyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQzVCLHNCQUFzQixFQUN0QixJQUFJLENBQUMscUJBQXFCLENBQzNCLENBQUM7WUFDRixXQUFXLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxXQUFXLENBQUM7U0FDdEU7UUFFRCxPQUFPLFdBQVcsQ0FBQztJQUNyQixDQUFDOzs7Ozs7O0lBRU8sV0FBVyxDQUNqQixLQUFhLEVBQ2IsMEJBQW9DO1FBRXBDLE9BQU8sS0FBSztZQUNWLENBQUMsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQywwQkFBMEIsQ0FBQyxFQUFFLEVBQUUsQ0FBQztZQUN0RSxDQUFDLENBQUMsS0FBSyxDQUFDO0lBQ1osQ0FBQzs7Ozs7O0lBRU8sYUFBYSxDQUFDLEtBQWE7UUFDakMsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUU7WUFDaEIsT0FBTyxLQUFLLENBQUM7U0FDZDtRQUNELE9BQU8sS0FBSyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQztJQUN4RCxDQUFDOzs7Ozs7SUFFTyxZQUFZLENBQUMsS0FBYTtRQUNoQyxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssRUFBRTtZQUNmLE9BQU8sS0FBSyxDQUFDO1NBQ2Q7UUFDRCxPQUFPLEtBQUssQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsS0FBSyxFQUFFLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7SUFDdkQsQ0FBQzs7Ozs7O0lBRU8sZ0JBQWdCLENBQUMsMEJBQW9DO1FBQzNELE9BQU8sSUFBSSxNQUFNLENBQ2YsMEJBQTBCLENBQUMsR0FBRyxDQUFDLENBQUMsSUFBWSxFQUFFLEVBQUUsQ0FBQyxLQUFLLElBQUksRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUN2RSxJQUFJLENBQ0wsQ0FBQztJQUNKLENBQUM7Ozs7OztJQUVPLGdCQUFnQixDQUFDLE1BQWM7UUFDckMsSUFBSSxDQUFDLElBQUksQ0FBQyxhQUFhLEVBQUU7WUFDdkIsT0FBTyxNQUFNLENBQUM7U0FDZjs7Y0FDSyxNQUFNLEdBQVcsTUFBTSxDQUFDLE1BQU07O2NBQzlCLFNBQVMsR0FBVyxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxXQUFXOztjQUNsRCxhQUFhLEdBQUcsU0FBUyxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUM7UUFFN0MsT0FBTyxNQUFNLEdBQUcsYUFBYSxDQUFDO0lBQ2hDLENBQUM7Ozs7OztJQUVPLHFCQUFxQixDQUFDLEtBQWE7Ozs7Ozs7O2NBT25DLFFBQVEsR0FBRyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsS0FBSyxDQUFDO1FBRTdDLElBQUksSUFBSSxDQUFDLGFBQWEsS0FBSyxRQUFRLEVBQUU7WUFDbkMsT0FBTztTQUNSOztZQUVHLGlCQUFpQixHQUFXLElBQUk7UUFFcEMsK0ZBQStGO1FBQy9GLCtEQUErRDtRQUMvRCxJQUFJLElBQUksQ0FBQyxhQUFhLElBQUksSUFBSSxFQUFFOztrQkFDeEIsQ0FBQyxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsUUFBUSxFQUFFO1lBQ3ZDLGlCQUFpQixHQUFHLENBQUMsQ0FBQyxTQUFTLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUM7U0FDbEQ7UUFFRCxJQUFJLFFBQVEsS0FBSyxpQkFBaUIsRUFBRTs7a0JBQzVCLEVBQUUsR0FBRyxRQUFRLElBQUksSUFBSSxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsUUFBUSxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUk7O2tCQUNsRCxDQUFDLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxFQUFFLEVBQUUsSUFBSSxDQUFDLGNBQWMsQ0FBQztZQUNqRCxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDO1NBQ2xCO0lBQ0gsQ0FBQzs7O1lBbk1GLFVBQVU7Ozs7NENBV04sTUFBTSxTQUFDLFFBQVE7NENBQ2YsTUFBTSxTQUFDLE1BQU07WUFsQlQsVUFBVTtZQUFzQixTQUFTO1lBSXpDLFNBQVM7Ozs7SUFJaEIscUNBQTJCOztJQUMzQixvQ0FBNkI7O0lBQzdCLG9DQUE2Qjs7SUFDN0Isa0NBQXdCOzs7OztJQUN4QixtQ0FBdUM7Ozs7O0lBQ3ZDLG9DQUF1Qzs7SUFDdkMsOEJBQTBCOzs7OztJQUd4QiwrQkFBdUM7Ozs7O0lBQ3ZDLDhCQUEwQzs7Ozs7SUFDMUMsa0NBQStCOzs7OztJQUMvQixnQ0FBNEI7Ozs7O0lBQzVCLGlDQUE2QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEVsZW1lbnRSZWYsIEluamVjdCwgSW5qZWN0YWJsZSwgUmVuZGVyZXIyIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IGNvbmZpZywgSUNvbmZpZyB9IGZyb20gJy4vY29uZmlnJztcclxuaW1wb3J0IHsgRE9DVU1FTlQgfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xyXG5pbXBvcnQgeyBNYXNrQXBwbGllclNlcnZpY2UgfSBmcm9tICcuL21hc2stYXBwbGllci5zZXJ2aWNlJztcclxuaW1wb3J0IHsgTmdDb250cm9sIH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xyXG5cclxuQEluamVjdGFibGUoKVxyXG5leHBvcnQgY2xhc3MgTWFza1NlcnZpY2UgZXh0ZW5kcyBNYXNrQXBwbGllclNlcnZpY2Uge1xyXG4gIHB1YmxpYyBtYXNrRXhwcmVzc2lvbiA9ICcnO1xyXG4gIHB1YmxpYyBpc051bWJlclZhbHVlID0gZmFsc2U7XHJcbiAgcHVibGljIHNob3dNYXNrVHlwZWQgPSBmYWxzZTtcclxuICBwdWJsaWMgbWFza0lzU2hvd24gPSAnJztcclxuICBwcml2YXRlIF9mb3JtRWxlbWVudDogSFRNTElucHV0RWxlbWVudDtcclxuICBwcml2YXRlIHVubWFza2VkVmFsdWU6IHN0cmluZyB8IG51bWJlcjtcclxuICBwdWJsaWMgb25Ub3VjaCA9ICgpID0+IHt9O1xyXG4gIHB1YmxpYyBjb25zdHJ1Y3RvcihcclxuICAgIC8vIHRzbGludDpkaXNhYmxlLW5leHQtbGluZVxyXG4gICAgQEluamVjdChET0NVTUVOVCkgcHJpdmF0ZSBkb2N1bWVudDogYW55LFxyXG4gICAgQEluamVjdChjb25maWcpIHByb3RlY3RlZCBfY29uZmlnOiBJQ29uZmlnLFxyXG4gICAgcHJpdmF0ZSBfZWxlbWVudFJlZjogRWxlbWVudFJlZixcclxuICAgIHByaXZhdGUgX3JlbmRlcmVyOiBSZW5kZXJlcjIsXHJcbiAgICBwcml2YXRlIF9uZ0NvbnRyb2w6IE5nQ29udHJvbFxyXG4gICkge1xyXG4gICAgc3VwZXIoX2NvbmZpZyk7XHJcbiAgICB0aGlzLnNldEZvcm1FbGVtZW50KF9lbGVtZW50UmVmLm5hdGl2ZUVsZW1lbnQpO1xyXG5cclxuICAgIHNldFRpbWVvdXQoKCkgPT4ge1xyXG4gICAgICBpZiAodGhpcy5fZm9ybUVsZW1lbnQubG9jYWxOYW1lICE9PSAnaW5wdXQnKSB7XHJcbiAgICAgICAgY29uc3QgaW5wdXRFbCA9IHRoaXMuX2VsZW1lbnRSZWYubmF0aXZlRWxlbWVudC5xdWVyeVNlbGVjdG9yKCdpbnB1dCcpO1xyXG4gICAgICAgIGlmIChpbnB1dEVsICE9IG51bGwpIHtcclxuICAgICAgICAgIHRoaXMuc2V0Rm9ybUVsZW1lbnQoaW5wdXRFbCk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgIGNvbnNvbGUud2FybihcclxuICAgICAgICAgICAgJ21hc2stc2VydmljZTogQ291bGQgbm90IGZpbmQgSW5wdXQgRWxlbWVudC4gIFBsZWFzZSBtYWtlIHN1cmUgb25lIGlzIHByZXNlbnQuJ1xyXG4gICAgICAgICAgKTtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIHRoaXMuX25nQ29udHJvbC52YWx1ZUNoYW5nZXMuc3Vic2NyaWJlKCh2YWx1ZTogc3RyaW5nKSA9PiB7XHJcbiAgICAgICAgdGhpcy5fb25Db250cm9sVmFsdWVDaGFuZ2UodmFsdWUpO1xyXG4gICAgICB9KTtcclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgcHVibGljIHNldEZvcm1FbGVtZW50KGVsOiBIVE1MSW5wdXRFbGVtZW50KSB7XHJcbiAgICB0aGlzLl9mb3JtRWxlbWVudCA9IGVsO1xyXG4gIH1cclxuXHJcbiAgcHVibGljIGFwcGx5TWFzayhcclxuICAgIGlucHV0VmFsdWU6IHN0cmluZyxcclxuICAgIG1hc2tFeHByZXNzaW9uOiBzdHJpbmcsXHJcbiAgICBwb3NpdGlvbjogbnVtYmVyID0gMCxcclxuICAgIGNiOiBGdW5jdGlvbiA9ICgpID0+IHt9XHJcbiAgKTogc3RyaW5nIHtcclxuICAgIHRoaXMubWFza0lzU2hvd24gPSB0aGlzLnNob3dNYXNrVHlwZWRcclxuICAgICAgPyB0aGlzLm1hc2tFeHByZXNzaW9uLnJlcGxhY2UoL1swLTldL2csICdfJylcclxuICAgICAgOiAnJztcclxuICAgIGlmICghaW5wdXRWYWx1ZSAmJiB0aGlzLnNob3dNYXNrVHlwZWQpIHtcclxuICAgICAgcmV0dXJuIHRoaXMucHJlZml4ICsgdGhpcy5tYXNrSXNTaG93bjtcclxuICAgIH1cclxuXHJcbiAgICBjb25zdCByZXN1bHQgPSBzdXBlci5hcHBseU1hc2soaW5wdXRWYWx1ZSwgbWFza0V4cHJlc3Npb24sIHBvc2l0aW9uLCBjYik7XHJcbiAgICB0aGlzLnVubWFza2VkVmFsdWUgPSB0aGlzLmdldFVubWFza2VkVmFsdWUocmVzdWx0KTtcclxuXHJcbiAgICByZXR1cm4gdGhpcy5fYXBwbHlNYXNrUmVzdWx0KHJlc3VsdCk7XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgYXBwbHlWYWx1ZUNoYW5nZXMoXHJcbiAgICBwb3NpdGlvbjogbnVtYmVyID0gMCxcclxuICAgIGNiOiBGdW5jdGlvbiA9ICgpID0+IHt9XHJcbiAgKTogdm9pZCB7XHJcbiAgICBjb25zdCBtYXNrZWRJbnB1dDogc3RyaW5nIHwgbnVtYmVyID0gdGhpcy5hcHBseU1hc2soXHJcbiAgICAgIHRoaXMuX2Zvcm1FbGVtZW50LnZhbHVlLFxyXG4gICAgICB0aGlzLm1hc2tFeHByZXNzaW9uLFxyXG4gICAgICBwb3NpdGlvbixcclxuICAgICAgY2JcclxuICAgICk7XHJcbiAgICB0aGlzLl9mb3JtRWxlbWVudC52YWx1ZSA9IG1hc2tlZElucHV0O1xyXG4gICAgaWYgKHRoaXMuX2Zvcm1FbGVtZW50ID09PSB0aGlzLmRvY3VtZW50LmFjdGl2ZUVsZW1lbnQpIHtcclxuICAgICAgcmV0dXJuO1xyXG4gICAgfVxyXG4gICAgdGhpcy5jbGVhcklmTm90TWF0Y2hGbigpO1xyXG4gIH1cclxuXHJcbiAgcHVibGljIHNob3dNYXNrSW5JbnB1dCgpOiB2b2lkIHtcclxuICAgIGlmICh0aGlzLnNob3dNYXNrVHlwZWQpIHtcclxuICAgICAgdGhpcy5tYXNrSXNTaG93biA9IHRoaXMubWFza0V4cHJlc3Npb24ucmVwbGFjZSgvWzAtOV0vZywgJ18nKTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIHB1YmxpYyBjbGVhcklmTm90TWF0Y2hGbigpOiB2b2lkIHtcclxuICAgIGNvbnNvbGUubG9nKCdjbGVhci1pZi1ub3QtbWF0Y2hlZCcpO1xyXG4gICAgaWYgKFxyXG4gICAgICB0aGlzLmNsZWFySWZOb3RNYXRjaCA9PT0gdHJ1ZSAmJlxyXG4gICAgICB0aGlzLm1hc2tFeHByZXNzaW9uLmxlbmd0aCAhPT0gdGhpcy5fZm9ybUVsZW1lbnQudmFsdWUubGVuZ3RoXHJcbiAgICApIHtcclxuICAgICAgdGhpcy5zZXRWYWx1ZSgnJyk7XHJcbiAgICAgIHRoaXMuYXBwbHlNYXNrKHRoaXMuX2Zvcm1FbGVtZW50LnZhbHVlLCB0aGlzLm1hc2tFeHByZXNzaW9uKTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIHB1YmxpYyBzZXRWYWx1ZSh2YWx1ZTogc3RyaW5nKSB7XHJcbiAgICB0aGlzLnVubWFza2VkVmFsdWUgPSB0aGlzLmdldFVubWFza2VkVmFsdWUodmFsdWUpO1xyXG4gICAgdGhpcy5fbmdDb250cm9sLmNvbnRyb2wuc2V0VmFsdWUodmFsdWUpO1xyXG4gIH1cclxuICBwdWJsaWMgc2V0Rm9ybUVsZW1lbnRQcm9wZXJ0eShuYW1lOiBzdHJpbmcsIHZhbHVlOiBzdHJpbmcgfCBib29sZWFuKSB7XHJcbiAgICBpZiAodGhpcy5fZm9ybUVsZW1lbnQpIHtcclxuICAgICAgdGhpcy5fcmVuZGVyZXIuc2V0UHJvcGVydHkodGhpcy5fZm9ybUVsZW1lbnQsIG5hbWUsIHZhbHVlKTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIHB1YmxpYyBnZXRVbm1hc2tlZFZhbHVlKHJlc3VsdDogc3RyaW5nKTogc3RyaW5nIHwgbnVtYmVyIHtcclxuICAgIGNvbnN0IHJlc3VsdE5vU3VmZml4T3JQcmVmaXggPSB0aGlzLl9yZW1vdmVTdWZpeChcclxuICAgICAgdGhpcy5fcmVtb3ZlUHJlZml4KHJlc3VsdClcclxuICAgICk7XHJcbiAgICBsZXQgY2hhbmdlVmFsdWU6IHN0cmluZyB8IG51bWJlciA9IHJlc3VsdE5vU3VmZml4T3JQcmVmaXg7XHJcblxyXG4gICAgaWYgKEFycmF5LmlzQXJyYXkodGhpcy5kcm9wU3BlY2lhbENoYXJhY3RlcnMpKSB7XHJcbiAgICAgIGNoYW5nZVZhbHVlID0gdGhpcy5fcmVtb3ZlTWFzayhcclxuICAgICAgICByZXN1bHROb1N1ZmZpeE9yUHJlZml4LFxyXG4gICAgICAgIHRoaXMuZHJvcFNwZWNpYWxDaGFyYWN0ZXJzXHJcbiAgICAgICk7XHJcbiAgICB9IGVsc2UgaWYgKHRoaXMuZHJvcFNwZWNpYWxDaGFyYWN0ZXJzKSB7XHJcbiAgICAgIGNoYW5nZVZhbHVlID0gdGhpcy5fcmVtb3ZlTWFzayhcclxuICAgICAgICByZXN1bHROb1N1ZmZpeE9yUHJlZml4LFxyXG4gICAgICAgIHRoaXMubWFza1NwZWNpYWxDaGFyYWN0ZXJzXHJcbiAgICAgICk7XHJcbiAgICAgIGNoYW5nZVZhbHVlID0gdGhpcy5pc051bWJlclZhbHVlID8gTnVtYmVyKGNoYW5nZVZhbHVlKSA6IGNoYW5nZVZhbHVlO1xyXG4gICAgfVxyXG5cclxuICAgIHJldHVybiBjaGFuZ2VWYWx1ZTtcclxuICB9XHJcblxyXG4gIHByaXZhdGUgX3JlbW92ZU1hc2soXHJcbiAgICB2YWx1ZTogc3RyaW5nLFxyXG4gICAgc3BlY2lhbENoYXJhY3RlcnNGb3JSZW1vdmU6IHN0cmluZ1tdXHJcbiAgKTogc3RyaW5nIHtcclxuICAgIHJldHVybiB2YWx1ZVxyXG4gICAgICA/IHZhbHVlLnJlcGxhY2UodGhpcy5fcmVnRXhwRm9yUmVtb3ZlKHNwZWNpYWxDaGFyYWN0ZXJzRm9yUmVtb3ZlKSwgJycpXHJcbiAgICAgIDogdmFsdWU7XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIF9yZW1vdmVQcmVmaXgodmFsdWU6IHN0cmluZyk6IHN0cmluZyB7XHJcbiAgICBpZiAoIXRoaXMucHJlZml4KSB7XHJcbiAgICAgIHJldHVybiB2YWx1ZTtcclxuICAgIH1cclxuICAgIHJldHVybiB2YWx1ZSA/IHZhbHVlLnJlcGxhY2UodGhpcy5wcmVmaXgsICcnKSA6IHZhbHVlO1xyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBfcmVtb3ZlU3VmaXgodmFsdWU6IHN0cmluZyk6IHN0cmluZyB7XHJcbiAgICBpZiAoIXRoaXMuc3VmaXgpIHtcclxuICAgICAgcmV0dXJuIHZhbHVlO1xyXG4gICAgfVxyXG4gICAgcmV0dXJuIHZhbHVlID8gdmFsdWUucmVwbGFjZSh0aGlzLnN1Zml4LCAnJykgOiB2YWx1ZTtcclxuICB9XHJcblxyXG4gIHByaXZhdGUgX3JlZ0V4cEZvclJlbW92ZShzcGVjaWFsQ2hhcmFjdGVyc0ZvclJlbW92ZTogc3RyaW5nW10pOiBSZWdFeHAge1xyXG4gICAgcmV0dXJuIG5ldyBSZWdFeHAoXHJcbiAgICAgIHNwZWNpYWxDaGFyYWN0ZXJzRm9yUmVtb3ZlLm1hcCgoaXRlbTogc3RyaW5nKSA9PiBgXFxcXCR7aXRlbX1gKS5qb2luKCd8JyksXHJcbiAgICAgICdnaSdcclxuICAgICk7XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIF9hcHBseU1hc2tSZXN1bHQocmVzdWx0OiBzdHJpbmcpIHtcclxuICAgIGlmICghdGhpcy5zaG93TWFza1R5cGVkKSB7XHJcbiAgICAgIHJldHVybiByZXN1bHQ7XHJcbiAgICB9XHJcbiAgICBjb25zdCByZXNMZW46IG51bWJlciA9IHJlc3VsdC5sZW5ndGg7XHJcbiAgICBjb25zdCBwcmVmTm1hc2s6IHN0cmluZyA9IHRoaXMucHJlZml4ICsgdGhpcy5tYXNrSXNTaG93bjtcclxuICAgIGNvbnN0IGlmTWFza0lzU2hvd24gPSBwcmVmTm1hc2suc2xpY2UocmVzTGVuKTtcclxuXHJcbiAgICByZXR1cm4gcmVzdWx0ICsgaWZNYXNrSXNTaG93bjtcclxuICB9XHJcblxyXG4gIHByaXZhdGUgX29uQ29udHJvbFZhbHVlQ2hhbmdlKHZhbHVlOiBzdHJpbmcpIHtcclxuICAgIC8qXHJcbiAgICAgIEJlY2F1c2Ugd2UgYXJlIG5vIGxvbmdlciB3b3JraW5nIHdpdGggdGhlIENvbnRyb2xWYWx1ZUFjY2Vzc29yIChzaW5jZSBpdCBkb2Vzbid0IHBsYXkgbmljZSB3aXRoIElvbmljKS5cclxuICAgICAgV2UgbmVlZCBsb2dpYyBoZXJlIHRvIHRyYWNrIGNoYW5nZXMgbWFkZSBwcm9ncmFtbWF0aWNhbGx5IHRvIHRoZSBmb3JtIHZhbHVlLiAgU3BlY2lmaWNhbGx5IGNoYW5nZXNcclxuICAgICAgZG9uZSBPVVRTSURFIG9mIHRoZSBtYXNrLiBTaW5jZSBjaGFuZ2VzIGRvbmUgaW5zaWRlIHRoZSBtYXNrIG1heSBhbHNvIGZpcmUgb2ZmIHRoaXMgbWV0aG9kXHJcbiAgICAgIHdlIG5lZWQgdG8gZG8gc29tZSBqaXUgaml0c3UgdG8gZW5zdXJlIHdlIGFyZSBpZ25vcmluZyB0aG9zZSBjaGFuZ2VzLlxyXG4gICAgKi9cclxuICAgIGNvbnN0IG5ld1ZhbHVlID0gdGhpcy5nZXRVbm1hc2tlZFZhbHVlKHZhbHVlKTtcclxuXHJcbiAgICBpZiAodGhpcy51bm1hc2tlZFZhbHVlID09PSBuZXdWYWx1ZSkge1xyXG4gICAgICByZXR1cm47XHJcbiAgICB9XHJcblxyXG4gICAgbGV0IHVubWFza2VkU3Vic3RyaW5nOiBzdHJpbmcgPSBudWxsO1xyXG5cclxuICAgIC8vIFRoaXMgbWV0aG9kICh2YWx1ZSBjaGFuZ2UpIGZpcmVzIG9mZiBiZWZvcmUgYSBLZXlkb3duIG9yIElucHV0IGV2ZW50LCBzbyB3ZSBuZWVkIHRvIHN1YnRyYWN0XHJcbiAgICAvLyB0aGUgbGF0ZXN0IGNoYW5nZSBhbmQgY29tcGFyZSB0byBvdXIgcHJldmlvdXMgKGtub3duKSB2YWx1ZS5cclxuICAgIGlmICh0aGlzLnVubWFza2VkVmFsdWUgIT0gbnVsbCkge1xyXG4gICAgICBjb25zdCB2ID0gdGhpcy51bm1hc2tlZFZhbHVlLnRvU3RyaW5nKCk7XHJcbiAgICAgIHVubWFza2VkU3Vic3RyaW5nID0gdi5zdWJzdHJpbmcoMCwgdi5sZW5ndGggLSAxKTtcclxuICAgIH1cclxuXHJcbiAgICBpZiAobmV3VmFsdWUgIT09IHVubWFza2VkU3Vic3RyaW5nKSB7XHJcbiAgICAgIGNvbnN0IG52ID0gbmV3VmFsdWUgIT0gbnVsbCA/IG5ld1ZhbHVlLnRvU3RyaW5nKCkgOiBudWxsO1xyXG4gICAgICBjb25zdCB2ID0gdGhpcy5hcHBseU1hc2sobnYsIHRoaXMubWFza0V4cHJlc3Npb24pO1xyXG4gICAgICB0aGlzLnNldFZhbHVlKHYpO1xyXG4gICAgfVxyXG4gIH1cclxufVxyXG4iXX0=