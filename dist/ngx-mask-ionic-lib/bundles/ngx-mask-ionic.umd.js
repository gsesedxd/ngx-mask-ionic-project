(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('@angular/core'), require('@angular/common'), require('@angular/forms')) :
    typeof define === 'function' && define.amd ? define('ngx-mask-ionic', ['exports', '@angular/core', '@angular/common', '@angular/forms'], factory) :
    (factory((global['ngx-mask-ionic'] = {}),global.ng.core,global.ng.common,global.ng.forms));
}(this, (function (exports,core,common,forms) { 'use strict';

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /** @type {?} */
    var config = new core.InjectionToken('config');
    /** @type {?} */
    var NEW_CONFIG = new core.InjectionToken('NEW_CONFIG');
    /** @type {?} */
    var INITIAL_CONFIG = new core.InjectionToken('INITIAL_CONFIG');
    /** @type {?} */
    var initialConfig = {
        sufix: '',
        prefix: '',
        clearIfNotMatch: false,
        showTemplate: false,
        showMaskTyped: false,
        dropSpecialCharacters: true,
        specialCharacters: [
            '/',
            '(',
            ')',
            '.',
            ':',
            '-',
            ' ',
            '+',
            ',',
            '@',
            '[',
            ']',
            '"',
            "'"
        ],
        patterns: {
            '0': {
                pattern: new RegExp('\\d')
            },
            '9': {
                pattern: new RegExp('\\d'),
                optional: true
            },
            A: {
                pattern: new RegExp('[a-zA-Z0-9]')
            },
            S: {
                pattern: new RegExp('[a-zA-Z]')
            },
            d: {
                pattern: new RegExp('\\d')
            },
            m: {
                pattern: new RegExp('\\d')
            }
        }
    };

    /*! *****************************************************************************
    Copyright (c) Microsoft Corporation. All rights reserved.
    Licensed under the Apache License, Version 2.0 (the "License"); you may not use
    this file except in compliance with the License. You may obtain a copy of the
    License at http://www.apache.org/licenses/LICENSE-2.0

    THIS CODE IS PROVIDED ON AN *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
    KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
    WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
    MERCHANTABLITY OR NON-INFRINGEMENT.

    See the Apache Version 2.0 License for specific language governing permissions
    and limitations under the License.
    ***************************************************************************** */
    /* global Reflect, Promise */
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b)
                if (b.hasOwnProperty(p))
                    d[p] = b[p]; };
        return extendStatics(d, b);
    };
    function __extends(d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    }
    var __assign = function () {
        __assign = Object.assign || function __assign(t) {
            for (var s, i = 1, n = arguments.length; i < n; i++) {
                s = arguments[i];
                for (var p in s)
                    if (Object.prototype.hasOwnProperty.call(s, p))
                        t[p] = s[p];
            }
            return t;
        };
        return __assign.apply(this, arguments);
    };
    function __read(o, n) {
        var m = typeof Symbol === "function" && o[Symbol.iterator];
        if (!m)
            return o;
        var i = m.call(o), r, ar = [], e;
        try {
            while ((n === void 0 || n-- > 0) && !(r = i.next()).done)
                ar.push(r.value);
        }
        catch (error) {
            e = { error: error };
        }
        finally {
            try {
                if (r && !r.done && (m = i["return"]))
                    m.call(i);
            }
            finally {
                if (e)
                    throw e.error;
            }
        }
        return ar;
    }

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var MaskApplierService = /** @class */ (function () {
        function MaskApplierService(_config) {
            this._config = _config;
            this.maskExpression = '';
            this._shift = new Set();
            this.maskSpecialCharacters = ( /** @type {?} */(this._config)).specialCharacters;
            this.maskAvailablePatterns = this._config.patterns;
            this.clearIfNotMatch = this._config.clearIfNotMatch;
            this.dropSpecialCharacters = this._config.dropSpecialCharacters;
            this.maskSpecialCharacters = ( /** @type {?} */(this._config)).specialCharacters;
            this.maskAvailablePatterns = this._config.patterns;
            this.prefix = this._config.prefix;
            this.sufix = this._config.sufix;
        }
        // tslint:disable-next-line:no-any
        // tslint:disable-next-line:no-any
        /**
         * @param {?} inputValue
         * @param {?} maskAndPattern
         * @return {?}
         */
        MaskApplierService.prototype.applyMaskWithPattern =
            // tslint:disable-next-line:no-any
            /**
             * @param {?} inputValue
             * @param {?} maskAndPattern
             * @return {?}
             */
            function (inputValue, maskAndPattern) {
                var _a = __read(maskAndPattern, 2), mask = _a[0], customPattern = _a[1];
                this.customPattern = customPattern;
                return this.applyMask(inputValue, mask);
            };
        /**
         * @param {?} inputValue
         * @param {?} maskExpression
         * @param {?=} position
         * @param {?=} cb
         * @return {?}
         */
        MaskApplierService.prototype.applyMask = /**
         * @param {?} inputValue
         * @param {?} maskExpression
         * @param {?=} position
         * @param {?=} cb
         * @return {?}
         */
            function (inputValue, maskExpression, position, cb) {
                if (position === void 0) {
                    position = 0;
                }
                if (cb === void 0) {
                    cb = function () { };
                }
                if (inputValue === undefined ||
                    inputValue === null ||
                    maskExpression === undefined) {
                    return '';
                }
                /** @type {?} */
                var cursor = 0;
                /** @type {?} */
                var result = "";
                /** @type {?} */
                var multi = false;
                if (inputValue.slice(0, this.prefix.length) === this.prefix) {
                    inputValue = inputValue.slice(this.prefix.length, inputValue.length);
                }
                /** @type {?} */
                var inputArray = inputValue.toString().split('');
                // tslint:disable-next-line
                for (var i = 0, inputSymbol = inputArray[0]; i < inputArray.length; i++, inputSymbol = inputArray[i]) {
                    if (cursor === maskExpression.length) {
                        break;
                    }
                    if (this._checkSymbolMask(inputSymbol, maskExpression[cursor]) &&
                        maskExpression[cursor + 1] === '?') {
                        result += inputSymbol;
                        cursor += 2;
                    }
                    else if (maskExpression[cursor + 1] === '*' &&
                        multi &&
                        this._checkSymbolMask(inputSymbol, maskExpression[cursor + 2])) {
                        result += inputSymbol;
                        cursor += 3;
                        multi = false;
                    }
                    else if (this._checkSymbolMask(inputSymbol, maskExpression[cursor]) &&
                        maskExpression[cursor + 1] === '*') {
                        result += inputSymbol;
                        multi = true;
                    }
                    else if (maskExpression[cursor + 1] === '?' &&
                        this._checkSymbolMask(inputSymbol, maskExpression[cursor + 2])) {
                        result += inputSymbol;
                        cursor += 3;
                    }
                    else if (this._checkSymbolMask(inputSymbol, maskExpression[cursor])) {
                        if (maskExpression[cursor] === 'd') {
                            if (Number(inputSymbol) > 3) {
                                result += 0;
                                cursor += 1;
                                /** @type {?} */
                                var shiftStep = /\*|\?/g.test(maskExpression.slice(0, cursor))
                                    ? inputArray.length
                                    : cursor;
                                this._shift.add(shiftStep + this.prefix.length || 0);
                                i--;
                                continue;
                            }
                        }
                        if (maskExpression[cursor - 1] === 'd') {
                            if (Number(inputValue.slice(cursor - 1, cursor + 1)) > 31) {
                                continue;
                            }
                        }
                        if (maskExpression[cursor] === 'm') {
                            if (Number(inputSymbol) > 1) {
                                result += 0;
                                cursor += 1;
                                /** @type {?} */
                                var shiftStep = /\*|\?/g.test(maskExpression.slice(0, cursor))
                                    ? inputArray.length
                                    : cursor;
                                this._shift.add(shiftStep + this.prefix.length || 0);
                                i--;
                                continue;
                            }
                        }
                        if (maskExpression[cursor - 1] === 'm') {
                            if (Number(inputValue.slice(cursor - 1, cursor + 1)) > 12) {
                                continue;
                            }
                        }
                        result += inputSymbol;
                        cursor++;
                    }
                    else if (this.maskSpecialCharacters.indexOf(maskExpression[cursor]) !== -1) {
                        result += maskExpression[cursor];
                        cursor++;
                        /** @type {?} */
                        var shiftStep = /\*|\?/g.test(maskExpression.slice(0, cursor))
                            ? inputArray.length
                            : cursor;
                        this._shift.add(shiftStep + this.prefix.length || 0);
                        i--;
                    }
                    else if (this.maskSpecialCharacters.indexOf(inputSymbol) > -1 &&
                        this.maskAvailablePatterns[maskExpression[cursor]] &&
                        this.maskAvailablePatterns[maskExpression[cursor]].optional) {
                        cursor++;
                        i--;
                    }
                    else if (this.maskExpression[cursor + 1] === '*' &&
                        this._findSpecialChar(this.maskExpression[cursor + 2]) &&
                        this._findSpecialChar(inputSymbol) === this.maskExpression[cursor + 2]) {
                        cursor += 3;
                        result += inputSymbol;
                    }
                }
                if (result.length + 1 === maskExpression.length &&
                    this.maskSpecialCharacters.indexOf(maskExpression[maskExpression.length - 1]) !== -1) {
                    result += maskExpression[maskExpression.length - 1];
                }
                /** @type {?} */
                var shift = 1;
                /** @type {?} */
                var newPosition = position + 1;
                while (this._shift.has(newPosition)) {
                    shift++;
                    newPosition++;
                }
                cb(this._shift.has(position) ? shift : 0);
                /** @type {?} */
                var res = "" + this.prefix + result;
                res =
                    this.sufix && cursor === maskExpression.length
                        ? "" + this.prefix + result + this.sufix
                        : "" + this.prefix + result;
                return res;
            };
        /**
         * @param {?} inputSymbol
         * @return {?}
         */
        MaskApplierService.prototype._findSpecialChar = /**
         * @param {?} inputSymbol
         * @return {?}
         */
            function (inputSymbol) {
                /** @type {?} */
                var symbol = this.maskSpecialCharacters.find(function (val) { return val === inputSymbol; });
                return symbol;
            };
        /**
         * @private
         * @param {?} inputSymbol
         * @param {?} maskSymbol
         * @return {?}
         */
        MaskApplierService.prototype._checkSymbolMask = /**
         * @private
         * @param {?} inputSymbol
         * @param {?} maskSymbol
         * @return {?}
         */
            function (inputSymbol, maskSymbol) {
                this.maskAvailablePatterns = this.customPattern
                    ? this.customPattern
                    : this.maskAvailablePatterns;
                return (this.maskAvailablePatterns[maskSymbol] &&
                    this.maskAvailablePatterns[maskSymbol].pattern &&
                    this.maskAvailablePatterns[maskSymbol].pattern.test(inputSymbol));
            };
        MaskApplierService.decorators = [
            { type: core.Injectable }
        ];
        /** @nocollapse */
        MaskApplierService.ctorParameters = function () {
            return [
                { type: undefined, decorators: [{ type: core.Inject, args: [config,] }] }
            ];
        };
        return MaskApplierService;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var MaskService = /** @class */ (function (_super) {
        __extends(MaskService, _super);
        function MaskService(document, _config, _elementRef, _renderer, _ngControl) {
            var _this = _super.call(this, _config) || this;
            _this.document = document;
            _this._config = _config;
            _this._elementRef = _elementRef;
            _this._renderer = _renderer;
            _this._ngControl = _ngControl;
            _this.maskExpression = '';
            _this.isNumberValue = false;
            _this.showMaskTyped = false;
            _this.maskIsShown = '';
            _this.onTouch = function () { };
            _this.setFormElement(_elementRef.nativeElement);
            setTimeout(function () {
                if (_this._formElement.localName !== 'input') {
                    /** @type {?} */
                    var inputEl = _this._elementRef.nativeElement.querySelector('input');
                    if (inputEl != null) {
                        _this.setFormElement(inputEl);
                    }
                    else {
                        console.warn('mask-service: Could not find Input Element.  Please make sure one is present.');
                    }
                }
                _this._ngControl.valueChanges.subscribe(function (value) {
                    _this._onControlValueChange(value);
                });
            });
            return _this;
        }
        /**
         * @param {?} el
         * @return {?}
         */
        MaskService.prototype.setFormElement = /**
         * @param {?} el
         * @return {?}
         */
            function (el) {
                this._formElement = el;
            };
        /**
         * @param {?} inputValue
         * @param {?} maskExpression
         * @param {?=} position
         * @param {?=} cb
         * @return {?}
         */
        MaskService.prototype.applyMask = /**
         * @param {?} inputValue
         * @param {?} maskExpression
         * @param {?=} position
         * @param {?=} cb
         * @return {?}
         */
            function (inputValue, maskExpression, position, cb) {
                if (position === void 0) {
                    position = 0;
                }
                if (cb === void 0) {
                    cb = function () { };
                }
                this.maskIsShown = this.showMaskTyped
                    ? this.maskExpression.replace(/[0-9]/g, '_')
                    : '';
                if (!inputValue && this.showMaskTyped) {
                    return this.prefix + this.maskIsShown;
                }
                /** @type {?} */
                var result = _super.prototype.applyMask.call(this, inputValue, maskExpression, position, cb);
                this.unmaskedValue = this.getUnmaskedValue(result);
                return this._applyMaskResult(result);
            };
        /**
         * @param {?=} position
         * @param {?=} cb
         * @return {?}
         */
        MaskService.prototype.applyValueChanges = /**
         * @param {?=} position
         * @param {?=} cb
         * @return {?}
         */
            function (position, cb) {
                if (position === void 0) {
                    position = 0;
                }
                if (cb === void 0) {
                    cb = function () { };
                }
                /** @type {?} */
                var maskedInput = this.applyMask(this._formElement.value, this.maskExpression, position, cb);
                this._formElement.value = maskedInput;
                if (this._formElement === this.document.activeElement) {
                    return;
                }
                this.clearIfNotMatchFn();
            };
        /**
         * @return {?}
         */
        MaskService.prototype.showMaskInInput = /**
         * @return {?}
         */
            function () {
                if (this.showMaskTyped) {
                    this.maskIsShown = this.maskExpression.replace(/[0-9]/g, '_');
                }
            };
        /**
         * @return {?}
         */
        MaskService.prototype.clearIfNotMatchFn = /**
         * @return {?}
         */
            function () {
                if (this.clearIfNotMatch === true &&
                    this.maskExpression.length !== this._formElement.value.length) {
                    this.setValue('');
                    this.applyMask(this._formElement.value, this.maskExpression);
                }
            };
        /**
         * @param {?} value
         * @return {?}
         */
        MaskService.prototype.setValue = /**
         * @param {?} value
         * @return {?}
         */
            function (value) {
                this.unmaskedValue = this.getUnmaskedValue(value);
                this._ngControl.control.setValue(value);
            };
        /**
         * @param {?} name
         * @param {?} value
         * @return {?}
         */
        MaskService.prototype.setFormElementProperty = /**
         * @param {?} name
         * @param {?} value
         * @return {?}
         */
            function (name, value) {
                if (this._formElement) {
                    this._renderer.setProperty(this._formElement, name, value);
                }
            };
        /**
         * @param {?} result
         * @return {?}
         */
        MaskService.prototype.getUnmaskedValue = /**
         * @param {?} result
         * @return {?}
         */
            function (result) {
                /** @type {?} */
                var resultNoSuffixOrPrefix = this._removeSufix(this._removePrefix(result));
                /** @type {?} */
                var changeValue = resultNoSuffixOrPrefix;
                if (Array.isArray(this.dropSpecialCharacters)) {
                    changeValue = this._removeMask(resultNoSuffixOrPrefix, this.dropSpecialCharacters);
                }
                else if (this.dropSpecialCharacters) {
                    changeValue = this._removeMask(resultNoSuffixOrPrefix, this.maskSpecialCharacters);
                    changeValue = this.isNumberValue ? Number(changeValue) : changeValue;
                }
                return changeValue;
            };
        /**
         * @private
         * @param {?} value
         * @param {?} specialCharactersForRemove
         * @return {?}
         */
        MaskService.prototype._removeMask = /**
         * @private
         * @param {?} value
         * @param {?} specialCharactersForRemove
         * @return {?}
         */
            function (value, specialCharactersForRemove) {
                return value
                    ? value.replace(this._regExpForRemove(specialCharactersForRemove), '')
                    : value;
            };
        /**
         * @private
         * @param {?} value
         * @return {?}
         */
        MaskService.prototype._removePrefix = /**
         * @private
         * @param {?} value
         * @return {?}
         */
            function (value) {
                if (!this.prefix) {
                    return value;
                }
                return value ? value.replace(this.prefix, '') : value;
            };
        /**
         * @private
         * @param {?} value
         * @return {?}
         */
        MaskService.prototype._removeSufix = /**
         * @private
         * @param {?} value
         * @return {?}
         */
            function (value) {
                if (!this.sufix) {
                    return value;
                }
                return value ? value.replace(this.sufix, '') : value;
            };
        /**
         * @private
         * @param {?} specialCharactersForRemove
         * @return {?}
         */
        MaskService.prototype._regExpForRemove = /**
         * @private
         * @param {?} specialCharactersForRemove
         * @return {?}
         */
            function (specialCharactersForRemove) {
                return new RegExp(specialCharactersForRemove.map(function (item) { return "\\" + item; }).join('|'), 'gi');
            };
        /**
         * @private
         * @param {?} result
         * @return {?}
         */
        MaskService.prototype._applyMaskResult = /**
         * @private
         * @param {?} result
         * @return {?}
         */
            function (result) {
                if (!this.showMaskTyped) {
                    return result;
                }
                /** @type {?} */
                var resLen = result.length;
                /** @type {?} */
                var prefNmask = this.prefix + this.maskIsShown;
                /** @type {?} */
                var ifMaskIsShown = prefNmask.slice(resLen);
                return result + ifMaskIsShown;
            };
        /**
         * @private
         * @param {?} value
         * @return {?}
         */
        MaskService.prototype._onControlValueChange = /**
         * @private
         * @param {?} value
         * @return {?}
         */
            function (value) {
                /*
                      Because we are no longer working with the ControlValueAccessor (since it doesn't play nice with Ionic).
                      We need logic here to track changes made programmatically to the form value.  Specifically changes
                      done OUTSIDE of the mask. Since changes done inside the mask may also fire off this method
                      we need to do some jiu jitsu to ensure we are ignoring those changes.
                    */
                /** @type {?} */
                var newValue = this.getUnmaskedValue(value);
                if (this.unmaskedValue === newValue) {
                    return;
                }
                /** @type {?} */
                var unmaskedSubstring = null;
                // This method (value change) fires off before a Keydown or Input event, so we need to subtract
                // the latest change and compare to our previous (known) value.
                if (this.unmaskedValue != null) {
                    /** @type {?} */
                    var v = this.unmaskedValue.toString();
                    unmaskedSubstring = v.substring(0, v.length - 1);
                }
                if (newValue !== unmaskedSubstring) {
                    /** @type {?} */
                    var nv = newValue != null ? newValue.toString() : null;
                    /** @type {?} */
                    var v = this.applyMask(nv, this.maskExpression);
                    this.setValue(v);
                }
            };
        MaskService.decorators = [
            { type: core.Injectable }
        ];
        /** @nocollapse */
        MaskService.ctorParameters = function () {
            return [
                { type: undefined, decorators: [{ type: core.Inject, args: [common.DOCUMENT,] }] },
                { type: undefined, decorators: [{ type: core.Inject, args: [config,] }] },
                { type: core.ElementRef },
                { type: core.Renderer2 },
                { type: forms.NgControl }
            ];
        };
        return MaskService;
    }(MaskApplierService));

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var MaskDirective = /** @class */ (function () {
        function MaskDirective(document, _maskService, _ngControl) {
            this.document = document;
            this._maskService = _maskService;
            this._ngControl = _ngControl;
            this._position = null;
            // tslint:disable-next-line
            this.onTouch = function () { };
        }
        Object.defineProperty(MaskDirective.prototype, "maskExpression", {
            set: /**
             * @param {?} value
             * @return {?}
             */ function (value) {
                this._maskValue = value || '';
                if (!this._maskValue) {
                    return;
                }
                this._inputValue = this._ngControl.control.value;
                this._initializeMask();
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MaskDirective.prototype, "specialCharacters", {
            set: /**
             * @param {?} value
             * @return {?}
             */ function (value) {
                if (!value ||
                    !Array.isArray(value) ||
                    (Array.isArray(value) && !value.length)) {
                    return;
                }
                this._maskService.maskSpecialCharacters = value;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MaskDirective.prototype, "patterns", {
            set: /**
             * @param {?} value
             * @return {?}
             */ function (value) {
                if (!value) {
                    return;
                }
                this._maskService.maskAvailablePatterns = value;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MaskDirective.prototype, "prefix", {
            set: /**
             * @param {?} value
             * @return {?}
             */ function (value) {
                if (!value) {
                    return;
                }
                this._maskService.prefix = value;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MaskDirective.prototype, "sufix", {
            set: /**
             * @param {?} value
             * @return {?}
             */ function (value) {
                if (!value) {
                    return;
                }
                this._maskService.sufix = value;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MaskDirective.prototype, "dropSpecialCharacters", {
            set: /**
             * @param {?} value
             * @return {?}
             */ function (value) {
                this._maskService.dropSpecialCharacters = value;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MaskDirective.prototype, "showMaskTyped", {
            set: /**
             * @param {?} value
             * @return {?}
             */ function (value) {
                if (!value) {
                    return;
                }
                this._maskService.showMaskTyped = value;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MaskDirective.prototype, "showTemplate", {
            set: /**
             * @param {?} value
             * @return {?}
             */ function (value) {
                this._maskService.showTemplate = value;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MaskDirective.prototype, "clearIfNotMatch", {
            set: /**
             * @param {?} value
             * @return {?}
             */ function (value) {
                this._maskService.clearIfNotMatch = value;
            },
            enumerable: true,
            configurable: true
        });
        /**
         * @param {?} e
         * @return {?}
         */
        MaskDirective.prototype.onInput = /**
         * @param {?} e
         * @return {?}
         */
            function (e) {
                /** @type {?} */
                var el = ( /** @type {?} */(e.target));
                this._inputValue = el.value;
                if (!this._maskValue) {
                    return;
                }
                /** @type {?} */
                var position = (( /** @type {?} */(el.selectionStart))) === 1
                    ? (( /** @type {?} */(el.selectionStart))) + this._maskService.prefix.length
                    : (( /** @type {?} */(el.selectionStart)));
                /** @type {?} */
                var caretShift = 0;
                this._maskService.applyValueChanges(position, function (shift) { return (caretShift = shift); });
                // only set the selection if the element is active
                if (this.document.activeElement !== el) {
                    return;
                }
                el.selectionStart = el.selectionEnd =
                    this._position !== null
                        ? this._position
                        : position +
                            // tslint:disable-next-line
                            ((( /** @type {?} */(e))).inputType === 'deleteContentBackward' ? 0 : caretShift);
                this._position = null;
            };
        /**
         * @return {?}
         */
        MaskDirective.prototype.onBlur = /**
         * @return {?}
         */
            function () {
                this._maskService.clearIfNotMatchFn();
                this.onTouch();
            };
        /**
         * @param {?} e
         * @return {?}
         */
        MaskDirective.prototype.onFocus = /**
         * @param {?} e
         * @return {?}
         */
            function (e) {
                /** @type {?} */
                var el = ( /** @type {?} */(e.target));
                if (el !== null &&
                    el.selectionStart !== null &&
                    el.selectionStart === el.selectionEnd &&
                    el.selectionStart > this._maskService.prefix.length &&
                    // tslint:disable-next-line
                    (( /** @type {?} */(e))).keyCode !== 38) {
                    return;
                }
                if (this._maskService.showMaskTyped) {
                    this._maskService.maskIsShown = this._maskService.maskExpression.replace(/[0-9]/g, '_');
                }
                el.value =
                    !el.value || el.value === this._maskService.prefix
                        ? this._maskService.prefix + this._maskService.maskIsShown
                        : el.value;
                /** fix of cursor position with prefix when mouse click occur */
                if (((( /** @type {?} */(el.selectionStart))) || (( /** @type {?} */(el.selectionEnd)))) <=
                    this._maskService.prefix.length) {
                    el.selectionStart = this._maskService.prefix.length;
                    return;
                }
            };
        /**
         * @param {?} e
         * @return {?}
         */
        MaskDirective.prototype.onKeyDown = /**
         * @param {?} e
         * @return {?}
         */
            function (e) {
                /** @type {?} */
                var el = ( /** @type {?} */(e.target));
                if (e.keyCode === 38) {
                    e.preventDefault();
                }
                if (e.keyCode === 37 || e.keyCode === 8) {
                    if ((( /** @type {?} */(el.selectionStart))) <= this._maskService.prefix.length &&
                        (( /** @type {?} */(el.selectionEnd))) <= this._maskService.prefix.length) {
                        e.preventDefault();
                    }
                    this.onFocus(e);
                    if (e.keyCode === 8 &&
                        el.selectionStart === 0 &&
                        el.selectionEnd === el.value.length) {
                        el.value = this._maskService.prefix;
                        this._position = this._maskService.prefix
                            ? this._maskService.prefix.length
                            : 1;
                        this.onInput(e);
                    }
                }
            };
        /**
         * @return {?}
         */
        MaskDirective.prototype.onPaste = /**
         * @return {?}
         */
            function () {
                this._position = Number.MAX_SAFE_INTEGER;
            };
        /** It disables the input element */
        /**
         * It disables the input element
         * @param {?} isDisabled
         * @return {?}
         */
        MaskDirective.prototype.setDisabledState = /**
         * It disables the input element
         * @param {?} isDisabled
         * @return {?}
         */
            function (isDisabled) {
                this._maskService.setFormElementProperty('disabled', isDisabled);
            };
        /**
         * @private
         * @param {?} maskExp
         * @return {?}
         */
        MaskDirective.prototype._repeatPatternSymbols = /**
         * @private
         * @param {?} maskExp
         * @return {?}
         */
            function (maskExp) {
                var _this = this;
                return ((maskExp.match(/{[0-9]+}/) &&
                    maskExp
                        .split('')
                        .reduce(function (accum, currval, index) {
                        _this._start = currval === '{' ? index : _this._start;
                        if (currval !== '}') {
                            return _this._maskService._findSpecialChar(currval)
                                ? accum + currval
                                : accum;
                        }
                        _this._end = index;
                        /** @type {?} */
                        var repeatNumber = Number(maskExp.slice(_this._start + 1, _this._end));
                        /** @type {?} */
                        var repaceWith = new Array(repeatNumber + 1).join(maskExp[_this._start - 1]);
                        return accum + repaceWith;
                    }, '')) ||
                    maskExp);
            };
        /**
         * @private
         * @return {?}
         */
        MaskDirective.prototype._initializeMask = /**
         * @private
         * @return {?}
         */
            function () {
                this._maskService.maskExpression = this._repeatPatternSymbols(this._maskValue);
                /** @type {?} */
                var m = this._maskService.applyMask(this._inputValue, this._maskService.maskExpression);
                this._maskService.setValue(m);
            };
        MaskDirective.decorators = [
            { type: core.Directive, args: [{
                        selector: '[mask]',
                        providers: [MaskService]
                    },] }
        ];
        /** @nocollapse */
        MaskDirective.ctorParameters = function () {
            return [
                { type: undefined, decorators: [{ type: core.Inject, args: [common.DOCUMENT,] }] },
                { type: MaskService },
                { type: forms.NgControl }
            ];
        };
        MaskDirective.propDecorators = {
            maskExpression: [{ type: core.Input, args: ['mask',] }],
            specialCharacters: [{ type: core.Input }],
            patterns: [{ type: core.Input }],
            prefix: [{ type: core.Input }],
            sufix: [{ type: core.Input }],
            dropSpecialCharacters: [{ type: core.Input }],
            showMaskTyped: [{ type: core.Input }],
            showTemplate: [{ type: core.Input }],
            clearIfNotMatch: [{ type: core.Input }],
            onInput: [{ type: core.HostListener, args: ['input', ['$event'],] }],
            onBlur: [{ type: core.HostListener, args: ['blur',] }],
            onFocus: [{ type: core.HostListener, args: ['click', ['$event'],] }],
            onKeyDown: [{ type: core.HostListener, args: ['keydown', ['$event'],] }],
            onPaste: [{ type: core.HostListener, args: ['paste',] }]
        };
        return MaskDirective;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var MaskPipe = /** @class */ (function () {
        function MaskPipe(_maskService) {
            this._maskService = _maskService;
        }
        /**
         * @param {?} value
         * @param {?} mask
         * @return {?}
         */
        MaskPipe.prototype.transform = /**
         * @param {?} value
         * @param {?} mask
         * @return {?}
         */
            function (value, mask) {
                if (!value) {
                    return '';
                }
                if (typeof mask === 'string') {
                    return this._maskService.applyMask("" + value, mask);
                }
                return this._maskService.applyMaskWithPattern("" + value, mask);
            };
        MaskPipe.decorators = [
            { type: core.Pipe, args: [{
                        name: 'mask',
                        pure: true
                    },] }
        ];
        /** @nocollapse */
        MaskPipe.ctorParameters = function () {
            return [
                { type: MaskApplierService }
            ];
        };
        return MaskPipe;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var NgxMaskIonicModule = /** @class */ (function () {
        function NgxMaskIonicModule() {
        }
        /**
         * @param {?=} configValue
         * @return {?}
         */
        NgxMaskIonicModule.forRoot = /**
         * @param {?=} configValue
         * @return {?}
         */
            function (configValue) {
                return {
                    ngModule: NgxMaskIonicModule,
                    providers: [
                        {
                            provide: NEW_CONFIG,
                            useValue: configValue
                        },
                        {
                            provide: INITIAL_CONFIG,
                            useValue: initialConfig
                        },
                        {
                            provide: config,
                            useFactory: _configFactory,
                            deps: [INITIAL_CONFIG, NEW_CONFIG]
                        },
                        MaskPipe
                    ]
                };
            };
        /**
         * @param {?=} configValue
         * @return {?}
         */
        NgxMaskIonicModule.forChild = /**
         * @param {?=} configValue
         * @return {?}
         */
            function (configValue) {
                return {
                    ngModule: NgxMaskIonicModule
                };
            };
        NgxMaskIonicModule.decorators = [
            { type: core.NgModule, args: [{
                        providers: [MaskApplierService, MaskPipe],
                        exports: [MaskDirective, MaskPipe],
                        declarations: [MaskDirective, MaskPipe]
                    },] }
        ];
        return NgxMaskIonicModule;
    }());
    /**
     * \@internal
     * @param {?} initConfig
     * @param {?} configValue
     * @return {?}
     */
    function _configFactory(initConfig, configValue) {
        return typeof configValue === 'function'
            ? configValue()
            : __assign({}, initConfig, configValue);
    }

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */

    exports.config = config;
    exports.NEW_CONFIG = NEW_CONFIG;
    exports.INITIAL_CONFIG = INITIAL_CONFIG;
    exports.initialConfig = initialConfig;
    exports.MaskDirective = MaskDirective;
    exports.MaskService = MaskService;
    exports._configFactory = _configFactory;
    exports.NgxMaskIonicModule = NgxMaskIonicModule;
    exports.MaskPipe = MaskPipe;
    exports.ɵa = MaskApplierService;

    Object.defineProperty(exports, '__esModule', { value: true });

})));

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibmd4LW1hc2staW9uaWMudW1kLmpzLm1hcCIsInNvdXJjZXMiOlsibmc6Ly9uZ3gtbWFzay1pb25pYy9saWIvY29uZmlnLnRzIiwibm9kZV9tb2R1bGVzL3RzbGliL3RzbGliLmVzNi5qcyIsIm5nOi8vbmd4LW1hc2staW9uaWMvbGliL21hc2stYXBwbGllci5zZXJ2aWNlLnRzIiwibmc6Ly9uZ3gtbWFzay1pb25pYy9saWIvbWFzay5zZXJ2aWNlLnRzIiwibmc6Ly9uZ3gtbWFzay1pb25pYy9saWIvbWFzay5kaXJlY3RpdmUudHMiLCJuZzovL25neC1tYXNrLWlvbmljL2xpYi9tYXNrLnBpcGUudHMiLCJuZzovL25neC1tYXNrLWlvbmljL2xpYi9uZ3gtbWFzay1pb25pYy5tb2R1bGUudHMiXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0aW9uVG9rZW4gfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuXHJcbmV4cG9ydCBpbnRlcmZhY2UgSUNvbmZpZyB7XHJcbiAgc3VmaXg6IHN0cmluZztcclxuICBwcmVmaXg6IHN0cmluZztcclxuICBjbGVhcklmTm90TWF0Y2g6IGJvb2xlYW47XHJcbiAgc2hvd1RlbXBsYXRlOiBib29sZWFuO1xyXG4gIHNob3dNYXNrVHlwZWQ6IGJvb2xlYW47XHJcbiAgZHJvcFNwZWNpYWxDaGFyYWN0ZXJzOiBib29sZWFuIHwgc3RyaW5nW107XHJcbiAgc3BlY2lhbENoYXJhY3RlcnM6IHN0cmluZ1tdO1xyXG4gIHBhdHRlcm5zOiB7XHJcbiAgICBbY2hhcmFjdGVyOiBzdHJpbmddOiB7XHJcbiAgICAgIHBhdHRlcm46IFJlZ0V4cDtcclxuICAgICAgb3B0aW9uYWw/OiBib29sZWFuO1xyXG4gICAgfTtcclxuICB9O1xyXG59XHJcblxyXG5leHBvcnQgdHlwZSBvcHRpb25zQ29uZmlnID0gUGFydGlhbDxJQ29uZmlnPjtcclxuZXhwb3J0IGNvbnN0IGNvbmZpZzogSW5qZWN0aW9uVG9rZW48c3RyaW5nPiA9IG5ldyBJbmplY3Rpb25Ub2tlbignY29uZmlnJyk7XHJcbmV4cG9ydCBjb25zdCBORVdfQ09ORklHOiBJbmplY3Rpb25Ub2tlbjxzdHJpbmc+ID0gbmV3IEluamVjdGlvblRva2VuKFxyXG4gICdORVdfQ09ORklHJ1xyXG4pO1xyXG5leHBvcnQgY29uc3QgSU5JVElBTF9DT05GSUc6IEluamVjdGlvblRva2VuPElDb25maWc+ID0gbmV3IEluamVjdGlvblRva2VuKFxyXG4gICdJTklUSUFMX0NPTkZJRydcclxuKTtcclxuXHJcbmV4cG9ydCBjb25zdCBpbml0aWFsQ29uZmlnOiBJQ29uZmlnID0ge1xyXG4gIHN1Zml4OiAnJyxcclxuICBwcmVmaXg6ICcnLFxyXG4gIGNsZWFySWZOb3RNYXRjaDogZmFsc2UsXHJcbiAgc2hvd1RlbXBsYXRlOiBmYWxzZSxcclxuICBzaG93TWFza1R5cGVkOiBmYWxzZSxcclxuICBkcm9wU3BlY2lhbENoYXJhY3RlcnM6IHRydWUsXHJcbiAgc3BlY2lhbENoYXJhY3RlcnM6IFtcclxuICAgICcvJyxcclxuICAgICcoJyxcclxuICAgICcpJyxcclxuICAgICcuJyxcclxuICAgICc6JyxcclxuICAgICctJyxcclxuICAgICcgJyxcclxuICAgICcrJyxcclxuICAgICcsJyxcclxuICAgICdAJyxcclxuICAgICdbJyxcclxuICAgICddJyxcclxuICAgICdcIicsXHJcbiAgICBcIidcIlxyXG4gIF0sXHJcbiAgcGF0dGVybnM6IHtcclxuICAgICcwJzoge1xyXG4gICAgICBwYXR0ZXJuOiBuZXcgUmVnRXhwKCdcXFxcZCcpXHJcbiAgICB9LFxyXG4gICAgJzknOiB7XHJcbiAgICAgIHBhdHRlcm46IG5ldyBSZWdFeHAoJ1xcXFxkJyksXHJcbiAgICAgIG9wdGlvbmFsOiB0cnVlXHJcbiAgICB9LFxyXG4gICAgQToge1xyXG4gICAgICBwYXR0ZXJuOiBuZXcgUmVnRXhwKCdbYS16QS1aMC05XScpXHJcbiAgICB9LFxyXG4gICAgUzoge1xyXG4gICAgICBwYXR0ZXJuOiBuZXcgUmVnRXhwKCdbYS16QS1aXScpXHJcbiAgICB9LFxyXG4gICAgZDoge1xyXG4gICAgICBwYXR0ZXJuOiBuZXcgUmVnRXhwKCdcXFxcZCcpXHJcbiAgICB9LFxyXG4gICAgbToge1xyXG4gICAgICBwYXR0ZXJuOiBuZXcgUmVnRXhwKCdcXFxcZCcpXHJcbiAgICB9XHJcbiAgfVxyXG59O1xyXG4iLCIvKiEgKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipcclxuQ29weXJpZ2h0IChjKSBNaWNyb3NvZnQgQ29ycG9yYXRpb24uIEFsbCByaWdodHMgcmVzZXJ2ZWQuXHJcbkxpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7IHlvdSBtYXkgbm90IHVzZVxyXG50aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS4gWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZVxyXG5MaWNlbnNlIGF0IGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG5cclxuVEhJUyBDT0RFIElTIFBST1ZJREVEIE9OIEFOICpBUyBJUyogQkFTSVMsIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWVxyXG5LSU5ELCBFSVRIRVIgRVhQUkVTUyBPUiBJTVBMSUVELCBJTkNMVURJTkcgV0lUSE9VVCBMSU1JVEFUSU9OIEFOWSBJTVBMSUVEXHJcbldBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBUSVRMRSwgRklUTkVTUyBGT1IgQSBQQVJUSUNVTEFSIFBVUlBPU0UsXHJcbk1FUkNIQU5UQUJMSVRZIE9SIE5PTi1JTkZSSU5HRU1FTlQuXHJcblxyXG5TZWUgdGhlIEFwYWNoZSBWZXJzaW9uIDIuMCBMaWNlbnNlIGZvciBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnNcclxuYW5kIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiAqL1xyXG4vKiBnbG9iYWwgUmVmbGVjdCwgUHJvbWlzZSAqL1xyXG5cclxudmFyIGV4dGVuZFN0YXRpY3MgPSBmdW5jdGlvbihkLCBiKSB7XHJcbiAgICBleHRlbmRTdGF0aWNzID0gT2JqZWN0LnNldFByb3RvdHlwZU9mIHx8XHJcbiAgICAgICAgKHsgX19wcm90b19fOiBbXSB9IGluc3RhbmNlb2YgQXJyYXkgJiYgZnVuY3Rpb24gKGQsIGIpIHsgZC5fX3Byb3RvX18gPSBiOyB9KSB8fFxyXG4gICAgICAgIGZ1bmN0aW9uIChkLCBiKSB7IGZvciAodmFyIHAgaW4gYikgaWYgKGIuaGFzT3duUHJvcGVydHkocCkpIGRbcF0gPSBiW3BdOyB9O1xyXG4gICAgcmV0dXJuIGV4dGVuZFN0YXRpY3MoZCwgYik7XHJcbn07XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gX19leHRlbmRzKGQsIGIpIHtcclxuICAgIGV4dGVuZFN0YXRpY3MoZCwgYik7XHJcbiAgICBmdW5jdGlvbiBfXygpIHsgdGhpcy5jb25zdHJ1Y3RvciA9IGQ7IH1cclxuICAgIGQucHJvdG90eXBlID0gYiA9PT0gbnVsbCA/IE9iamVjdC5jcmVhdGUoYikgOiAoX18ucHJvdG90eXBlID0gYi5wcm90b3R5cGUsIG5ldyBfXygpKTtcclxufVxyXG5cclxuZXhwb3J0IHZhciBfX2Fzc2lnbiA9IGZ1bmN0aW9uKCkge1xyXG4gICAgX19hc3NpZ24gPSBPYmplY3QuYXNzaWduIHx8IGZ1bmN0aW9uIF9fYXNzaWduKHQpIHtcclxuICAgICAgICBmb3IgKHZhciBzLCBpID0gMSwgbiA9IGFyZ3VtZW50cy5sZW5ndGg7IGkgPCBuOyBpKyspIHtcclxuICAgICAgICAgICAgcyA9IGFyZ3VtZW50c1tpXTtcclxuICAgICAgICAgICAgZm9yICh2YXIgcCBpbiBzKSBpZiAoT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKHMsIHApKSB0W3BdID0gc1twXTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIHQ7XHJcbiAgICB9XHJcbiAgICByZXR1cm4gX19hc3NpZ24uYXBwbHkodGhpcywgYXJndW1lbnRzKTtcclxufVxyXG5cclxuZXhwb3J0IGZ1bmN0aW9uIF9fcmVzdChzLCBlKSB7XHJcbiAgICB2YXIgdCA9IHt9O1xyXG4gICAgZm9yICh2YXIgcCBpbiBzKSBpZiAoT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKHMsIHApICYmIGUuaW5kZXhPZihwKSA8IDApXHJcbiAgICAgICAgdFtwXSA9IHNbcF07XHJcbiAgICBpZiAocyAhPSBudWxsICYmIHR5cGVvZiBPYmplY3QuZ2V0T3duUHJvcGVydHlTeW1ib2xzID09PSBcImZ1bmN0aW9uXCIpXHJcbiAgICAgICAgZm9yICh2YXIgaSA9IDAsIHAgPSBPYmplY3QuZ2V0T3duUHJvcGVydHlTeW1ib2xzKHMpOyBpIDwgcC5sZW5ndGg7IGkrKykgaWYgKGUuaW5kZXhPZihwW2ldKSA8IDApXHJcbiAgICAgICAgICAgIHRbcFtpXV0gPSBzW3BbaV1dO1xyXG4gICAgcmV0dXJuIHQ7XHJcbn1cclxuXHJcbmV4cG9ydCBmdW5jdGlvbiBfX2RlY29yYXRlKGRlY29yYXRvcnMsIHRhcmdldCwga2V5LCBkZXNjKSB7XHJcbiAgICB2YXIgYyA9IGFyZ3VtZW50cy5sZW5ndGgsIHIgPSBjIDwgMyA/IHRhcmdldCA6IGRlc2MgPT09IG51bGwgPyBkZXNjID0gT2JqZWN0LmdldE93blByb3BlcnR5RGVzY3JpcHRvcih0YXJnZXQsIGtleSkgOiBkZXNjLCBkO1xyXG4gICAgaWYgKHR5cGVvZiBSZWZsZWN0ID09PSBcIm9iamVjdFwiICYmIHR5cGVvZiBSZWZsZWN0LmRlY29yYXRlID09PSBcImZ1bmN0aW9uXCIpIHIgPSBSZWZsZWN0LmRlY29yYXRlKGRlY29yYXRvcnMsIHRhcmdldCwga2V5LCBkZXNjKTtcclxuICAgIGVsc2UgZm9yICh2YXIgaSA9IGRlY29yYXRvcnMubGVuZ3RoIC0gMTsgaSA+PSAwOyBpLS0pIGlmIChkID0gZGVjb3JhdG9yc1tpXSkgciA9IChjIDwgMyA/IGQocikgOiBjID4gMyA/IGQodGFyZ2V0LCBrZXksIHIpIDogZCh0YXJnZXQsIGtleSkpIHx8IHI7XHJcbiAgICByZXR1cm4gYyA+IDMgJiYgciAmJiBPYmplY3QuZGVmaW5lUHJvcGVydHkodGFyZ2V0LCBrZXksIHIpLCByO1xyXG59XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gX19wYXJhbShwYXJhbUluZGV4LCBkZWNvcmF0b3IpIHtcclxuICAgIHJldHVybiBmdW5jdGlvbiAodGFyZ2V0LCBrZXkpIHsgZGVjb3JhdG9yKHRhcmdldCwga2V5LCBwYXJhbUluZGV4KTsgfVxyXG59XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gX19tZXRhZGF0YShtZXRhZGF0YUtleSwgbWV0YWRhdGFWYWx1ZSkge1xyXG4gICAgaWYgKHR5cGVvZiBSZWZsZWN0ID09PSBcIm9iamVjdFwiICYmIHR5cGVvZiBSZWZsZWN0Lm1ldGFkYXRhID09PSBcImZ1bmN0aW9uXCIpIHJldHVybiBSZWZsZWN0Lm1ldGFkYXRhKG1ldGFkYXRhS2V5LCBtZXRhZGF0YVZhbHVlKTtcclxufVxyXG5cclxuZXhwb3J0IGZ1bmN0aW9uIF9fYXdhaXRlcih0aGlzQXJnLCBfYXJndW1lbnRzLCBQLCBnZW5lcmF0b3IpIHtcclxuICAgIHJldHVybiBuZXcgKFAgfHwgKFAgPSBQcm9taXNlKSkoZnVuY3Rpb24gKHJlc29sdmUsIHJlamVjdCkge1xyXG4gICAgICAgIGZ1bmN0aW9uIGZ1bGZpbGxlZCh2YWx1ZSkgeyB0cnkgeyBzdGVwKGdlbmVyYXRvci5uZXh0KHZhbHVlKSk7IH0gY2F0Y2ggKGUpIHsgcmVqZWN0KGUpOyB9IH1cclxuICAgICAgICBmdW5jdGlvbiByZWplY3RlZCh2YWx1ZSkgeyB0cnkgeyBzdGVwKGdlbmVyYXRvcltcInRocm93XCJdKHZhbHVlKSk7IH0gY2F0Y2ggKGUpIHsgcmVqZWN0KGUpOyB9IH1cclxuICAgICAgICBmdW5jdGlvbiBzdGVwKHJlc3VsdCkgeyByZXN1bHQuZG9uZSA/IHJlc29sdmUocmVzdWx0LnZhbHVlKSA6IG5ldyBQKGZ1bmN0aW9uIChyZXNvbHZlKSB7IHJlc29sdmUocmVzdWx0LnZhbHVlKTsgfSkudGhlbihmdWxmaWxsZWQsIHJlamVjdGVkKTsgfVxyXG4gICAgICAgIHN0ZXAoKGdlbmVyYXRvciA9IGdlbmVyYXRvci5hcHBseSh0aGlzQXJnLCBfYXJndW1lbnRzIHx8IFtdKSkubmV4dCgpKTtcclxuICAgIH0pO1xyXG59XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gX19nZW5lcmF0b3IodGhpc0FyZywgYm9keSkge1xyXG4gICAgdmFyIF8gPSB7IGxhYmVsOiAwLCBzZW50OiBmdW5jdGlvbigpIHsgaWYgKHRbMF0gJiAxKSB0aHJvdyB0WzFdOyByZXR1cm4gdFsxXTsgfSwgdHJ5czogW10sIG9wczogW10gfSwgZiwgeSwgdCwgZztcclxuICAgIHJldHVybiBnID0geyBuZXh0OiB2ZXJiKDApLCBcInRocm93XCI6IHZlcmIoMSksIFwicmV0dXJuXCI6IHZlcmIoMikgfSwgdHlwZW9mIFN5bWJvbCA9PT0gXCJmdW5jdGlvblwiICYmIChnW1N5bWJvbC5pdGVyYXRvcl0gPSBmdW5jdGlvbigpIHsgcmV0dXJuIHRoaXM7IH0pLCBnO1xyXG4gICAgZnVuY3Rpb24gdmVyYihuKSB7IHJldHVybiBmdW5jdGlvbiAodikgeyByZXR1cm4gc3RlcChbbiwgdl0pOyB9OyB9XHJcbiAgICBmdW5jdGlvbiBzdGVwKG9wKSB7XHJcbiAgICAgICAgaWYgKGYpIHRocm93IG5ldyBUeXBlRXJyb3IoXCJHZW5lcmF0b3IgaXMgYWxyZWFkeSBleGVjdXRpbmcuXCIpO1xyXG4gICAgICAgIHdoaWxlIChfKSB0cnkge1xyXG4gICAgICAgICAgICBpZiAoZiA9IDEsIHkgJiYgKHQgPSBvcFswXSAmIDIgPyB5W1wicmV0dXJuXCJdIDogb3BbMF0gPyB5W1widGhyb3dcIl0gfHwgKCh0ID0geVtcInJldHVyblwiXSkgJiYgdC5jYWxsKHkpLCAwKSA6IHkubmV4dCkgJiYgISh0ID0gdC5jYWxsKHksIG9wWzFdKSkuZG9uZSkgcmV0dXJuIHQ7XHJcbiAgICAgICAgICAgIGlmICh5ID0gMCwgdCkgb3AgPSBbb3BbMF0gJiAyLCB0LnZhbHVlXTtcclxuICAgICAgICAgICAgc3dpdGNoIChvcFswXSkge1xyXG4gICAgICAgICAgICAgICAgY2FzZSAwOiBjYXNlIDE6IHQgPSBvcDsgYnJlYWs7XHJcbiAgICAgICAgICAgICAgICBjYXNlIDQ6IF8ubGFiZWwrKzsgcmV0dXJuIHsgdmFsdWU6IG9wWzFdLCBkb25lOiBmYWxzZSB9O1xyXG4gICAgICAgICAgICAgICAgY2FzZSA1OiBfLmxhYmVsKys7IHkgPSBvcFsxXTsgb3AgPSBbMF07IGNvbnRpbnVlO1xyXG4gICAgICAgICAgICAgICAgY2FzZSA3OiBvcCA9IF8ub3BzLnBvcCgpOyBfLnRyeXMucG9wKCk7IGNvbnRpbnVlO1xyXG4gICAgICAgICAgICAgICAgZGVmYXVsdDpcclxuICAgICAgICAgICAgICAgICAgICBpZiAoISh0ID0gXy50cnlzLCB0ID0gdC5sZW5ndGggPiAwICYmIHRbdC5sZW5ndGggLSAxXSkgJiYgKG9wWzBdID09PSA2IHx8IG9wWzBdID09PSAyKSkgeyBfID0gMDsgY29udGludWU7IH1cclxuICAgICAgICAgICAgICAgICAgICBpZiAob3BbMF0gPT09IDMgJiYgKCF0IHx8IChvcFsxXSA+IHRbMF0gJiYgb3BbMV0gPCB0WzNdKSkpIHsgXy5sYWJlbCA9IG9wWzFdOyBicmVhazsgfVxyXG4gICAgICAgICAgICAgICAgICAgIGlmIChvcFswXSA9PT0gNiAmJiBfLmxhYmVsIDwgdFsxXSkgeyBfLmxhYmVsID0gdFsxXTsgdCA9IG9wOyBicmVhazsgfVxyXG4gICAgICAgICAgICAgICAgICAgIGlmICh0ICYmIF8ubGFiZWwgPCB0WzJdKSB7IF8ubGFiZWwgPSB0WzJdOyBfLm9wcy5wdXNoKG9wKTsgYnJlYWs7IH1cclxuICAgICAgICAgICAgICAgICAgICBpZiAodFsyXSkgXy5vcHMucG9wKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgXy50cnlzLnBvcCgpOyBjb250aW51ZTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBvcCA9IGJvZHkuY2FsbCh0aGlzQXJnLCBfKTtcclxuICAgICAgICB9IGNhdGNoIChlKSB7IG9wID0gWzYsIGVdOyB5ID0gMDsgfSBmaW5hbGx5IHsgZiA9IHQgPSAwOyB9XHJcbiAgICAgICAgaWYgKG9wWzBdICYgNSkgdGhyb3cgb3BbMV07IHJldHVybiB7IHZhbHVlOiBvcFswXSA/IG9wWzFdIDogdm9pZCAwLCBkb25lOiB0cnVlIH07XHJcbiAgICB9XHJcbn1cclxuXHJcbmV4cG9ydCBmdW5jdGlvbiBfX2V4cG9ydFN0YXIobSwgZXhwb3J0cykge1xyXG4gICAgZm9yICh2YXIgcCBpbiBtKSBpZiAoIWV4cG9ydHMuaGFzT3duUHJvcGVydHkocCkpIGV4cG9ydHNbcF0gPSBtW3BdO1xyXG59XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gX192YWx1ZXMobykge1xyXG4gICAgdmFyIG0gPSB0eXBlb2YgU3ltYm9sID09PSBcImZ1bmN0aW9uXCIgJiYgb1tTeW1ib2wuaXRlcmF0b3JdLCBpID0gMDtcclxuICAgIGlmIChtKSByZXR1cm4gbS5jYWxsKG8pO1xyXG4gICAgcmV0dXJuIHtcclxuICAgICAgICBuZXh0OiBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgIGlmIChvICYmIGkgPj0gby5sZW5ndGgpIG8gPSB2b2lkIDA7XHJcbiAgICAgICAgICAgIHJldHVybiB7IHZhbHVlOiBvICYmIG9baSsrXSwgZG9uZTogIW8gfTtcclxuICAgICAgICB9XHJcbiAgICB9O1xyXG59XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gX19yZWFkKG8sIG4pIHtcclxuICAgIHZhciBtID0gdHlwZW9mIFN5bWJvbCA9PT0gXCJmdW5jdGlvblwiICYmIG9bU3ltYm9sLml0ZXJhdG9yXTtcclxuICAgIGlmICghbSkgcmV0dXJuIG87XHJcbiAgICB2YXIgaSA9IG0uY2FsbChvKSwgciwgYXIgPSBbXSwgZTtcclxuICAgIHRyeSB7XHJcbiAgICAgICAgd2hpbGUgKChuID09PSB2b2lkIDAgfHwgbi0tID4gMCkgJiYgIShyID0gaS5uZXh0KCkpLmRvbmUpIGFyLnB1c2goci52YWx1ZSk7XHJcbiAgICB9XHJcbiAgICBjYXRjaCAoZXJyb3IpIHsgZSA9IHsgZXJyb3I6IGVycm9yIH07IH1cclxuICAgIGZpbmFsbHkge1xyXG4gICAgICAgIHRyeSB7XHJcbiAgICAgICAgICAgIGlmIChyICYmICFyLmRvbmUgJiYgKG0gPSBpW1wicmV0dXJuXCJdKSkgbS5jYWxsKGkpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBmaW5hbGx5IHsgaWYgKGUpIHRocm93IGUuZXJyb3I7IH1cclxuICAgIH1cclxuICAgIHJldHVybiBhcjtcclxufVxyXG5cclxuZXhwb3J0IGZ1bmN0aW9uIF9fc3ByZWFkKCkge1xyXG4gICAgZm9yICh2YXIgYXIgPSBbXSwgaSA9IDA7IGkgPCBhcmd1bWVudHMubGVuZ3RoOyBpKyspXHJcbiAgICAgICAgYXIgPSBhci5jb25jYXQoX19yZWFkKGFyZ3VtZW50c1tpXSkpO1xyXG4gICAgcmV0dXJuIGFyO1xyXG59XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gX19hd2FpdCh2KSB7XHJcbiAgICByZXR1cm4gdGhpcyBpbnN0YW5jZW9mIF9fYXdhaXQgPyAodGhpcy52ID0gdiwgdGhpcykgOiBuZXcgX19hd2FpdCh2KTtcclxufVxyXG5cclxuZXhwb3J0IGZ1bmN0aW9uIF9fYXN5bmNHZW5lcmF0b3IodGhpc0FyZywgX2FyZ3VtZW50cywgZ2VuZXJhdG9yKSB7XHJcbiAgICBpZiAoIVN5bWJvbC5hc3luY0l0ZXJhdG9yKSB0aHJvdyBuZXcgVHlwZUVycm9yKFwiU3ltYm9sLmFzeW5jSXRlcmF0b3IgaXMgbm90IGRlZmluZWQuXCIpO1xyXG4gICAgdmFyIGcgPSBnZW5lcmF0b3IuYXBwbHkodGhpc0FyZywgX2FyZ3VtZW50cyB8fCBbXSksIGksIHEgPSBbXTtcclxuICAgIHJldHVybiBpID0ge30sIHZlcmIoXCJuZXh0XCIpLCB2ZXJiKFwidGhyb3dcIiksIHZlcmIoXCJyZXR1cm5cIiksIGlbU3ltYm9sLmFzeW5jSXRlcmF0b3JdID0gZnVuY3Rpb24gKCkgeyByZXR1cm4gdGhpczsgfSwgaTtcclxuICAgIGZ1bmN0aW9uIHZlcmIobikgeyBpZiAoZ1tuXSkgaVtuXSA9IGZ1bmN0aW9uICh2KSB7IHJldHVybiBuZXcgUHJvbWlzZShmdW5jdGlvbiAoYSwgYikgeyBxLnB1c2goW24sIHYsIGEsIGJdKSA+IDEgfHwgcmVzdW1lKG4sIHYpOyB9KTsgfTsgfVxyXG4gICAgZnVuY3Rpb24gcmVzdW1lKG4sIHYpIHsgdHJ5IHsgc3RlcChnW25dKHYpKTsgfSBjYXRjaCAoZSkgeyBzZXR0bGUocVswXVszXSwgZSk7IH0gfVxyXG4gICAgZnVuY3Rpb24gc3RlcChyKSB7IHIudmFsdWUgaW5zdGFuY2VvZiBfX2F3YWl0ID8gUHJvbWlzZS5yZXNvbHZlKHIudmFsdWUudikudGhlbihmdWxmaWxsLCByZWplY3QpIDogc2V0dGxlKHFbMF1bMl0sIHIpOyB9XHJcbiAgICBmdW5jdGlvbiBmdWxmaWxsKHZhbHVlKSB7IHJlc3VtZShcIm5leHRcIiwgdmFsdWUpOyB9XHJcbiAgICBmdW5jdGlvbiByZWplY3QodmFsdWUpIHsgcmVzdW1lKFwidGhyb3dcIiwgdmFsdWUpOyB9XHJcbiAgICBmdW5jdGlvbiBzZXR0bGUoZiwgdikgeyBpZiAoZih2KSwgcS5zaGlmdCgpLCBxLmxlbmd0aCkgcmVzdW1lKHFbMF1bMF0sIHFbMF1bMV0pOyB9XHJcbn1cclxuXHJcbmV4cG9ydCBmdW5jdGlvbiBfX2FzeW5jRGVsZWdhdG9yKG8pIHtcclxuICAgIHZhciBpLCBwO1xyXG4gICAgcmV0dXJuIGkgPSB7fSwgdmVyYihcIm5leHRcIiksIHZlcmIoXCJ0aHJvd1wiLCBmdW5jdGlvbiAoZSkgeyB0aHJvdyBlOyB9KSwgdmVyYihcInJldHVyblwiKSwgaVtTeW1ib2wuaXRlcmF0b3JdID0gZnVuY3Rpb24gKCkgeyByZXR1cm4gdGhpczsgfSwgaTtcclxuICAgIGZ1bmN0aW9uIHZlcmIobiwgZikgeyBpW25dID0gb1tuXSA/IGZ1bmN0aW9uICh2KSB7IHJldHVybiAocCA9ICFwKSA/IHsgdmFsdWU6IF9fYXdhaXQob1tuXSh2KSksIGRvbmU6IG4gPT09IFwicmV0dXJuXCIgfSA6IGYgPyBmKHYpIDogdjsgfSA6IGY7IH1cclxufVxyXG5cclxuZXhwb3J0IGZ1bmN0aW9uIF9fYXN5bmNWYWx1ZXMobykge1xyXG4gICAgaWYgKCFTeW1ib2wuYXN5bmNJdGVyYXRvcikgdGhyb3cgbmV3IFR5cGVFcnJvcihcIlN5bWJvbC5hc3luY0l0ZXJhdG9yIGlzIG5vdCBkZWZpbmVkLlwiKTtcclxuICAgIHZhciBtID0gb1tTeW1ib2wuYXN5bmNJdGVyYXRvcl0sIGk7XHJcbiAgICByZXR1cm4gbSA/IG0uY2FsbChvKSA6IChvID0gdHlwZW9mIF9fdmFsdWVzID09PSBcImZ1bmN0aW9uXCIgPyBfX3ZhbHVlcyhvKSA6IG9bU3ltYm9sLml0ZXJhdG9yXSgpLCBpID0ge30sIHZlcmIoXCJuZXh0XCIpLCB2ZXJiKFwidGhyb3dcIiksIHZlcmIoXCJyZXR1cm5cIiksIGlbU3ltYm9sLmFzeW5jSXRlcmF0b3JdID0gZnVuY3Rpb24gKCkgeyByZXR1cm4gdGhpczsgfSwgaSk7XHJcbiAgICBmdW5jdGlvbiB2ZXJiKG4pIHsgaVtuXSA9IG9bbl0gJiYgZnVuY3Rpb24gKHYpIHsgcmV0dXJuIG5ldyBQcm9taXNlKGZ1bmN0aW9uIChyZXNvbHZlLCByZWplY3QpIHsgdiA9IG9bbl0odiksIHNldHRsZShyZXNvbHZlLCByZWplY3QsIHYuZG9uZSwgdi52YWx1ZSk7IH0pOyB9OyB9XHJcbiAgICBmdW5jdGlvbiBzZXR0bGUocmVzb2x2ZSwgcmVqZWN0LCBkLCB2KSB7IFByb21pc2UucmVzb2x2ZSh2KS50aGVuKGZ1bmN0aW9uKHYpIHsgcmVzb2x2ZSh7IHZhbHVlOiB2LCBkb25lOiBkIH0pOyB9LCByZWplY3QpOyB9XHJcbn1cclxuXHJcbmV4cG9ydCBmdW5jdGlvbiBfX21ha2VUZW1wbGF0ZU9iamVjdChjb29rZWQsIHJhdykge1xyXG4gICAgaWYgKE9iamVjdC5kZWZpbmVQcm9wZXJ0eSkgeyBPYmplY3QuZGVmaW5lUHJvcGVydHkoY29va2VkLCBcInJhd1wiLCB7IHZhbHVlOiByYXcgfSk7IH0gZWxzZSB7IGNvb2tlZC5yYXcgPSByYXc7IH1cclxuICAgIHJldHVybiBjb29rZWQ7XHJcbn07XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gX19pbXBvcnRTdGFyKG1vZCkge1xyXG4gICAgaWYgKG1vZCAmJiBtb2QuX19lc01vZHVsZSkgcmV0dXJuIG1vZDtcclxuICAgIHZhciByZXN1bHQgPSB7fTtcclxuICAgIGlmIChtb2QgIT0gbnVsbCkgZm9yICh2YXIgayBpbiBtb2QpIGlmIChPYmplY3QuaGFzT3duUHJvcGVydHkuY2FsbChtb2QsIGspKSByZXN1bHRba10gPSBtb2Rba107XHJcbiAgICByZXN1bHQuZGVmYXVsdCA9IG1vZDtcclxuICAgIHJldHVybiByZXN1bHQ7XHJcbn1cclxuXHJcbmV4cG9ydCBmdW5jdGlvbiBfX2ltcG9ydERlZmF1bHQobW9kKSB7XHJcbiAgICByZXR1cm4gKG1vZCAmJiBtb2QuX19lc01vZHVsZSkgPyBtb2QgOiB7IGRlZmF1bHQ6IG1vZCB9O1xyXG59XHJcbiIsImltcG9ydCB7IEluamVjdCwgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBjb25maWcsIElDb25maWcgfSBmcm9tICcuL2NvbmZpZyc7XHJcblxyXG5ASW5qZWN0YWJsZSgpXHJcbmV4cG9ydCBjbGFzcyBNYXNrQXBwbGllclNlcnZpY2Uge1xyXG4gIHB1YmxpYyBkcm9wU3BlY2lhbENoYXJhY3RlcnM6IElDb25maWdbJ2Ryb3BTcGVjaWFsQ2hhcmFjdGVycyddO1xyXG4gIHB1YmxpYyBzaG93VGVtcGxhdGU6IElDb25maWdbJ3Nob3dUZW1wbGF0ZSddO1xyXG4gIHB1YmxpYyBjbGVhcklmTm90TWF0Y2g6IElDb25maWdbJ2NsZWFySWZOb3RNYXRjaCddO1xyXG4gIHB1YmxpYyBtYXNrRXhwcmVzc2lvbiA9ICcnO1xyXG4gIHB1YmxpYyBtYXNrU3BlY2lhbENoYXJhY3RlcnM6IElDb25maWdbJ3NwZWNpYWxDaGFyYWN0ZXJzJ107XHJcbiAgcHVibGljIG1hc2tBdmFpbGFibGVQYXR0ZXJuczogSUNvbmZpZ1sncGF0dGVybnMnXTtcclxuICBwdWJsaWMgcHJlZml4OiBJQ29uZmlnWydwcmVmaXgnXTtcclxuICBwdWJsaWMgc3VmaXg6IElDb25maWdbJ3N1Zml4J107XHJcbiAgcHVibGljIGN1c3RvbVBhdHRlcm46IElDb25maWdbJ3BhdHRlcm5zJ107XHJcblxyXG4gIHByaXZhdGUgX3NoaWZ0OiBTZXQ8bnVtYmVyPjtcclxuXHJcbiAgcHVibGljIGNvbnN0cnVjdG9yKEBJbmplY3QoY29uZmlnKSBwcm90ZWN0ZWQgX2NvbmZpZzogSUNvbmZpZykge1xyXG4gICAgdGhpcy5fc2hpZnQgPSBuZXcgU2V0KCk7XHJcbiAgICB0aGlzLm1hc2tTcGVjaWFsQ2hhcmFjdGVycyA9IHRoaXMuX2NvbmZpZyEuc3BlY2lhbENoYXJhY3RlcnM7XHJcbiAgICB0aGlzLm1hc2tBdmFpbGFibGVQYXR0ZXJucyA9IHRoaXMuX2NvbmZpZy5wYXR0ZXJucztcclxuICAgIHRoaXMuY2xlYXJJZk5vdE1hdGNoID0gdGhpcy5fY29uZmlnLmNsZWFySWZOb3RNYXRjaDtcclxuICAgIHRoaXMuZHJvcFNwZWNpYWxDaGFyYWN0ZXJzID0gdGhpcy5fY29uZmlnLmRyb3BTcGVjaWFsQ2hhcmFjdGVycztcclxuICAgIHRoaXMubWFza1NwZWNpYWxDaGFyYWN0ZXJzID0gdGhpcy5fY29uZmlnIS5zcGVjaWFsQ2hhcmFjdGVycztcclxuICAgIHRoaXMubWFza0F2YWlsYWJsZVBhdHRlcm5zID0gdGhpcy5fY29uZmlnLnBhdHRlcm5zO1xyXG4gICAgdGhpcy5wcmVmaXggPSB0aGlzLl9jb25maWcucHJlZml4O1xyXG4gICAgdGhpcy5zdWZpeCA9IHRoaXMuX2NvbmZpZy5zdWZpeDtcclxuICB9XHJcbiAgLy8gdHNsaW50OmRpc2FibGUtbmV4dC1saW5lOm5vLWFueVxyXG4gIHB1YmxpYyBhcHBseU1hc2tXaXRoUGF0dGVybihcclxuICAgIGlucHV0VmFsdWU6IHN0cmluZyxcclxuICAgIG1hc2tBbmRQYXR0ZXJuOiBbc3RyaW5nLCBJQ29uZmlnWydwYXR0ZXJucyddXVxyXG4gICk6IHN0cmluZyB7XHJcbiAgICBjb25zdCBbbWFzaywgY3VzdG9tUGF0dGVybl0gPSBtYXNrQW5kUGF0dGVybjtcclxuICAgIHRoaXMuY3VzdG9tUGF0dGVybiA9IGN1c3RvbVBhdHRlcm47XHJcbiAgICByZXR1cm4gdGhpcy5hcHBseU1hc2soaW5wdXRWYWx1ZSwgbWFzayk7XHJcbiAgfVxyXG4gIHB1YmxpYyBhcHBseU1hc2soXHJcbiAgICBpbnB1dFZhbHVlOiBzdHJpbmcsXHJcbiAgICBtYXNrRXhwcmVzc2lvbjogc3RyaW5nLFxyXG4gICAgcG9zaXRpb246IG51bWJlciA9IDAsXHJcbiAgICBjYjogRnVuY3Rpb24gPSAoKSA9PiB7fVxyXG4gICk6IHN0cmluZyB7XHJcbiAgICBpZiAoXHJcbiAgICAgIGlucHV0VmFsdWUgPT09IHVuZGVmaW5lZCB8fFxyXG4gICAgICBpbnB1dFZhbHVlID09PSBudWxsIHx8XHJcbiAgICAgIG1hc2tFeHByZXNzaW9uID09PSB1bmRlZmluZWRcclxuICAgICkge1xyXG4gICAgICByZXR1cm4gJyc7XHJcbiAgICB9XHJcblxyXG4gICAgbGV0IGN1cnNvciA9IDA7XHJcbiAgICBsZXQgcmVzdWx0ID0gYGA7XHJcbiAgICBsZXQgbXVsdGkgPSBmYWxzZTtcclxuXHJcbiAgICBpZiAoaW5wdXRWYWx1ZS5zbGljZSgwLCB0aGlzLnByZWZpeC5sZW5ndGgpID09PSB0aGlzLnByZWZpeCkge1xyXG4gICAgICBpbnB1dFZhbHVlID0gaW5wdXRWYWx1ZS5zbGljZSh0aGlzLnByZWZpeC5sZW5ndGgsIGlucHV0VmFsdWUubGVuZ3RoKTtcclxuICAgIH1cclxuXHJcbiAgICBjb25zdCBpbnB1dEFycmF5OiBzdHJpbmdbXSA9IGlucHV0VmFsdWUudG9TdHJpbmcoKS5zcGxpdCgnJyk7XHJcblxyXG4gICAgLy8gdHNsaW50OmRpc2FibGUtbmV4dC1saW5lXHJcbiAgICBmb3IgKFxyXG4gICAgICBsZXQgaTogbnVtYmVyID0gMCwgaW5wdXRTeW1ib2w6IHN0cmluZyA9IGlucHV0QXJyYXlbMF07XHJcbiAgICAgIGkgPCBpbnB1dEFycmF5Lmxlbmd0aDtcclxuICAgICAgaSsrLCBpbnB1dFN5bWJvbCA9IGlucHV0QXJyYXlbaV1cclxuICAgICkge1xyXG4gICAgICBpZiAoY3Vyc29yID09PSBtYXNrRXhwcmVzc2lvbi5sZW5ndGgpIHtcclxuICAgICAgICBicmVhaztcclxuICAgICAgfVxyXG4gICAgICBpZiAoXHJcbiAgICAgICAgdGhpcy5fY2hlY2tTeW1ib2xNYXNrKGlucHV0U3ltYm9sLCBtYXNrRXhwcmVzc2lvbltjdXJzb3JdKSAmJlxyXG4gICAgICAgIG1hc2tFeHByZXNzaW9uW2N1cnNvciArIDFdID09PSAnPydcclxuICAgICAgKSB7XHJcbiAgICAgICAgcmVzdWx0ICs9IGlucHV0U3ltYm9sO1xyXG4gICAgICAgIGN1cnNvciArPSAyO1xyXG4gICAgICB9IGVsc2UgaWYgKFxyXG4gICAgICAgIG1hc2tFeHByZXNzaW9uW2N1cnNvciArIDFdID09PSAnKicgJiZcclxuICAgICAgICBtdWx0aSAmJlxyXG4gICAgICAgIHRoaXMuX2NoZWNrU3ltYm9sTWFzayhpbnB1dFN5bWJvbCwgbWFza0V4cHJlc3Npb25bY3Vyc29yICsgMl0pXHJcbiAgICAgICkge1xyXG4gICAgICAgIHJlc3VsdCArPSBpbnB1dFN5bWJvbDtcclxuICAgICAgICBjdXJzb3IgKz0gMztcclxuICAgICAgICBtdWx0aSA9IGZhbHNlO1xyXG4gICAgICB9IGVsc2UgaWYgKFxyXG4gICAgICAgIHRoaXMuX2NoZWNrU3ltYm9sTWFzayhpbnB1dFN5bWJvbCwgbWFza0V4cHJlc3Npb25bY3Vyc29yXSkgJiZcclxuICAgICAgICBtYXNrRXhwcmVzc2lvbltjdXJzb3IgKyAxXSA9PT0gJyonXHJcbiAgICAgICkge1xyXG4gICAgICAgIHJlc3VsdCArPSBpbnB1dFN5bWJvbDtcclxuICAgICAgICBtdWx0aSA9IHRydWU7XHJcbiAgICAgIH0gZWxzZSBpZiAoXHJcbiAgICAgICAgbWFza0V4cHJlc3Npb25bY3Vyc29yICsgMV0gPT09ICc/JyAmJlxyXG4gICAgICAgIHRoaXMuX2NoZWNrU3ltYm9sTWFzayhpbnB1dFN5bWJvbCwgbWFza0V4cHJlc3Npb25bY3Vyc29yICsgMl0pXHJcbiAgICAgICkge1xyXG4gICAgICAgIHJlc3VsdCArPSBpbnB1dFN5bWJvbDtcclxuICAgICAgICBjdXJzb3IgKz0gMztcclxuICAgICAgfSBlbHNlIGlmICh0aGlzLl9jaGVja1N5bWJvbE1hc2soaW5wdXRTeW1ib2wsIG1hc2tFeHByZXNzaW9uW2N1cnNvcl0pKSB7XHJcbiAgICAgICAgaWYgKG1hc2tFeHByZXNzaW9uW2N1cnNvcl0gPT09ICdkJykge1xyXG4gICAgICAgICAgaWYgKE51bWJlcihpbnB1dFN5bWJvbCkgPiAzKSB7XHJcbiAgICAgICAgICAgIHJlc3VsdCArPSAwO1xyXG4gICAgICAgICAgICBjdXJzb3IgKz0gMTtcclxuICAgICAgICAgICAgY29uc3Qgc2hpZnRTdGVwOiBudW1iZXIgPSAvXFwqfFxcPy9nLnRlc3QoXHJcbiAgICAgICAgICAgICAgbWFza0V4cHJlc3Npb24uc2xpY2UoMCwgY3Vyc29yKVxyXG4gICAgICAgICAgICApXHJcbiAgICAgICAgICAgICAgPyBpbnB1dEFycmF5Lmxlbmd0aFxyXG4gICAgICAgICAgICAgIDogY3Vyc29yO1xyXG4gICAgICAgICAgICB0aGlzLl9zaGlmdC5hZGQoc2hpZnRTdGVwICsgdGhpcy5wcmVmaXgubGVuZ3RoIHx8IDApO1xyXG4gICAgICAgICAgICBpLS07XHJcbiAgICAgICAgICAgIGNvbnRpbnVlO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAobWFza0V4cHJlc3Npb25bY3Vyc29yIC0gMV0gPT09ICdkJykge1xyXG4gICAgICAgICAgaWYgKE51bWJlcihpbnB1dFZhbHVlLnNsaWNlKGN1cnNvciAtIDEsIGN1cnNvciArIDEpKSA+IDMxKSB7XHJcbiAgICAgICAgICAgIGNvbnRpbnVlO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAobWFza0V4cHJlc3Npb25bY3Vyc29yXSA9PT0gJ20nKSB7XHJcbiAgICAgICAgICBpZiAoTnVtYmVyKGlucHV0U3ltYm9sKSA+IDEpIHtcclxuICAgICAgICAgICAgcmVzdWx0ICs9IDA7XHJcbiAgICAgICAgICAgIGN1cnNvciArPSAxO1xyXG4gICAgICAgICAgICBjb25zdCBzaGlmdFN0ZXA6IG51bWJlciA9IC9cXCp8XFw/L2cudGVzdChcclxuICAgICAgICAgICAgICBtYXNrRXhwcmVzc2lvbi5zbGljZSgwLCBjdXJzb3IpXHJcbiAgICAgICAgICAgIClcclxuICAgICAgICAgICAgICA/IGlucHV0QXJyYXkubGVuZ3RoXHJcbiAgICAgICAgICAgICAgOiBjdXJzb3I7XHJcbiAgICAgICAgICAgIHRoaXMuX3NoaWZ0LmFkZChzaGlmdFN0ZXAgKyB0aGlzLnByZWZpeC5sZW5ndGggfHwgMCk7XHJcbiAgICAgICAgICAgIGktLTtcclxuICAgICAgICAgICAgY29udGludWU7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmIChtYXNrRXhwcmVzc2lvbltjdXJzb3IgLSAxXSA9PT0gJ20nKSB7XHJcbiAgICAgICAgICBpZiAoTnVtYmVyKGlucHV0VmFsdWUuc2xpY2UoY3Vyc29yIC0gMSwgY3Vyc29yICsgMSkpID4gMTIpIHtcclxuICAgICAgICAgICAgY29udGludWU7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJlc3VsdCArPSBpbnB1dFN5bWJvbDtcclxuICAgICAgICBjdXJzb3IrKztcclxuICAgICAgfSBlbHNlIGlmIChcclxuICAgICAgICB0aGlzLm1hc2tTcGVjaWFsQ2hhcmFjdGVycy5pbmRleE9mKG1hc2tFeHByZXNzaW9uW2N1cnNvcl0pICE9PSAtMVxyXG4gICAgICApIHtcclxuICAgICAgICByZXN1bHQgKz0gbWFza0V4cHJlc3Npb25bY3Vyc29yXTtcclxuICAgICAgICBjdXJzb3IrKztcclxuICAgICAgICBjb25zdCBzaGlmdFN0ZXA6IG51bWJlciA9IC9cXCp8XFw/L2cudGVzdChtYXNrRXhwcmVzc2lvbi5zbGljZSgwLCBjdXJzb3IpKVxyXG4gICAgICAgICAgPyBpbnB1dEFycmF5Lmxlbmd0aFxyXG4gICAgICAgICAgOiBjdXJzb3I7XHJcbiAgICAgICAgdGhpcy5fc2hpZnQuYWRkKHNoaWZ0U3RlcCArIHRoaXMucHJlZml4Lmxlbmd0aCB8fCAwKTtcclxuICAgICAgICBpLS07XHJcbiAgICAgIH0gZWxzZSBpZiAoXHJcbiAgICAgICAgdGhpcy5tYXNrU3BlY2lhbENoYXJhY3RlcnMuaW5kZXhPZihpbnB1dFN5bWJvbCkgPiAtMSAmJlxyXG4gICAgICAgIHRoaXMubWFza0F2YWlsYWJsZVBhdHRlcm5zW21hc2tFeHByZXNzaW9uW2N1cnNvcl1dICYmXHJcbiAgICAgICAgdGhpcy5tYXNrQXZhaWxhYmxlUGF0dGVybnNbbWFza0V4cHJlc3Npb25bY3Vyc29yXV0ub3B0aW9uYWxcclxuICAgICAgKSB7XHJcbiAgICAgICAgY3Vyc29yKys7XHJcbiAgICAgICAgaS0tO1xyXG4gICAgICB9IGVsc2UgaWYgKFxyXG4gICAgICAgIHRoaXMubWFza0V4cHJlc3Npb25bY3Vyc29yICsgMV0gPT09ICcqJyAmJlxyXG4gICAgICAgIHRoaXMuX2ZpbmRTcGVjaWFsQ2hhcih0aGlzLm1hc2tFeHByZXNzaW9uW2N1cnNvciArIDJdKSAmJlxyXG4gICAgICAgIHRoaXMuX2ZpbmRTcGVjaWFsQ2hhcihpbnB1dFN5bWJvbCkgPT09IHRoaXMubWFza0V4cHJlc3Npb25bY3Vyc29yICsgMl1cclxuICAgICAgKSB7XHJcbiAgICAgICAgY3Vyc29yICs9IDM7XHJcbiAgICAgICAgcmVzdWx0ICs9IGlucHV0U3ltYm9sO1xyXG4gICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgaWYgKFxyXG4gICAgICByZXN1bHQubGVuZ3RoICsgMSA9PT0gbWFza0V4cHJlc3Npb24ubGVuZ3RoICYmXHJcbiAgICAgIHRoaXMubWFza1NwZWNpYWxDaGFyYWN0ZXJzLmluZGV4T2YoXHJcbiAgICAgICAgbWFza0V4cHJlc3Npb25bbWFza0V4cHJlc3Npb24ubGVuZ3RoIC0gMV1cclxuICAgICAgKSAhPT0gLTFcclxuICAgICkge1xyXG4gICAgICByZXN1bHQgKz0gbWFza0V4cHJlc3Npb25bbWFza0V4cHJlc3Npb24ubGVuZ3RoIC0gMV07XHJcbiAgICB9XHJcblxyXG4gICAgbGV0IHNoaWZ0ID0gMTtcclxuICAgIGxldCBuZXdQb3NpdGlvbjogbnVtYmVyID0gcG9zaXRpb24gKyAxO1xyXG5cclxuICAgIHdoaWxlICh0aGlzLl9zaGlmdC5oYXMobmV3UG9zaXRpb24pKSB7XHJcbiAgICAgIHNoaWZ0Kys7XHJcbiAgICAgIG5ld1Bvc2l0aW9uKys7XHJcbiAgICB9XHJcblxyXG4gICAgY2IodGhpcy5fc2hpZnQuaGFzKHBvc2l0aW9uKSA/IHNoaWZ0IDogMCk7XHJcbiAgICBsZXQgcmVzID0gYCR7dGhpcy5wcmVmaXh9JHtyZXN1bHR9YDtcclxuICAgIHJlcyA9XHJcbiAgICAgIHRoaXMuc3VmaXggJiYgY3Vyc29yID09PSBtYXNrRXhwcmVzc2lvbi5sZW5ndGhcclxuICAgICAgICA/IGAke3RoaXMucHJlZml4fSR7cmVzdWx0fSR7dGhpcy5zdWZpeH1gXHJcbiAgICAgICAgOiBgJHt0aGlzLnByZWZpeH0ke3Jlc3VsdH1gO1xyXG4gICAgcmV0dXJuIHJlcztcclxuICB9XHJcbiAgcHVibGljIF9maW5kU3BlY2lhbENoYXIoaW5wdXRTeW1ib2w6IHN0cmluZyk6IHVuZGVmaW5lZCB8IHN0cmluZyB7XHJcbiAgICBjb25zdCBzeW1ib2w6IHN0cmluZyB8IHVuZGVmaW5lZCA9IHRoaXMubWFza1NwZWNpYWxDaGFyYWN0ZXJzLmZpbmQoXHJcbiAgICAgICh2YWw6IHN0cmluZykgPT4gdmFsID09PSBpbnB1dFN5bWJvbFxyXG4gICAgKTtcclxuICAgIHJldHVybiBzeW1ib2w7XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIF9jaGVja1N5bWJvbE1hc2soaW5wdXRTeW1ib2w6IHN0cmluZywgbWFza1N5bWJvbDogc3RyaW5nKTogYm9vbGVhbiB7XHJcbiAgICB0aGlzLm1hc2tBdmFpbGFibGVQYXR0ZXJucyA9IHRoaXMuY3VzdG9tUGF0dGVyblxyXG4gICAgICA/IHRoaXMuY3VzdG9tUGF0dGVyblxyXG4gICAgICA6IHRoaXMubWFza0F2YWlsYWJsZVBhdHRlcm5zO1xyXG4gICAgcmV0dXJuIChcclxuICAgICAgdGhpcy5tYXNrQXZhaWxhYmxlUGF0dGVybnNbbWFza1N5bWJvbF0gJiZcclxuICAgICAgdGhpcy5tYXNrQXZhaWxhYmxlUGF0dGVybnNbbWFza1N5bWJvbF0ucGF0dGVybiAmJlxyXG4gICAgICB0aGlzLm1hc2tBdmFpbGFibGVQYXR0ZXJuc1ttYXNrU3ltYm9sXS5wYXR0ZXJuLnRlc3QoaW5wdXRTeW1ib2wpXHJcbiAgICApO1xyXG4gIH1cclxufVxyXG4iLCJpbXBvcnQgeyBFbGVtZW50UmVmLCBJbmplY3QsIEluamVjdGFibGUsIFJlbmRlcmVyMiB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBjb25maWcsIElDb25maWcgfSBmcm9tICcuL2NvbmZpZyc7XHJcbmltcG9ydCB7IERPQ1VNRU5UIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcclxuaW1wb3J0IHsgTWFza0FwcGxpZXJTZXJ2aWNlIH0gZnJvbSAnLi9tYXNrLWFwcGxpZXIuc2VydmljZSc7XHJcbmltcG9ydCB7IE5nQ29udHJvbCB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcclxuXHJcbkBJbmplY3RhYmxlKClcclxuZXhwb3J0IGNsYXNzIE1hc2tTZXJ2aWNlIGV4dGVuZHMgTWFza0FwcGxpZXJTZXJ2aWNlIHtcclxuICBwdWJsaWMgbWFza0V4cHJlc3Npb24gPSAnJztcclxuICBwdWJsaWMgaXNOdW1iZXJWYWx1ZSA9IGZhbHNlO1xyXG4gIHB1YmxpYyBzaG93TWFza1R5cGVkID0gZmFsc2U7XHJcbiAgcHVibGljIG1hc2tJc1Nob3duID0gJyc7XHJcbiAgcHJpdmF0ZSBfZm9ybUVsZW1lbnQ6IEhUTUxJbnB1dEVsZW1lbnQ7XHJcbiAgcHJpdmF0ZSB1bm1hc2tlZFZhbHVlOiBzdHJpbmcgfCBudW1iZXI7XHJcbiAgcHVibGljIG9uVG91Y2ggPSAoKSA9PiB7fTtcclxuICBwdWJsaWMgY29uc3RydWN0b3IoXHJcbiAgICAvLyB0c2xpbnQ6ZGlzYWJsZS1uZXh0LWxpbmVcclxuICAgIEBJbmplY3QoRE9DVU1FTlQpIHByaXZhdGUgZG9jdW1lbnQ6IGFueSxcclxuICAgIEBJbmplY3QoY29uZmlnKSBwcm90ZWN0ZWQgX2NvbmZpZzogSUNvbmZpZyxcclxuICAgIHByaXZhdGUgX2VsZW1lbnRSZWY6IEVsZW1lbnRSZWYsXHJcbiAgICBwcml2YXRlIF9yZW5kZXJlcjogUmVuZGVyZXIyLFxyXG4gICAgcHJpdmF0ZSBfbmdDb250cm9sOiBOZ0NvbnRyb2xcclxuICApIHtcclxuICAgIHN1cGVyKF9jb25maWcpO1xyXG4gICAgdGhpcy5zZXRGb3JtRWxlbWVudChfZWxlbWVudFJlZi5uYXRpdmVFbGVtZW50KTtcclxuXHJcbiAgICBzZXRUaW1lb3V0KCgpID0+IHtcclxuICAgICAgaWYgKHRoaXMuX2Zvcm1FbGVtZW50LmxvY2FsTmFtZSAhPT0gJ2lucHV0Jykge1xyXG4gICAgICAgIGNvbnN0IGlucHV0RWwgPSB0aGlzLl9lbGVtZW50UmVmLm5hdGl2ZUVsZW1lbnQucXVlcnlTZWxlY3RvcignaW5wdXQnKTtcclxuICAgICAgICBpZiAoaW5wdXRFbCAhPSBudWxsKSB7XHJcbiAgICAgICAgICB0aGlzLnNldEZvcm1FbGVtZW50KGlucHV0RWwpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICBjb25zb2xlLndhcm4oXHJcbiAgICAgICAgICAgICdtYXNrLXNlcnZpY2U6IENvdWxkIG5vdCBmaW5kIElucHV0IEVsZW1lbnQuICBQbGVhc2UgbWFrZSBzdXJlIG9uZSBpcyBwcmVzZW50LidcclxuICAgICAgICAgICk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcblxyXG4gICAgICB0aGlzLl9uZ0NvbnRyb2wudmFsdWVDaGFuZ2VzLnN1YnNjcmliZSgodmFsdWU6IHN0cmluZykgPT4ge1xyXG4gICAgICAgIHRoaXMuX29uQ29udHJvbFZhbHVlQ2hhbmdlKHZhbHVlKTtcclxuICAgICAgfSk7XHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIHB1YmxpYyBzZXRGb3JtRWxlbWVudChlbDogSFRNTElucHV0RWxlbWVudCkge1xyXG4gICAgdGhpcy5fZm9ybUVsZW1lbnQgPSBlbDtcclxuICB9XHJcblxyXG4gIHB1YmxpYyBhcHBseU1hc2soXHJcbiAgICBpbnB1dFZhbHVlOiBzdHJpbmcsXHJcbiAgICBtYXNrRXhwcmVzc2lvbjogc3RyaW5nLFxyXG4gICAgcG9zaXRpb246IG51bWJlciA9IDAsXHJcbiAgICBjYjogRnVuY3Rpb24gPSAoKSA9PiB7fVxyXG4gICk6IHN0cmluZyB7XHJcbiAgICB0aGlzLm1hc2tJc1Nob3duID0gdGhpcy5zaG93TWFza1R5cGVkXHJcbiAgICAgID8gdGhpcy5tYXNrRXhwcmVzc2lvbi5yZXBsYWNlKC9bMC05XS9nLCAnXycpXHJcbiAgICAgIDogJyc7XHJcbiAgICBpZiAoIWlucHV0VmFsdWUgJiYgdGhpcy5zaG93TWFza1R5cGVkKSB7XHJcbiAgICAgIHJldHVybiB0aGlzLnByZWZpeCArIHRoaXMubWFza0lzU2hvd247XHJcbiAgICB9XHJcblxyXG4gICAgY29uc3QgcmVzdWx0ID0gc3VwZXIuYXBwbHlNYXNrKGlucHV0VmFsdWUsIG1hc2tFeHByZXNzaW9uLCBwb3NpdGlvbiwgY2IpO1xyXG4gICAgdGhpcy51bm1hc2tlZFZhbHVlID0gdGhpcy5nZXRVbm1hc2tlZFZhbHVlKHJlc3VsdCk7XHJcblxyXG4gICAgcmV0dXJuIHRoaXMuX2FwcGx5TWFza1Jlc3VsdChyZXN1bHQpO1xyXG4gIH1cclxuXHJcbiAgcHVibGljIGFwcGx5VmFsdWVDaGFuZ2VzKFxyXG4gICAgcG9zaXRpb246IG51bWJlciA9IDAsXHJcbiAgICBjYjogRnVuY3Rpb24gPSAoKSA9PiB7fVxyXG4gICk6IHZvaWQge1xyXG4gICAgY29uc3QgbWFza2VkSW5wdXQ6IHN0cmluZyB8IG51bWJlciA9IHRoaXMuYXBwbHlNYXNrKFxyXG4gICAgICB0aGlzLl9mb3JtRWxlbWVudC52YWx1ZSxcclxuICAgICAgdGhpcy5tYXNrRXhwcmVzc2lvbixcclxuICAgICAgcG9zaXRpb24sXHJcbiAgICAgIGNiXHJcbiAgICApO1xyXG4gICAgdGhpcy5fZm9ybUVsZW1lbnQudmFsdWUgPSBtYXNrZWRJbnB1dDtcclxuICAgIGlmICh0aGlzLl9mb3JtRWxlbWVudCA9PT0gdGhpcy5kb2N1bWVudC5hY3RpdmVFbGVtZW50KSB7XHJcbiAgICAgIHJldHVybjtcclxuICAgIH1cclxuICAgIHRoaXMuY2xlYXJJZk5vdE1hdGNoRm4oKTtcclxuICB9XHJcblxyXG4gIHB1YmxpYyBzaG93TWFza0luSW5wdXQoKTogdm9pZCB7XHJcbiAgICBpZiAodGhpcy5zaG93TWFza1R5cGVkKSB7XHJcbiAgICAgIHRoaXMubWFza0lzU2hvd24gPSB0aGlzLm1hc2tFeHByZXNzaW9uLnJlcGxhY2UoL1swLTldL2csICdfJyk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgY2xlYXJJZk5vdE1hdGNoRm4oKTogdm9pZCB7XHJcbiAgICBjb25zb2xlLmxvZygnY2xlYXItaWYtbm90LW1hdGNoZWQnKTtcclxuICAgIGlmIChcclxuICAgICAgdGhpcy5jbGVhcklmTm90TWF0Y2ggPT09IHRydWUgJiZcclxuICAgICAgdGhpcy5tYXNrRXhwcmVzc2lvbi5sZW5ndGggIT09IHRoaXMuX2Zvcm1FbGVtZW50LnZhbHVlLmxlbmd0aFxyXG4gICAgKSB7XHJcbiAgICAgIHRoaXMuc2V0VmFsdWUoJycpO1xyXG4gICAgICB0aGlzLmFwcGx5TWFzayh0aGlzLl9mb3JtRWxlbWVudC52YWx1ZSwgdGhpcy5tYXNrRXhwcmVzc2lvbik7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgc2V0VmFsdWUodmFsdWU6IHN0cmluZykge1xyXG4gICAgdGhpcy51bm1hc2tlZFZhbHVlID0gdGhpcy5nZXRVbm1hc2tlZFZhbHVlKHZhbHVlKTtcclxuICAgIHRoaXMuX25nQ29udHJvbC5jb250cm9sLnNldFZhbHVlKHZhbHVlKTtcclxuICB9XHJcbiAgcHVibGljIHNldEZvcm1FbGVtZW50UHJvcGVydHkobmFtZTogc3RyaW5nLCB2YWx1ZTogc3RyaW5nIHwgYm9vbGVhbikge1xyXG4gICAgaWYgKHRoaXMuX2Zvcm1FbGVtZW50KSB7XHJcbiAgICAgIHRoaXMuX3JlbmRlcmVyLnNldFByb3BlcnR5KHRoaXMuX2Zvcm1FbGVtZW50LCBuYW1lLCB2YWx1ZSk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgZ2V0VW5tYXNrZWRWYWx1ZShyZXN1bHQ6IHN0cmluZyk6IHN0cmluZyB8IG51bWJlciB7XHJcbiAgICBjb25zdCByZXN1bHROb1N1ZmZpeE9yUHJlZml4ID0gdGhpcy5fcmVtb3ZlU3VmaXgoXHJcbiAgICAgIHRoaXMuX3JlbW92ZVByZWZpeChyZXN1bHQpXHJcbiAgICApO1xyXG4gICAgbGV0IGNoYW5nZVZhbHVlOiBzdHJpbmcgfCBudW1iZXIgPSByZXN1bHROb1N1ZmZpeE9yUHJlZml4O1xyXG5cclxuICAgIGlmIChBcnJheS5pc0FycmF5KHRoaXMuZHJvcFNwZWNpYWxDaGFyYWN0ZXJzKSkge1xyXG4gICAgICBjaGFuZ2VWYWx1ZSA9IHRoaXMuX3JlbW92ZU1hc2soXHJcbiAgICAgICAgcmVzdWx0Tm9TdWZmaXhPclByZWZpeCxcclxuICAgICAgICB0aGlzLmRyb3BTcGVjaWFsQ2hhcmFjdGVyc1xyXG4gICAgICApO1xyXG4gICAgfSBlbHNlIGlmICh0aGlzLmRyb3BTcGVjaWFsQ2hhcmFjdGVycykge1xyXG4gICAgICBjaGFuZ2VWYWx1ZSA9IHRoaXMuX3JlbW92ZU1hc2soXHJcbiAgICAgICAgcmVzdWx0Tm9TdWZmaXhPclByZWZpeCxcclxuICAgICAgICB0aGlzLm1hc2tTcGVjaWFsQ2hhcmFjdGVyc1xyXG4gICAgICApO1xyXG4gICAgICBjaGFuZ2VWYWx1ZSA9IHRoaXMuaXNOdW1iZXJWYWx1ZSA/IE51bWJlcihjaGFuZ2VWYWx1ZSkgOiBjaGFuZ2VWYWx1ZTtcclxuICAgIH1cclxuXHJcbiAgICByZXR1cm4gY2hhbmdlVmFsdWU7XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIF9yZW1vdmVNYXNrKFxyXG4gICAgdmFsdWU6IHN0cmluZyxcclxuICAgIHNwZWNpYWxDaGFyYWN0ZXJzRm9yUmVtb3ZlOiBzdHJpbmdbXVxyXG4gICk6IHN0cmluZyB7XHJcbiAgICByZXR1cm4gdmFsdWVcclxuICAgICAgPyB2YWx1ZS5yZXBsYWNlKHRoaXMuX3JlZ0V4cEZvclJlbW92ZShzcGVjaWFsQ2hhcmFjdGVyc0ZvclJlbW92ZSksICcnKVxyXG4gICAgICA6IHZhbHVlO1xyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBfcmVtb3ZlUHJlZml4KHZhbHVlOiBzdHJpbmcpOiBzdHJpbmcge1xyXG4gICAgaWYgKCF0aGlzLnByZWZpeCkge1xyXG4gICAgICByZXR1cm4gdmFsdWU7XHJcbiAgICB9XHJcbiAgICByZXR1cm4gdmFsdWUgPyB2YWx1ZS5yZXBsYWNlKHRoaXMucHJlZml4LCAnJykgOiB2YWx1ZTtcclxuICB9XHJcblxyXG4gIHByaXZhdGUgX3JlbW92ZVN1Zml4KHZhbHVlOiBzdHJpbmcpOiBzdHJpbmcge1xyXG4gICAgaWYgKCF0aGlzLnN1Zml4KSB7XHJcbiAgICAgIHJldHVybiB2YWx1ZTtcclxuICAgIH1cclxuICAgIHJldHVybiB2YWx1ZSA/IHZhbHVlLnJlcGxhY2UodGhpcy5zdWZpeCwgJycpIDogdmFsdWU7XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIF9yZWdFeHBGb3JSZW1vdmUoc3BlY2lhbENoYXJhY3RlcnNGb3JSZW1vdmU6IHN0cmluZ1tdKTogUmVnRXhwIHtcclxuICAgIHJldHVybiBuZXcgUmVnRXhwKFxyXG4gICAgICBzcGVjaWFsQ2hhcmFjdGVyc0ZvclJlbW92ZS5tYXAoKGl0ZW06IHN0cmluZykgPT4gYFxcXFwke2l0ZW19YCkuam9pbignfCcpLFxyXG4gICAgICAnZ2knXHJcbiAgICApO1xyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBfYXBwbHlNYXNrUmVzdWx0KHJlc3VsdDogc3RyaW5nKSB7XHJcbiAgICBpZiAoIXRoaXMuc2hvd01hc2tUeXBlZCkge1xyXG4gICAgICByZXR1cm4gcmVzdWx0O1xyXG4gICAgfVxyXG4gICAgY29uc3QgcmVzTGVuOiBudW1iZXIgPSByZXN1bHQubGVuZ3RoO1xyXG4gICAgY29uc3QgcHJlZk5tYXNrOiBzdHJpbmcgPSB0aGlzLnByZWZpeCArIHRoaXMubWFza0lzU2hvd247XHJcbiAgICBjb25zdCBpZk1hc2tJc1Nob3duID0gcHJlZk5tYXNrLnNsaWNlKHJlc0xlbik7XHJcblxyXG4gICAgcmV0dXJuIHJlc3VsdCArIGlmTWFza0lzU2hvd247XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIF9vbkNvbnRyb2xWYWx1ZUNoYW5nZSh2YWx1ZTogc3RyaW5nKSB7XHJcbiAgICAvKlxyXG4gICAgICBCZWNhdXNlIHdlIGFyZSBubyBsb25nZXIgd29ya2luZyB3aXRoIHRoZSBDb250cm9sVmFsdWVBY2Nlc3NvciAoc2luY2UgaXQgZG9lc24ndCBwbGF5IG5pY2Ugd2l0aCBJb25pYykuXHJcbiAgICAgIFdlIG5lZWQgbG9naWMgaGVyZSB0byB0cmFjayBjaGFuZ2VzIG1hZGUgcHJvZ3JhbW1hdGljYWxseSB0byB0aGUgZm9ybSB2YWx1ZS4gIFNwZWNpZmljYWxseSBjaGFuZ2VzXHJcbiAgICAgIGRvbmUgT1VUU0lERSBvZiB0aGUgbWFzay4gU2luY2UgY2hhbmdlcyBkb25lIGluc2lkZSB0aGUgbWFzayBtYXkgYWxzbyBmaXJlIG9mZiB0aGlzIG1ldGhvZFxyXG4gICAgICB3ZSBuZWVkIHRvIGRvIHNvbWUgaml1IGppdHN1IHRvIGVuc3VyZSB3ZSBhcmUgaWdub3JpbmcgdGhvc2UgY2hhbmdlcy5cclxuICAgICovXHJcbiAgICBjb25zdCBuZXdWYWx1ZSA9IHRoaXMuZ2V0VW5tYXNrZWRWYWx1ZSh2YWx1ZSk7XHJcblxyXG4gICAgaWYgKHRoaXMudW5tYXNrZWRWYWx1ZSA9PT0gbmV3VmFsdWUpIHtcclxuICAgICAgcmV0dXJuO1xyXG4gICAgfVxyXG5cclxuICAgIGxldCB1bm1hc2tlZFN1YnN0cmluZzogc3RyaW5nID0gbnVsbDtcclxuXHJcbiAgICAvLyBUaGlzIG1ldGhvZCAodmFsdWUgY2hhbmdlKSBmaXJlcyBvZmYgYmVmb3JlIGEgS2V5ZG93biBvciBJbnB1dCBldmVudCwgc28gd2UgbmVlZCB0byBzdWJ0cmFjdFxyXG4gICAgLy8gdGhlIGxhdGVzdCBjaGFuZ2UgYW5kIGNvbXBhcmUgdG8gb3VyIHByZXZpb3VzIChrbm93bikgdmFsdWUuXHJcbiAgICBpZiAodGhpcy51bm1hc2tlZFZhbHVlICE9IG51bGwpIHtcclxuICAgICAgY29uc3QgdiA9IHRoaXMudW5tYXNrZWRWYWx1ZS50b1N0cmluZygpO1xyXG4gICAgICB1bm1hc2tlZFN1YnN0cmluZyA9IHYuc3Vic3RyaW5nKDAsIHYubGVuZ3RoIC0gMSk7XHJcbiAgICB9XHJcblxyXG4gICAgaWYgKG5ld1ZhbHVlICE9PSB1bm1hc2tlZFN1YnN0cmluZykge1xyXG4gICAgICBjb25zdCBudiA9IG5ld1ZhbHVlICE9IG51bGwgPyBuZXdWYWx1ZS50b1N0cmluZygpIDogbnVsbDtcclxuICAgICAgY29uc3QgdiA9IHRoaXMuYXBwbHlNYXNrKG52LCB0aGlzLm1hc2tFeHByZXNzaW9uKTtcclxuICAgICAgdGhpcy5zZXRWYWx1ZSh2KTtcclxuICAgIH1cclxuICB9XHJcbn1cclxuIiwiaW1wb3J0IHsgRGlyZWN0aXZlLCBIb3N0TGlzdGVuZXIsIEluamVjdCwgSW5wdXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgRE9DVU1FTlQgfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xyXG5pbXBvcnQgeyBOZ0NvbnRyb2wgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XHJcbmltcG9ydCB7IE1hc2tTZXJ2aWNlIH0gZnJvbSAnLi9tYXNrLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBJQ29uZmlnIH0gZnJvbSAnLi9jb25maWcnO1xyXG5cclxuQERpcmVjdGl2ZSh7XHJcbiAgc2VsZWN0b3I6ICdbbWFza10nLFxyXG4gIHByb3ZpZGVyczogW01hc2tTZXJ2aWNlXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgTWFza0RpcmVjdGl2ZSB7XHJcbiAgcHJpdmF0ZSBfbWFza1ZhbHVlOiBzdHJpbmc7XHJcbiAgcHJpdmF0ZSBfaW5wdXRWYWx1ZTogc3RyaW5nO1xyXG4gIHByaXZhdGUgX3Bvc2l0aW9uOiBudW1iZXIgfCBudWxsID0gbnVsbDtcclxuICAvLyB0c2xpbnQ6ZGlzYWJsZS1uZXh0LWxpbmVcclxuICBwcml2YXRlIF9zdGFydDogbnVtYmVyO1xyXG4gIHByaXZhdGUgX2VuZDogbnVtYmVyO1xyXG4gIC8vIHRzbGludDpkaXNhYmxlLW5leHQtbGluZVxyXG4gIHB1YmxpYyBvblRvdWNoID0gKCkgPT4ge307XHJcbiAgcHVibGljIGNvbnN0cnVjdG9yKFxyXG4gICAgLy8gdHNsaW50OmRpc2FibGUtbmV4dC1saW5lXHJcbiAgICBASW5qZWN0KERPQ1VNRU5UKSBwcml2YXRlIGRvY3VtZW50OiBhbnksXHJcbiAgICBwcml2YXRlIF9tYXNrU2VydmljZTogTWFza1NlcnZpY2UsXHJcbiAgICBwcml2YXRlIF9uZ0NvbnRyb2w6IE5nQ29udHJvbFxyXG4gICkge31cclxuXHJcbiAgQElucHV0KCdtYXNrJylcclxuICBwdWJsaWMgc2V0IG1hc2tFeHByZXNzaW9uKHZhbHVlOiBzdHJpbmcpIHtcclxuICAgIHRoaXMuX21hc2tWYWx1ZSA9IHZhbHVlIHx8ICcnO1xyXG4gICAgaWYgKCF0aGlzLl9tYXNrVmFsdWUpIHtcclxuICAgICAgcmV0dXJuO1xyXG4gICAgfVxyXG4gICAgdGhpcy5faW5wdXRWYWx1ZSA9IHRoaXMuX25nQ29udHJvbC5jb250cm9sLnZhbHVlO1xyXG4gICAgdGhpcy5faW5pdGlhbGl6ZU1hc2soKTtcclxuICB9XHJcblxyXG4gIEBJbnB1dCgpXHJcbiAgcHVibGljIHNldCBzcGVjaWFsQ2hhcmFjdGVycyh2YWx1ZTogSUNvbmZpZ1snc3BlY2lhbENoYXJhY3RlcnMnXSkge1xyXG4gICAgaWYgKFxyXG4gICAgICAhdmFsdWUgfHxcclxuICAgICAgIUFycmF5LmlzQXJyYXkodmFsdWUpIHx8XHJcbiAgICAgIChBcnJheS5pc0FycmF5KHZhbHVlKSAmJiAhdmFsdWUubGVuZ3RoKVxyXG4gICAgKSB7XHJcbiAgICAgIHJldHVybjtcclxuICAgIH1cclxuICAgIHRoaXMuX21hc2tTZXJ2aWNlLm1hc2tTcGVjaWFsQ2hhcmFjdGVycyA9IHZhbHVlO1xyXG4gIH1cclxuXHJcbiAgQElucHV0KClcclxuICBwdWJsaWMgc2V0IHBhdHRlcm5zKHZhbHVlOiBJQ29uZmlnWydwYXR0ZXJucyddKSB7XHJcbiAgICBpZiAoIXZhbHVlKSB7XHJcbiAgICAgIHJldHVybjtcclxuICAgIH1cclxuICAgIHRoaXMuX21hc2tTZXJ2aWNlLm1hc2tBdmFpbGFibGVQYXR0ZXJucyA9IHZhbHVlO1xyXG4gIH1cclxuXHJcbiAgQElucHV0KClcclxuICBwdWJsaWMgc2V0IHByZWZpeCh2YWx1ZTogSUNvbmZpZ1sncHJlZml4J10pIHtcclxuICAgIGlmICghdmFsdWUpIHtcclxuICAgICAgcmV0dXJuO1xyXG4gICAgfVxyXG4gICAgdGhpcy5fbWFza1NlcnZpY2UucHJlZml4ID0gdmFsdWU7XHJcbiAgfVxyXG5cclxuICBASW5wdXQoKVxyXG4gIHB1YmxpYyBzZXQgc3VmaXgodmFsdWU6IElDb25maWdbJ3N1Zml4J10pIHtcclxuICAgIGlmICghdmFsdWUpIHtcclxuICAgICAgcmV0dXJuO1xyXG4gICAgfVxyXG4gICAgdGhpcy5fbWFza1NlcnZpY2Uuc3VmaXggPSB2YWx1ZTtcclxuICB9XHJcblxyXG4gIEBJbnB1dCgpXHJcbiAgcHVibGljIHNldCBkcm9wU3BlY2lhbENoYXJhY3RlcnModmFsdWU6IElDb25maWdbJ2Ryb3BTcGVjaWFsQ2hhcmFjdGVycyddKSB7XHJcbiAgICB0aGlzLl9tYXNrU2VydmljZS5kcm9wU3BlY2lhbENoYXJhY3RlcnMgPSB2YWx1ZTtcclxuICB9XHJcblxyXG4gIEBJbnB1dCgpXHJcbiAgcHVibGljIHNldCBzaG93TWFza1R5cGVkKHZhbHVlOiBJQ29uZmlnWydzaG93TWFza1R5cGVkJ10pIHtcclxuICAgIGlmICghdmFsdWUpIHtcclxuICAgICAgcmV0dXJuO1xyXG4gICAgfVxyXG4gICAgdGhpcy5fbWFza1NlcnZpY2Uuc2hvd01hc2tUeXBlZCA9IHZhbHVlO1xyXG4gIH1cclxuXHJcbiAgQElucHV0KClcclxuICBwdWJsaWMgc2V0IHNob3dUZW1wbGF0ZSh2YWx1ZTogSUNvbmZpZ1snc2hvd1RlbXBsYXRlJ10pIHtcclxuICAgIHRoaXMuX21hc2tTZXJ2aWNlLnNob3dUZW1wbGF0ZSA9IHZhbHVlO1xyXG4gIH1cclxuXHJcbiAgQElucHV0KClcclxuICBwdWJsaWMgc2V0IGNsZWFySWZOb3RNYXRjaCh2YWx1ZTogSUNvbmZpZ1snY2xlYXJJZk5vdE1hdGNoJ10pIHtcclxuICAgIHRoaXMuX21hc2tTZXJ2aWNlLmNsZWFySWZOb3RNYXRjaCA9IHZhbHVlO1xyXG4gIH1cclxuXHJcbiAgQEhvc3RMaXN0ZW5lcignaW5wdXQnLCBbJyRldmVudCddKVxyXG4gIHB1YmxpYyBvbklucHV0KGU6IEtleWJvYXJkRXZlbnQpOiB2b2lkIHtcclxuICAgIGNvbnN0IGVsOiBIVE1MSW5wdXRFbGVtZW50ID0gZS50YXJnZXQgYXMgSFRNTElucHV0RWxlbWVudDtcclxuICAgIHRoaXMuX2lucHV0VmFsdWUgPSBlbC52YWx1ZTtcclxuXHJcbiAgICBpZiAoIXRoaXMuX21hc2tWYWx1ZSkge1xyXG4gICAgICByZXR1cm47XHJcbiAgICB9XHJcbiAgICBjb25zdCBwb3NpdGlvbjogbnVtYmVyID1cclxuICAgICAgKGVsLnNlbGVjdGlvblN0YXJ0IGFzIG51bWJlcikgPT09IDFcclxuICAgICAgICA/IChlbC5zZWxlY3Rpb25TdGFydCBhcyBudW1iZXIpICsgdGhpcy5fbWFza1NlcnZpY2UucHJlZml4Lmxlbmd0aFxyXG4gICAgICAgIDogKGVsLnNlbGVjdGlvblN0YXJ0IGFzIG51bWJlcik7XHJcbiAgICBsZXQgY2FyZXRTaGlmdCA9IDA7XHJcbiAgICB0aGlzLl9tYXNrU2VydmljZS5hcHBseVZhbHVlQ2hhbmdlcyhcclxuICAgICAgcG9zaXRpb24sXHJcbiAgICAgIChzaGlmdDogbnVtYmVyKSA9PiAoY2FyZXRTaGlmdCA9IHNoaWZ0KVxyXG4gICAgKTtcclxuICAgIC8vIG9ubHkgc2V0IHRoZSBzZWxlY3Rpb24gaWYgdGhlIGVsZW1lbnQgaXMgYWN0aXZlXHJcbiAgICBpZiAodGhpcy5kb2N1bWVudC5hY3RpdmVFbGVtZW50ICE9PSBlbCkge1xyXG4gICAgICByZXR1cm47XHJcbiAgICB9XHJcbiAgICBlbC5zZWxlY3Rpb25TdGFydCA9IGVsLnNlbGVjdGlvbkVuZCA9XHJcbiAgICAgIHRoaXMuX3Bvc2l0aW9uICE9PSBudWxsXHJcbiAgICAgICAgPyB0aGlzLl9wb3NpdGlvblxyXG4gICAgICAgIDogcG9zaXRpb24gK1xyXG4gICAgICAgICAgLy8gdHNsaW50OmRpc2FibGUtbmV4dC1saW5lXHJcbiAgICAgICAgICAoKGUgYXMgYW55KS5pbnB1dFR5cGUgPT09ICdkZWxldGVDb250ZW50QmFja3dhcmQnID8gMCA6IGNhcmV0U2hpZnQpO1xyXG4gICAgdGhpcy5fcG9zaXRpb24gPSBudWxsO1xyXG4gIH1cclxuXHJcbiAgQEhvc3RMaXN0ZW5lcignYmx1cicpXHJcbiAgcHVibGljIG9uQmx1cigpOiB2b2lkIHtcclxuICAgIHRoaXMuX21hc2tTZXJ2aWNlLmNsZWFySWZOb3RNYXRjaEZuKCk7XHJcbiAgICB0aGlzLm9uVG91Y2goKTtcclxuICB9XHJcblxyXG4gIEBIb3N0TGlzdGVuZXIoJ2NsaWNrJywgWyckZXZlbnQnXSlcclxuICBwdWJsaWMgb25Gb2N1cyhlOiBNb3VzZUV2ZW50IHwgS2V5Ym9hcmRFdmVudCk6IHZvaWQge1xyXG4gICAgY29uc3QgZWw6IEhUTUxJbnB1dEVsZW1lbnQgPSBlLnRhcmdldCBhcyBIVE1MSW5wdXRFbGVtZW50O1xyXG5cclxuICAgIGlmIChcclxuICAgICAgZWwgIT09IG51bGwgJiZcclxuICAgICAgZWwuc2VsZWN0aW9uU3RhcnQgIT09IG51bGwgJiZcclxuICAgICAgZWwuc2VsZWN0aW9uU3RhcnQgPT09IGVsLnNlbGVjdGlvbkVuZCAmJlxyXG4gICAgICBlbC5zZWxlY3Rpb25TdGFydCA+IHRoaXMuX21hc2tTZXJ2aWNlLnByZWZpeC5sZW5ndGggJiZcclxuICAgICAgLy8gdHNsaW50OmRpc2FibGUtbmV4dC1saW5lXHJcbiAgICAgIChlIGFzIGFueSkua2V5Q29kZSAhPT0gMzhcclxuICAgICkge1xyXG4gICAgICByZXR1cm47XHJcbiAgICB9XHJcbiAgICBpZiAodGhpcy5fbWFza1NlcnZpY2Uuc2hvd01hc2tUeXBlZCkge1xyXG4gICAgICB0aGlzLl9tYXNrU2VydmljZS5tYXNrSXNTaG93biA9IHRoaXMuX21hc2tTZXJ2aWNlLm1hc2tFeHByZXNzaW9uLnJlcGxhY2UoXHJcbiAgICAgICAgL1swLTldL2csXHJcbiAgICAgICAgJ18nXHJcbiAgICAgICk7XHJcbiAgICB9XHJcbiAgICBlbC52YWx1ZSA9XHJcbiAgICAgICFlbC52YWx1ZSB8fCBlbC52YWx1ZSA9PT0gdGhpcy5fbWFza1NlcnZpY2UucHJlZml4XHJcbiAgICAgICAgPyB0aGlzLl9tYXNrU2VydmljZS5wcmVmaXggKyB0aGlzLl9tYXNrU2VydmljZS5tYXNrSXNTaG93blxyXG4gICAgICAgIDogZWwudmFsdWU7XHJcbiAgICAvKiogZml4IG9mIGN1cnNvciBwb3NpdGlvbiB3aXRoIHByZWZpeCB3aGVuIG1vdXNlIGNsaWNrIG9jY3VyICovXHJcbiAgICBpZiAoXHJcbiAgICAgICgoZWwuc2VsZWN0aW9uU3RhcnQgYXMgbnVtYmVyKSB8fCAoZWwuc2VsZWN0aW9uRW5kIGFzIG51bWJlcikpIDw9XHJcbiAgICAgIHRoaXMuX21hc2tTZXJ2aWNlLnByZWZpeC5sZW5ndGhcclxuICAgICkge1xyXG4gICAgICBlbC5zZWxlY3Rpb25TdGFydCA9IHRoaXMuX21hc2tTZXJ2aWNlLnByZWZpeC5sZW5ndGg7XHJcbiAgICAgIHJldHVybjtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIEBIb3N0TGlzdGVuZXIoJ2tleWRvd24nLCBbJyRldmVudCddKVxyXG4gIHB1YmxpYyBvbktleURvd24oZTogS2V5Ym9hcmRFdmVudCk6IHZvaWQge1xyXG4gICAgY29uc3QgZWw6IEhUTUxJbnB1dEVsZW1lbnQgPSBlLnRhcmdldCBhcyBIVE1MSW5wdXRFbGVtZW50O1xyXG4gICAgaWYgKGUua2V5Q29kZSA9PT0gMzgpIHtcclxuICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICAgfVxyXG4gICAgaWYgKGUua2V5Q29kZSA9PT0gMzcgfHwgZS5rZXlDb2RlID09PSA4KSB7XHJcbiAgICAgIGlmIChcclxuICAgICAgICAoZWwuc2VsZWN0aW9uU3RhcnQgYXMgbnVtYmVyKSA8PSB0aGlzLl9tYXNrU2VydmljZS5wcmVmaXgubGVuZ3RoICYmXHJcbiAgICAgICAgKGVsLnNlbGVjdGlvbkVuZCBhcyBudW1iZXIpIDw9IHRoaXMuX21hc2tTZXJ2aWNlLnByZWZpeC5sZW5ndGhcclxuICAgICAgKSB7XHJcbiAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICAgICB9XHJcbiAgICAgIHRoaXMub25Gb2N1cyhlKTtcclxuICAgICAgaWYgKFxyXG4gICAgICAgIGUua2V5Q29kZSA9PT0gOCAmJlxyXG4gICAgICAgIGVsLnNlbGVjdGlvblN0YXJ0ID09PSAwICYmXHJcbiAgICAgICAgZWwuc2VsZWN0aW9uRW5kID09PSBlbC52YWx1ZS5sZW5ndGhcclxuICAgICAgKSB7XHJcbiAgICAgICAgZWwudmFsdWUgPSB0aGlzLl9tYXNrU2VydmljZS5wcmVmaXg7XHJcbiAgICAgICAgdGhpcy5fcG9zaXRpb24gPSB0aGlzLl9tYXNrU2VydmljZS5wcmVmaXhcclxuICAgICAgICAgID8gdGhpcy5fbWFza1NlcnZpY2UucHJlZml4Lmxlbmd0aFxyXG4gICAgICAgICAgOiAxO1xyXG4gICAgICAgIHRoaXMub25JbnB1dChlKTtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgQEhvc3RMaXN0ZW5lcigncGFzdGUnKVxyXG4gIHB1YmxpYyBvblBhc3RlKCk6IHZvaWQge1xyXG4gICAgdGhpcy5fcG9zaXRpb24gPSBOdW1iZXIuTUFYX1NBRkVfSU5URUdFUjtcclxuICB9XHJcblxyXG4gIC8qKiBJdCBkaXNhYmxlcyB0aGUgaW5wdXQgZWxlbWVudCAqL1xyXG4gIHB1YmxpYyBzZXREaXNhYmxlZFN0YXRlKGlzRGlzYWJsZWQ6IGJvb2xlYW4pOiB2b2lkIHtcclxuICAgIHRoaXMuX21hc2tTZXJ2aWNlLnNldEZvcm1FbGVtZW50UHJvcGVydHkoJ2Rpc2FibGVkJywgaXNEaXNhYmxlZCk7XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIF9yZXBlYXRQYXR0ZXJuU3ltYm9scyhtYXNrRXhwOiBzdHJpbmcpOiBzdHJpbmcge1xyXG4gICAgcmV0dXJuIChcclxuICAgICAgKG1hc2tFeHAubWF0Y2goL3tbMC05XSt9LykgJiZcclxuICAgICAgICBtYXNrRXhwXHJcbiAgICAgICAgICAuc3BsaXQoJycpXHJcbiAgICAgICAgICAucmVkdWNlKChhY2N1bTogc3RyaW5nLCBjdXJydmFsOiBzdHJpbmcsIGluZGV4OiBudW1iZXIpOiBzdHJpbmcgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLl9zdGFydCA9IGN1cnJ2YWwgPT09ICd7JyA/IGluZGV4IDogdGhpcy5fc3RhcnQ7XHJcblxyXG4gICAgICAgICAgICBpZiAoY3VycnZhbCAhPT0gJ30nKSB7XHJcbiAgICAgICAgICAgICAgcmV0dXJuIHRoaXMuX21hc2tTZXJ2aWNlLl9maW5kU3BlY2lhbENoYXIoY3VycnZhbClcclxuICAgICAgICAgICAgICAgID8gYWNjdW0gKyBjdXJydmFsXHJcbiAgICAgICAgICAgICAgICA6IGFjY3VtO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHRoaXMuX2VuZCA9IGluZGV4O1xyXG4gICAgICAgICAgICBjb25zdCByZXBlYXROdW1iZXI6IG51bWJlciA9IE51bWJlcihcclxuICAgICAgICAgICAgICBtYXNrRXhwLnNsaWNlKHRoaXMuX3N0YXJ0ICsgMSwgdGhpcy5fZW5kKVxyXG4gICAgICAgICAgICApO1xyXG4gICAgICAgICAgICBjb25zdCByZXBhY2VXaXRoOiBzdHJpbmcgPSBuZXcgQXJyYXkocmVwZWF0TnVtYmVyICsgMSkuam9pbihcclxuICAgICAgICAgICAgICBtYXNrRXhwW3RoaXMuX3N0YXJ0IC0gMV1cclxuICAgICAgICAgICAgKTtcclxuICAgICAgICAgICAgcmV0dXJuIGFjY3VtICsgcmVwYWNlV2l0aDtcclxuICAgICAgICAgIH0sICcnKSkgfHxcclxuICAgICAgbWFza0V4cFxyXG4gICAgKTtcclxuICB9XHJcblxyXG4gIHByaXZhdGUgX2luaXRpYWxpemVNYXNrKCkge1xyXG4gICAgdGhpcy5fbWFza1NlcnZpY2UubWFza0V4cHJlc3Npb24gPSB0aGlzLl9yZXBlYXRQYXR0ZXJuU3ltYm9scyhcclxuICAgICAgdGhpcy5fbWFza1ZhbHVlXHJcbiAgICApO1xyXG5cclxuICAgIGNvbnN0IG0gPSB0aGlzLl9tYXNrU2VydmljZS5hcHBseU1hc2soXHJcbiAgICAgIHRoaXMuX2lucHV0VmFsdWUsXHJcbiAgICAgIHRoaXMuX21hc2tTZXJ2aWNlLm1hc2tFeHByZXNzaW9uXHJcbiAgICApO1xyXG5cclxuICAgIHRoaXMuX21hc2tTZXJ2aWNlLnNldFZhbHVlKG0pO1xyXG4gIH1cclxufVxyXG4iLCJpbXBvcnQgeyBQaXBlLCBQaXBlVHJhbnNmb3JtIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IE1hc2tBcHBsaWVyU2VydmljZSB9IGZyb20gJy4vbWFzay1hcHBsaWVyLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBJQ29uZmlnIH0gZnJvbSAnLi9jb25maWcnO1xyXG5cclxuQFBpcGUoe1xyXG4gIG5hbWU6ICdtYXNrJyxcclxuICBwdXJlOiB0cnVlXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBNYXNrUGlwZSBpbXBsZW1lbnRzIFBpcGVUcmFuc2Zvcm0ge1xyXG4gIHB1YmxpYyBjb25zdHJ1Y3Rvcihwcml2YXRlIF9tYXNrU2VydmljZTogTWFza0FwcGxpZXJTZXJ2aWNlKSB7fVxyXG5cclxuICBwdWJsaWMgdHJhbnNmb3JtKFxyXG4gICAgdmFsdWU6IHN0cmluZyB8IG51bWJlcixcclxuICAgIG1hc2s6IHN0cmluZyB8IFtzdHJpbmcsIElDb25maWdbJ3BhdHRlcm5zJ11dXHJcbiAgKTogc3RyaW5nIHtcclxuICAgIGlmICghdmFsdWUpIHtcclxuICAgICAgcmV0dXJuICcnO1xyXG4gICAgfVxyXG4gICAgaWYgKHR5cGVvZiBtYXNrID09PSAnc3RyaW5nJykge1xyXG4gICAgICByZXR1cm4gdGhpcy5fbWFza1NlcnZpY2UuYXBwbHlNYXNrKGAke3ZhbHVlfWAsIG1hc2spO1xyXG4gICAgfVxyXG4gICAgcmV0dXJuIHRoaXMuX21hc2tTZXJ2aWNlLmFwcGx5TWFza1dpdGhQYXR0ZXJuKGAke3ZhbHVlfWAsIG1hc2spO1xyXG4gIH1cclxufVxyXG4iLCJpbXBvcnQge1xyXG4gIGNvbmZpZyxcclxuICBJTklUSUFMX0NPTkZJRyxcclxuICBpbml0aWFsQ29uZmlnLFxyXG4gIE5FV19DT05GSUcsXHJcbiAgb3B0aW9uc0NvbmZpZ1xyXG4gIH0gZnJvbSAnLi9jb25maWcnO1xyXG5pbXBvcnQgeyBNYXNrQXBwbGllclNlcnZpY2UgfSBmcm9tICcuL21hc2stYXBwbGllci5zZXJ2aWNlJztcclxuaW1wb3J0IHsgTWFza0RpcmVjdGl2ZSB9IGZyb20gJy4vbWFzay5kaXJlY3RpdmUnO1xyXG5pbXBvcnQgeyBNYXNrUGlwZSB9IGZyb20gJy4vbWFzay5waXBlJztcclxuaW1wb3J0IHsgTW9kdWxlV2l0aFByb3ZpZGVycywgTmdNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuXHJcblxyXG5ATmdNb2R1bGUoe1xyXG4gIHByb3ZpZGVyczogW01hc2tBcHBsaWVyU2VydmljZSwgTWFza1BpcGVdLFxyXG4gIGV4cG9ydHM6IFtNYXNrRGlyZWN0aXZlLCBNYXNrUGlwZV0sXHJcbiAgZGVjbGFyYXRpb25zOiBbTWFza0RpcmVjdGl2ZSwgTWFza1BpcGVdXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBOZ3hNYXNrSW9uaWNNb2R1bGUge1xyXG4gIHB1YmxpYyBzdGF0aWMgZm9yUm9vdChjb25maWdWYWx1ZT86IG9wdGlvbnNDb25maWcpOiBNb2R1bGVXaXRoUHJvdmlkZXJzIHtcclxuICAgIHJldHVybiB7XHJcbiAgICAgIG5nTW9kdWxlOiBOZ3hNYXNrSW9uaWNNb2R1bGUsXHJcbiAgICAgIHByb3ZpZGVyczogW1xyXG4gICAgICAgIHtcclxuICAgICAgICAgIHByb3ZpZGU6IE5FV19DT05GSUcsXHJcbiAgICAgICAgICB1c2VWYWx1ZTogY29uZmlnVmFsdWVcclxuICAgICAgICB9LFxyXG4gICAgICAgIHtcclxuICAgICAgICAgIHByb3ZpZGU6IElOSVRJQUxfQ09ORklHLFxyXG4gICAgICAgICAgdXNlVmFsdWU6IGluaXRpYWxDb25maWdcclxuICAgICAgICB9LFxyXG4gICAgICAgIHtcclxuICAgICAgICAgIHByb3ZpZGU6IGNvbmZpZyxcclxuICAgICAgICAgIHVzZUZhY3Rvcnk6IF9jb25maWdGYWN0b3J5LFxyXG4gICAgICAgICAgZGVwczogW0lOSVRJQUxfQ09ORklHLCBORVdfQ09ORklHXVxyXG4gICAgICAgIH0sXHJcbiAgICAgICAgTWFza1BpcGVcclxuICAgICAgXVxyXG4gICAgfTtcclxuICB9XHJcbiAgcHVibGljIHN0YXRpYyBmb3JDaGlsZChjb25maWdWYWx1ZT86IG9wdGlvbnNDb25maWcpOiBNb2R1bGVXaXRoUHJvdmlkZXJzIHtcclxuICAgIHJldHVybiB7XHJcbiAgICAgIG5nTW9kdWxlOiBOZ3hNYXNrSW9uaWNNb2R1bGVcclxuICAgIH07XHJcbiAgfVxyXG59XHJcblxyXG4vKipcclxuICogQGludGVybmFsXHJcbiAqL1xyXG5leHBvcnQgZnVuY3Rpb24gX2NvbmZpZ0ZhY3RvcnkoXHJcbiAgaW5pdENvbmZpZzogb3B0aW9uc0NvbmZpZyxcclxuICBjb25maWdWYWx1ZTogb3B0aW9uc0NvbmZpZyB8ICgoKSA9PiBvcHRpb25zQ29uZmlnKVxyXG4pOiBGdW5jdGlvbiB8IG9wdGlvbnNDb25maWcge1xyXG4gIHJldHVybiB0eXBlb2YgY29uZmlnVmFsdWUgPT09ICdmdW5jdGlvbidcclxuICAgID8gY29uZmlnVmFsdWUoKVxyXG4gICAgOiB7IC4uLmluaXRDb25maWcsIC4uLmNvbmZpZ1ZhbHVlIH07XHJcbn1cclxuIl0sIm5hbWVzIjpbIkluamVjdGlvblRva2VuIiwiSW5qZWN0YWJsZSIsIkluamVjdCIsInRzbGliXzEuX19leHRlbmRzIiwiRE9DVU1FTlQiLCJFbGVtZW50UmVmIiwiUmVuZGVyZXIyIiwiTmdDb250cm9sIiwiRGlyZWN0aXZlIiwiSW5wdXQiLCJIb3N0TGlzdGVuZXIiLCJQaXBlIiwiTmdNb2R1bGUiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFBQTtBQW1CQSxRQUFhLE1BQU0sR0FBMkIsSUFBSUEsbUJBQWMsQ0FBQyxRQUFRLENBQUM7O0FBQzFFLFFBQWEsVUFBVSxHQUEyQixJQUFJQSxtQkFBYyxDQUNsRSxZQUFZLENBQ2I7O0FBQ0QsUUFBYSxjQUFjLEdBQTRCLElBQUlBLG1CQUFjLENBQ3ZFLGdCQUFnQixDQUNqQjs7QUFFRCxRQUFhLGFBQWEsR0FBWTtRQUNwQyxLQUFLLEVBQUUsRUFBRTtRQUNULE1BQU0sRUFBRSxFQUFFO1FBQ1YsZUFBZSxFQUFFLEtBQUs7UUFDdEIsWUFBWSxFQUFFLEtBQUs7UUFDbkIsYUFBYSxFQUFFLEtBQUs7UUFDcEIscUJBQXFCLEVBQUUsSUFBSTtRQUMzQixpQkFBaUIsRUFBRTtZQUNqQixHQUFHO1lBQ0gsR0FBRztZQUNILEdBQUc7WUFDSCxHQUFHO1lBQ0gsR0FBRztZQUNILEdBQUc7WUFDSCxHQUFHO1lBQ0gsR0FBRztZQUNILEdBQUc7WUFDSCxHQUFHO1lBQ0gsR0FBRztZQUNILEdBQUc7WUFDSCxHQUFHO1lBQ0gsR0FBRztTQUNKO1FBQ0QsUUFBUSxFQUFFO1lBQ1IsR0FBRyxFQUFFO2dCQUNILE9BQU8sRUFBRSxJQUFJLE1BQU0sQ0FBQyxLQUFLLENBQUM7YUFDM0I7WUFDRCxHQUFHLEVBQUU7Z0JBQ0gsT0FBTyxFQUFFLElBQUksTUFBTSxDQUFDLEtBQUssQ0FBQztnQkFDMUIsUUFBUSxFQUFFLElBQUk7YUFDZjtZQUNELENBQUMsRUFBRTtnQkFDRCxPQUFPLEVBQUUsSUFBSSxNQUFNLENBQUMsYUFBYSxDQUFDO2FBQ25DO1lBQ0QsQ0FBQyxFQUFFO2dCQUNELE9BQU8sRUFBRSxJQUFJLE1BQU0sQ0FBQyxVQUFVLENBQUM7YUFDaEM7WUFDRCxDQUFDLEVBQUU7Z0JBQ0QsT0FBTyxFQUFFLElBQUksTUFBTSxDQUFDLEtBQUssQ0FBQzthQUMzQjtZQUNELENBQUMsRUFBRTtnQkFDRCxPQUFPLEVBQUUsSUFBSSxNQUFNLENBQUMsS0FBSyxDQUFDO2FBQzNCO1NBQ0Y7S0FDRjs7SUN2RUQ7Ozs7Ozs7Ozs7Ozs7O0lBY0E7SUFFQSxJQUFJLGFBQWEsR0FBRyxVQUFTLENBQUMsRUFBRSxDQUFDO1FBQzdCLGFBQWEsR0FBRyxNQUFNLENBQUMsY0FBYzthQUNoQyxFQUFFLFNBQVMsRUFBRSxFQUFFLEVBQUUsWUFBWSxLQUFLLElBQUksVUFBVSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsQ0FBQyxTQUFTLEdBQUcsQ0FBQyxDQUFDLEVBQUUsQ0FBQztZQUM1RSxVQUFVLENBQUMsRUFBRSxDQUFDLElBQUksS0FBSyxJQUFJLENBQUMsSUFBSSxDQUFDO2dCQUFFLElBQUksQ0FBQyxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUM7b0JBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7UUFDL0UsT0FBTyxhQUFhLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO0lBQy9CLENBQUMsQ0FBQztBQUVGLGFBQWdCLFNBQVMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztRQUMxQixhQUFhLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO1FBQ3BCLFNBQVMsRUFBRSxLQUFLLElBQUksQ0FBQyxXQUFXLEdBQUcsQ0FBQyxDQUFDLEVBQUU7UUFDdkMsQ0FBQyxDQUFDLFNBQVMsR0FBRyxDQUFDLEtBQUssSUFBSSxHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLElBQUksRUFBRSxDQUFDLFNBQVMsR0FBRyxDQUFDLENBQUMsU0FBUyxFQUFFLElBQUksRUFBRSxFQUFFLENBQUMsQ0FBQztJQUN6RixDQUFDO0FBRUQsSUFBTyxJQUFJLFFBQVEsR0FBRztRQUNsQixRQUFRLEdBQUcsTUFBTSxDQUFDLE1BQU0sSUFBSSxTQUFTLFFBQVEsQ0FBQyxDQUFDO1lBQzNDLEtBQUssSUFBSSxDQUFDLEVBQUUsQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsU0FBUyxDQUFDLE1BQU0sRUFBRSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsRUFBRSxFQUFFO2dCQUNqRCxDQUFDLEdBQUcsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUNqQixLQUFLLElBQUksQ0FBQyxJQUFJLENBQUM7b0JBQUUsSUFBSSxNQUFNLENBQUMsU0FBUyxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQzt3QkFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO2FBQ2hGO1lBQ0QsT0FBTyxDQUFDLENBQUM7U0FDWixDQUFBO1FBQ0QsT0FBTyxRQUFRLENBQUMsS0FBSyxDQUFDLElBQUksRUFBRSxTQUFTLENBQUMsQ0FBQztJQUMzQyxDQUFDLENBQUE7QUFFRCxhQTZFZ0IsTUFBTSxDQUFDLENBQUMsRUFBRSxDQUFDO1FBQ3ZCLElBQUksQ0FBQyxHQUFHLE9BQU8sTUFBTSxLQUFLLFVBQVUsSUFBSSxDQUFDLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQzNELElBQUksQ0FBQyxDQUFDO1lBQUUsT0FBTyxDQUFDLENBQUM7UUFDakIsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLEVBQUUsRUFBRSxHQUFHLEVBQUUsRUFBRSxDQUFDLENBQUM7UUFDakMsSUFBSTtZQUNBLE9BQU8sQ0FBQyxDQUFDLEtBQUssS0FBSyxDQUFDLElBQUksQ0FBQyxFQUFFLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLElBQUksRUFBRSxFQUFFLElBQUk7Z0JBQUUsRUFBRSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUM7U0FDOUU7UUFDRCxPQUFPLEtBQUssRUFBRTtZQUFFLENBQUMsR0FBRyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsQ0FBQztTQUFFO2dCQUMvQjtZQUNKLElBQUk7Z0JBQ0EsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsSUFBSSxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsUUFBUSxDQUFDLENBQUM7b0JBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQzthQUNwRDtvQkFDTztnQkFBRSxJQUFJLENBQUM7b0JBQUUsTUFBTSxDQUFDLENBQUMsS0FBSyxDQUFDO2FBQUU7U0FDcEM7UUFDRCxPQUFPLEVBQUUsQ0FBQztJQUNkLENBQUM7Ozs7Ozs7UUNuSEMsNEJBQTZDLE9BQWdCO1lBQWhCLFlBQU8sR0FBUCxPQUFPLENBQVM7WUFUdEQsbUJBQWMsR0FBRyxFQUFFLENBQUM7WUFVekIsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLEdBQUcsRUFBRSxDQUFDO1lBQ3hCLElBQUksQ0FBQyxxQkFBcUIsR0FBRyxtQkFBQSxJQUFJLENBQUMsT0FBTyxHQUFFLGlCQUFpQixDQUFDO1lBQzdELElBQUksQ0FBQyxxQkFBcUIsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQztZQUNuRCxJQUFJLENBQUMsZUFBZSxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsZUFBZSxDQUFDO1lBQ3BELElBQUksQ0FBQyxxQkFBcUIsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLHFCQUFxQixDQUFDO1lBQ2hFLElBQUksQ0FBQyxxQkFBcUIsR0FBRyxtQkFBQSxJQUFJLENBQUMsT0FBTyxHQUFFLGlCQUFpQixDQUFDO1lBQzdELElBQUksQ0FBQyxxQkFBcUIsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQztZQUNuRCxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDO1lBQ2xDLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUM7U0FDakM7Ozs7Ozs7O1FBRU0saURBQW9COzs7Ozs7O1lBQTNCLFVBQ0UsVUFBa0IsRUFDbEIsY0FBNkM7Z0JBRXZDLElBQUEsOEJBQXNDLEVBQXJDLFlBQUksRUFBRSxxQkFBK0I7Z0JBQzVDLElBQUksQ0FBQyxhQUFhLEdBQUcsYUFBYSxDQUFDO2dCQUNuQyxPQUFPLElBQUksQ0FBQyxTQUFTLENBQUMsVUFBVSxFQUFFLElBQUksQ0FBQyxDQUFDO2FBQ3pDOzs7Ozs7OztRQUNNLHNDQUFTOzs7Ozs7O1lBQWhCLFVBQ0UsVUFBa0IsRUFDbEIsY0FBc0IsRUFDdEIsUUFBb0IsRUFDcEIsRUFBdUI7Z0JBRHZCLHlCQUFBO29CQUFBLFlBQW9COztnQkFDcEIsbUJBQUE7b0JBQUEsb0JBQXVCOztnQkFFdkIsSUFDRSxVQUFVLEtBQUssU0FBUztvQkFDeEIsVUFBVSxLQUFLLElBQUk7b0JBQ25CLGNBQWMsS0FBSyxTQUFTLEVBQzVCO29CQUNBLE9BQU8sRUFBRSxDQUFDO2lCQUNYOztvQkFFRyxNQUFNLEdBQUcsQ0FBQzs7b0JBQ1YsTUFBTSxHQUFHLEVBQUU7O29CQUNYLEtBQUssR0FBRyxLQUFLO2dCQUVqQixJQUFJLFVBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLEtBQUssSUFBSSxDQUFDLE1BQU0sRUFBRTtvQkFDM0QsVUFBVSxHQUFHLFVBQVUsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEVBQUUsVUFBVSxDQUFDLE1BQU0sQ0FBQyxDQUFDO2lCQUN0RTs7b0JBRUssVUFBVSxHQUFhLFVBQVUsQ0FBQyxRQUFRLEVBQUUsQ0FBQyxLQUFLLENBQUMsRUFBRSxDQUFDOztnQkFHNUQsS0FDRSxJQUFJLENBQUMsR0FBVyxDQUFDLEVBQUUsV0FBVyxHQUFXLFVBQVUsQ0FBQyxDQUFDLENBQUMsRUFDdEQsQ0FBQyxHQUFHLFVBQVUsQ0FBQyxNQUFNLEVBQ3JCLENBQUMsRUFBRSxFQUFFLFdBQVcsR0FBRyxVQUFVLENBQUMsQ0FBQyxDQUFDLEVBQ2hDO29CQUNBLElBQUksTUFBTSxLQUFLLGNBQWMsQ0FBQyxNQUFNLEVBQUU7d0JBQ3BDLE1BQU07cUJBQ1A7b0JBQ0QsSUFDRSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsV0FBVyxFQUFFLGNBQWMsQ0FBQyxNQUFNLENBQUMsQ0FBQzt3QkFDMUQsY0FBYyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsS0FBSyxHQUFHLEVBQ2xDO3dCQUNBLE1BQU0sSUFBSSxXQUFXLENBQUM7d0JBQ3RCLE1BQU0sSUFBSSxDQUFDLENBQUM7cUJBQ2I7eUJBQU0sSUFDTCxjQUFjLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxLQUFLLEdBQUc7d0JBQ2xDLEtBQUs7d0JBQ0wsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFdBQVcsRUFBRSxjQUFjLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFDLEVBQzlEO3dCQUNBLE1BQU0sSUFBSSxXQUFXLENBQUM7d0JBQ3RCLE1BQU0sSUFBSSxDQUFDLENBQUM7d0JBQ1osS0FBSyxHQUFHLEtBQUssQ0FBQztxQkFDZjt5QkFBTSxJQUNMLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxXQUFXLEVBQUUsY0FBYyxDQUFDLE1BQU0sQ0FBQyxDQUFDO3dCQUMxRCxjQUFjLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxLQUFLLEdBQUcsRUFDbEM7d0JBQ0EsTUFBTSxJQUFJLFdBQVcsQ0FBQzt3QkFDdEIsS0FBSyxHQUFHLElBQUksQ0FBQztxQkFDZDt5QkFBTSxJQUNMLGNBQWMsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLEtBQUssR0FBRzt3QkFDbEMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFdBQVcsRUFBRSxjQUFjLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFDLEVBQzlEO3dCQUNBLE1BQU0sSUFBSSxXQUFXLENBQUM7d0JBQ3RCLE1BQU0sSUFBSSxDQUFDLENBQUM7cUJBQ2I7eUJBQU0sSUFBSSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsV0FBVyxFQUFFLGNBQWMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxFQUFFO3dCQUNyRSxJQUFJLGNBQWMsQ0FBQyxNQUFNLENBQUMsS0FBSyxHQUFHLEVBQUU7NEJBQ2xDLElBQUksTUFBTSxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsRUFBRTtnQ0FDM0IsTUFBTSxJQUFJLENBQUMsQ0FBQztnQ0FDWixNQUFNLElBQUksQ0FBQyxDQUFDOztvQ0FDTixTQUFTLEdBQVcsUUFBUSxDQUFDLElBQUksQ0FDckMsY0FBYyxDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUUsTUFBTSxDQUFDLENBQ2hDO3NDQUNHLFVBQVUsQ0FBQyxNQUFNO3NDQUNqQixNQUFNO2dDQUNWLElBQUksQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sSUFBSSxDQUFDLENBQUMsQ0FBQztnQ0FDckQsQ0FBQyxFQUFFLENBQUM7Z0NBQ0osU0FBUzs2QkFDVjt5QkFDRjt3QkFDRCxJQUFJLGNBQWMsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLEtBQUssR0FBRyxFQUFFOzRCQUN0QyxJQUFJLE1BQU0sQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUUsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFDLEdBQUcsRUFBRSxFQUFFO2dDQUN6RCxTQUFTOzZCQUNWO3lCQUNGO3dCQUNELElBQUksY0FBYyxDQUFDLE1BQU0sQ0FBQyxLQUFLLEdBQUcsRUFBRTs0QkFDbEMsSUFBSSxNQUFNLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxFQUFFO2dDQUMzQixNQUFNLElBQUksQ0FBQyxDQUFDO2dDQUNaLE1BQU0sSUFBSSxDQUFDLENBQUM7O29DQUNOLFNBQVMsR0FBVyxRQUFRLENBQUMsSUFBSSxDQUNyQyxjQUFjLENBQUMsS0FBSyxDQUFDLENBQUMsRUFBRSxNQUFNLENBQUMsQ0FDaEM7c0NBQ0csVUFBVSxDQUFDLE1BQU07c0NBQ2pCLE1BQU07Z0NBQ1YsSUFBSSxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxJQUFJLENBQUMsQ0FBQyxDQUFDO2dDQUNyRCxDQUFDLEVBQUUsQ0FBQztnQ0FDSixTQUFTOzZCQUNWO3lCQUNGO3dCQUNELElBQUksY0FBYyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsS0FBSyxHQUFHLEVBQUU7NEJBQ3RDLElBQUksTUFBTSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRSxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUMsR0FBRyxFQUFFLEVBQUU7Z0NBQ3pELFNBQVM7NkJBQ1Y7eUJBQ0Y7d0JBQ0QsTUFBTSxJQUFJLFdBQVcsQ0FBQzt3QkFDdEIsTUFBTSxFQUFFLENBQUM7cUJBQ1Y7eUJBQU0sSUFDTCxJQUFJLENBQUMscUJBQXFCLENBQUMsT0FBTyxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUNqRTt3QkFDQSxNQUFNLElBQUksY0FBYyxDQUFDLE1BQU0sQ0FBQyxDQUFDO3dCQUNqQyxNQUFNLEVBQUUsQ0FBQzs7NEJBQ0gsU0FBUyxHQUFXLFFBQVEsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUUsTUFBTSxDQUFDLENBQUM7OEJBQ3BFLFVBQVUsQ0FBQyxNQUFNOzhCQUNqQixNQUFNO3dCQUNWLElBQUksQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sSUFBSSxDQUFDLENBQUMsQ0FBQzt3QkFDckQsQ0FBQyxFQUFFLENBQUM7cUJBQ0w7eUJBQU0sSUFDTCxJQUFJLENBQUMscUJBQXFCLENBQUMsT0FBTyxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsQ0FBQzt3QkFDcEQsSUFBSSxDQUFDLHFCQUFxQixDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUMsQ0FBQzt3QkFDbEQsSUFBSSxDQUFDLHFCQUFxQixDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLFFBQVEsRUFDM0Q7d0JBQ0EsTUFBTSxFQUFFLENBQUM7d0JBQ1QsQ0FBQyxFQUFFLENBQUM7cUJBQ0w7eUJBQU0sSUFDTCxJQUFJLENBQUMsY0FBYyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsS0FBSyxHQUFHO3dCQUN2QyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUM7d0JBQ3RELElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxXQUFXLENBQUMsS0FBSyxJQUFJLENBQUMsY0FBYyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsRUFDdEU7d0JBQ0EsTUFBTSxJQUFJLENBQUMsQ0FBQzt3QkFDWixNQUFNLElBQUksV0FBVyxDQUFDO3FCQUN2QjtpQkFDRjtnQkFFRCxJQUNFLE1BQU0sQ0FBQyxNQUFNLEdBQUcsQ0FBQyxLQUFLLGNBQWMsQ0FBQyxNQUFNO29CQUMzQyxJQUFJLENBQUMscUJBQXFCLENBQUMsT0FBTyxDQUNoQyxjQUFjLENBQUMsY0FBYyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FDMUMsS0FBSyxDQUFDLENBQUMsRUFDUjtvQkFDQSxNQUFNLElBQUksY0FBYyxDQUFDLGNBQWMsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUM7aUJBQ3JEOztvQkFFRyxLQUFLLEdBQUcsQ0FBQzs7b0JBQ1QsV0FBVyxHQUFXLFFBQVEsR0FBRyxDQUFDO2dCQUV0QyxPQUFPLElBQUksQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLFdBQVcsQ0FBQyxFQUFFO29CQUNuQyxLQUFLLEVBQUUsQ0FBQztvQkFDUixXQUFXLEVBQUUsQ0FBQztpQkFDZjtnQkFFRCxFQUFFLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLEdBQUcsS0FBSyxHQUFHLENBQUMsQ0FBQyxDQUFDOztvQkFDdEMsR0FBRyxHQUFHLEtBQUcsSUFBSSxDQUFDLE1BQU0sR0FBRyxNQUFRO2dCQUNuQyxHQUFHO29CQUNELElBQUksQ0FBQyxLQUFLLElBQUksTUFBTSxLQUFLLGNBQWMsQ0FBQyxNQUFNOzBCQUMxQyxLQUFHLElBQUksQ0FBQyxNQUFNLEdBQUcsTUFBTSxHQUFHLElBQUksQ0FBQyxLQUFPOzBCQUN0QyxLQUFHLElBQUksQ0FBQyxNQUFNLEdBQUcsTUFBUSxDQUFDO2dCQUNoQyxPQUFPLEdBQUcsQ0FBQzthQUNaOzs7OztRQUNNLDZDQUFnQjs7OztZQUF2QixVQUF3QixXQUFtQjs7b0JBQ25DLE1BQU0sR0FBdUIsSUFBSSxDQUFDLHFCQUFxQixDQUFDLElBQUksQ0FDaEUsVUFBQyxHQUFXLElBQUssT0FBQSxHQUFHLEtBQUssV0FBVyxHQUFBLENBQ3JDO2dCQUNELE9BQU8sTUFBTSxDQUFDO2FBQ2Y7Ozs7Ozs7UUFFTyw2Q0FBZ0I7Ozs7OztZQUF4QixVQUF5QixXQUFtQixFQUFFLFVBQWtCO2dCQUM5RCxJQUFJLENBQUMscUJBQXFCLEdBQUcsSUFBSSxDQUFDLGFBQWE7c0JBQzNDLElBQUksQ0FBQyxhQUFhO3NCQUNsQixJQUFJLENBQUMscUJBQXFCLENBQUM7Z0JBQy9CLFFBQ0UsSUFBSSxDQUFDLHFCQUFxQixDQUFDLFVBQVUsQ0FBQztvQkFDdEMsSUFBSSxDQUFDLHFCQUFxQixDQUFDLFVBQVUsQ0FBQyxDQUFDLE9BQU87b0JBQzlDLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxVQUFVLENBQUMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxFQUNoRTthQUNIOztvQkExTUZDLGVBQVU7Ozs7O3dEQWNXQyxXQUFNLFNBQUMsTUFBTTs7O1FBNkxuQyx5QkFBQztLQTNNRDs7Ozs7OztRQ0lpQ0MsK0JBQWtCO1FBUWpELHFCQUU0QixRQUFhLEVBQ2IsT0FBZ0IsRUFDbEMsV0FBdUIsRUFDdkIsU0FBb0IsRUFDcEIsVUFBcUI7WUFOL0IsWUFRRSxrQkFBTSxPQUFPLENBQUMsU0FtQmY7WUF6QjJCLGNBQVEsR0FBUixRQUFRLENBQUs7WUFDYixhQUFPLEdBQVAsT0FBTyxDQUFTO1lBQ2xDLGlCQUFXLEdBQVgsV0FBVyxDQUFZO1lBQ3ZCLGVBQVMsR0FBVCxTQUFTLENBQVc7WUFDcEIsZ0JBQVUsR0FBVixVQUFVLENBQVc7WUFieEIsb0JBQWMsR0FBRyxFQUFFLENBQUM7WUFDcEIsbUJBQWEsR0FBRyxLQUFLLENBQUM7WUFDdEIsbUJBQWEsR0FBRyxLQUFLLENBQUM7WUFDdEIsaUJBQVcsR0FBRyxFQUFFLENBQUM7WUFHakIsYUFBTyxHQUFHLGVBQVEsQ0FBQztZQVV4QixLQUFJLENBQUMsY0FBYyxDQUFDLFdBQVcsQ0FBQyxhQUFhLENBQUMsQ0FBQztZQUUvQyxVQUFVLENBQUM7Z0JBQ1QsSUFBSSxLQUFJLENBQUMsWUFBWSxDQUFDLFNBQVMsS0FBSyxPQUFPLEVBQUU7O3dCQUNyQyxPQUFPLEdBQUcsS0FBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLENBQUMsYUFBYSxDQUFDLE9BQU8sQ0FBQztvQkFDckUsSUFBSSxPQUFPLElBQUksSUFBSSxFQUFFO3dCQUNuQixLQUFJLENBQUMsY0FBYyxDQUFDLE9BQU8sQ0FBQyxDQUFDO3FCQUM5Qjt5QkFBTTt3QkFDTCxPQUFPLENBQUMsSUFBSSxDQUNWLCtFQUErRSxDQUNoRixDQUFDO3FCQUNIO2lCQUNGO2dCQUVELEtBQUksQ0FBQyxVQUFVLENBQUMsWUFBWSxDQUFDLFNBQVMsQ0FBQyxVQUFDLEtBQWE7b0JBQ25ELEtBQUksQ0FBQyxxQkFBcUIsQ0FBQyxLQUFLLENBQUMsQ0FBQztpQkFDbkMsQ0FBQyxDQUFDO2FBQ0osQ0FBQyxDQUFDOztTQUNKOzs7OztRQUVNLG9DQUFjOzs7O1lBQXJCLFVBQXNCLEVBQW9CO2dCQUN4QyxJQUFJLENBQUMsWUFBWSxHQUFHLEVBQUUsQ0FBQzthQUN4Qjs7Ozs7Ozs7UUFFTSwrQkFBUzs7Ozs7OztZQUFoQixVQUNFLFVBQWtCLEVBQ2xCLGNBQXNCLEVBQ3RCLFFBQW9CLEVBQ3BCLEVBQXVCO2dCQUR2Qix5QkFBQTtvQkFBQSxZQUFvQjs7Z0JBQ3BCLG1CQUFBO29CQUFBLG9CQUF1Qjs7Z0JBRXZCLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLGFBQWE7c0JBQ2pDLElBQUksQ0FBQyxjQUFjLENBQUMsT0FBTyxDQUFDLFFBQVEsRUFBRSxHQUFHLENBQUM7c0JBQzFDLEVBQUUsQ0FBQztnQkFDUCxJQUFJLENBQUMsVUFBVSxJQUFJLElBQUksQ0FBQyxhQUFhLEVBQUU7b0JBQ3JDLE9BQU8sSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDO2lCQUN2Qzs7b0JBRUssTUFBTSxHQUFHLGlCQUFNLFNBQVMsWUFBQyxVQUFVLEVBQUUsY0FBYyxFQUFFLFFBQVEsRUFBRSxFQUFFLENBQUM7Z0JBQ3hFLElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDLE1BQU0sQ0FBQyxDQUFDO2dCQUVuRCxPQUFPLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxNQUFNLENBQUMsQ0FBQzthQUN0Qzs7Ozs7O1FBRU0sdUNBQWlCOzs7OztZQUF4QixVQUNFLFFBQW9CLEVBQ3BCLEVBQXVCO2dCQUR2Qix5QkFBQTtvQkFBQSxZQUFvQjs7Z0JBQ3BCLG1CQUFBO29CQUFBLG9CQUF1Qjs7O29CQUVqQixXQUFXLEdBQW9CLElBQUksQ0FBQyxTQUFTLENBQ2pELElBQUksQ0FBQyxZQUFZLENBQUMsS0FBSyxFQUN2QixJQUFJLENBQUMsY0FBYyxFQUNuQixRQUFRLEVBQ1IsRUFBRSxDQUNIO2dCQUNELElBQUksQ0FBQyxZQUFZLENBQUMsS0FBSyxHQUFHLFdBQVcsQ0FBQztnQkFDdEMsSUFBSSxJQUFJLENBQUMsWUFBWSxLQUFLLElBQUksQ0FBQyxRQUFRLENBQUMsYUFBYSxFQUFFO29CQUNyRCxPQUFPO2lCQUNSO2dCQUNELElBQUksQ0FBQyxpQkFBaUIsRUFBRSxDQUFDO2FBQzFCOzs7O1FBRU0scUNBQWU7OztZQUF0QjtnQkFDRSxJQUFJLElBQUksQ0FBQyxhQUFhLEVBQUU7b0JBQ3RCLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsUUFBUSxFQUFFLEdBQUcsQ0FBQyxDQUFDO2lCQUMvRDthQUNGOzs7O1FBRU0sdUNBQWlCOzs7WUFBeEI7Z0JBQ0UsT0FBTyxDQUFDLEdBQUcsQ0FBQyxzQkFBc0IsQ0FBQyxDQUFDO2dCQUNwQyxJQUNFLElBQUksQ0FBQyxlQUFlLEtBQUssSUFBSTtvQkFDN0IsSUFBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNLEtBQUssSUFBSSxDQUFDLFlBQVksQ0FBQyxLQUFLLENBQUMsTUFBTSxFQUM3RDtvQkFDQSxJQUFJLENBQUMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxDQUFDO29CQUNsQixJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQztpQkFDOUQ7YUFDRjs7Ozs7UUFFTSw4QkFBUTs7OztZQUFmLFVBQWdCLEtBQWE7Z0JBQzNCLElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUNsRCxJQUFJLENBQUMsVUFBVSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUM7YUFDekM7Ozs7OztRQUNNLDRDQUFzQjs7Ozs7WUFBN0IsVUFBOEIsSUFBWSxFQUFFLEtBQXVCO2dCQUNqRSxJQUFJLElBQUksQ0FBQyxZQUFZLEVBQUU7b0JBQ3JCLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxZQUFZLEVBQUUsSUFBSSxFQUFFLEtBQUssQ0FBQyxDQUFDO2lCQUM1RDthQUNGOzs7OztRQUVNLHNDQUFnQjs7OztZQUF2QixVQUF3QixNQUFjOztvQkFDOUIsc0JBQXNCLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FDOUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUMsQ0FDM0I7O29CQUNHLFdBQVcsR0FBb0Isc0JBQXNCO2dCQUV6RCxJQUFJLEtBQUssQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLHFCQUFxQixDQUFDLEVBQUU7b0JBQzdDLFdBQVcsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUM1QixzQkFBc0IsRUFDdEIsSUFBSSxDQUFDLHFCQUFxQixDQUMzQixDQUFDO2lCQUNIO3FCQUFNLElBQUksSUFBSSxDQUFDLHFCQUFxQixFQUFFO29CQUNyQyxXQUFXLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FDNUIsc0JBQXNCLEVBQ3RCLElBQUksQ0FBQyxxQkFBcUIsQ0FDM0IsQ0FBQztvQkFDRixXQUFXLEdBQUcsSUFBSSxDQUFDLGFBQWEsR0FBRyxNQUFNLENBQUMsV0FBVyxDQUFDLEdBQUcsV0FBVyxDQUFDO2lCQUN0RTtnQkFFRCxPQUFPLFdBQVcsQ0FBQzthQUNwQjs7Ozs7OztRQUVPLGlDQUFXOzs7Ozs7WUFBbkIsVUFDRSxLQUFhLEVBQ2IsMEJBQW9DO2dCQUVwQyxPQUFPLEtBQUs7c0JBQ1IsS0FBSyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsMEJBQTBCLENBQUMsRUFBRSxFQUFFLENBQUM7c0JBQ3BFLEtBQUssQ0FBQzthQUNYOzs7Ozs7UUFFTyxtQ0FBYTs7Ozs7WUFBckIsVUFBc0IsS0FBYTtnQkFDakMsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUU7b0JBQ2hCLE9BQU8sS0FBSyxDQUFDO2lCQUNkO2dCQUNELE9BQU8sS0FBSyxHQUFHLEtBQUssQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxFQUFFLENBQUMsR0FBRyxLQUFLLENBQUM7YUFDdkQ7Ozs7OztRQUVPLGtDQUFZOzs7OztZQUFwQixVQUFxQixLQUFhO2dCQUNoQyxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssRUFBRTtvQkFDZixPQUFPLEtBQUssQ0FBQztpQkFDZDtnQkFDRCxPQUFPLEtBQUssR0FBRyxLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxLQUFLLEVBQUUsRUFBRSxDQUFDLEdBQUcsS0FBSyxDQUFDO2FBQ3REOzs7Ozs7UUFFTyxzQ0FBZ0I7Ozs7O1lBQXhCLFVBQXlCLDBCQUFvQztnQkFDM0QsT0FBTyxJQUFJLE1BQU0sQ0FDZiwwQkFBMEIsQ0FBQyxHQUFHLENBQUMsVUFBQyxJQUFZLElBQUssT0FBQSxPQUFLLElBQU0sR0FBQSxDQUFDLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUN2RSxJQUFJLENBQ0wsQ0FBQzthQUNIOzs7Ozs7UUFFTyxzQ0FBZ0I7Ozs7O1lBQXhCLFVBQXlCLE1BQWM7Z0JBQ3JDLElBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxFQUFFO29CQUN2QixPQUFPLE1BQU0sQ0FBQztpQkFDZjs7b0JBQ0ssTUFBTSxHQUFXLE1BQU0sQ0FBQyxNQUFNOztvQkFDOUIsU0FBUyxHQUFXLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLFdBQVc7O29CQUNsRCxhQUFhLEdBQUcsU0FBUyxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUM7Z0JBRTdDLE9BQU8sTUFBTSxHQUFHLGFBQWEsQ0FBQzthQUMvQjs7Ozs7O1FBRU8sMkNBQXFCOzs7OztZQUE3QixVQUE4QixLQUFhOzs7Ozs7OztvQkFPbkMsUUFBUSxHQUFHLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxLQUFLLENBQUM7Z0JBRTdDLElBQUksSUFBSSxDQUFDLGFBQWEsS0FBSyxRQUFRLEVBQUU7b0JBQ25DLE9BQU87aUJBQ1I7O29CQUVHLGlCQUFpQixHQUFXLElBQUk7OztnQkFJcEMsSUFBSSxJQUFJLENBQUMsYUFBYSxJQUFJLElBQUksRUFBRTs7d0JBQ3hCLENBQUMsR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLFFBQVEsRUFBRTtvQkFDdkMsaUJBQWlCLEdBQUcsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQztpQkFDbEQ7Z0JBRUQsSUFBSSxRQUFRLEtBQUssaUJBQWlCLEVBQUU7O3dCQUM1QixFQUFFLEdBQUcsUUFBUSxJQUFJLElBQUksR0FBRyxRQUFRLENBQUMsUUFBUSxFQUFFLEdBQUcsSUFBSTs7d0JBQ2xELENBQUMsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLEVBQUUsRUFBRSxJQUFJLENBQUMsY0FBYyxDQUFDO29CQUNqRCxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDO2lCQUNsQjthQUNGOztvQkFuTUZGLGVBQVU7Ozs7O3dEQVdOQyxXQUFNLFNBQUNFLGVBQVE7d0RBQ2ZGLFdBQU0sU0FBQyxNQUFNO3dCQWxCVEcsZUFBVTt3QkFBc0JDLGNBQVM7d0JBSXpDQyxlQUFTOzs7UUFzTWxCLGtCQUFDO0tBQUEsQ0FuTWdDLGtCQUFrQjs7Ozs7O0FDUG5EO1FBbUJFLHVCQUU0QixRQUFhLEVBQy9CLFlBQXlCLEVBQ3pCLFVBQXFCO1lBRkgsYUFBUSxHQUFSLFFBQVEsQ0FBSztZQUMvQixpQkFBWSxHQUFaLFlBQVksQ0FBYTtZQUN6QixlQUFVLEdBQVYsVUFBVSxDQUFXO1lBVnZCLGNBQVMsR0FBa0IsSUFBSSxDQUFDOztZQUtqQyxZQUFPLEdBQUcsZUFBUSxDQUFDO1NBTXRCO1FBRUosc0JBQ1cseUNBQWM7Ozs7Z0JBRHpCLFVBQzBCLEtBQWE7Z0JBQ3JDLElBQUksQ0FBQyxVQUFVLEdBQUcsS0FBSyxJQUFJLEVBQUUsQ0FBQztnQkFDOUIsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLEVBQUU7b0JBQ3BCLE9BQU87aUJBQ1I7Z0JBQ0QsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUM7Z0JBQ2pELElBQUksQ0FBQyxlQUFlLEVBQUUsQ0FBQzthQUN4Qjs7O1dBQUE7UUFFRCxzQkFDVyw0Q0FBaUI7Ozs7Z0JBRDVCLFVBQzZCLEtBQW1DO2dCQUM5RCxJQUNFLENBQUMsS0FBSztvQkFDTixDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDO3FCQUNwQixLQUFLLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxFQUN2QztvQkFDQSxPQUFPO2lCQUNSO2dCQUNELElBQUksQ0FBQyxZQUFZLENBQUMscUJBQXFCLEdBQUcsS0FBSyxDQUFDO2FBQ2pEOzs7V0FBQTtRQUVELHNCQUNXLG1DQUFROzs7O2dCQURuQixVQUNvQixLQUEwQjtnQkFDNUMsSUFBSSxDQUFDLEtBQUssRUFBRTtvQkFDVixPQUFPO2lCQUNSO2dCQUNELElBQUksQ0FBQyxZQUFZLENBQUMscUJBQXFCLEdBQUcsS0FBSyxDQUFDO2FBQ2pEOzs7V0FBQTtRQUVELHNCQUNXLGlDQUFNOzs7O2dCQURqQixVQUNrQixLQUF3QjtnQkFDeEMsSUFBSSxDQUFDLEtBQUssRUFBRTtvQkFDVixPQUFPO2lCQUNSO2dCQUNELElBQUksQ0FBQyxZQUFZLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQzthQUNsQzs7O1dBQUE7UUFFRCxzQkFDVyxnQ0FBSzs7OztnQkFEaEIsVUFDaUIsS0FBdUI7Z0JBQ3RDLElBQUksQ0FBQyxLQUFLLEVBQUU7b0JBQ1YsT0FBTztpQkFDUjtnQkFDRCxJQUFJLENBQUMsWUFBWSxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUM7YUFDakM7OztXQUFBO1FBRUQsc0JBQ1csZ0RBQXFCOzs7O2dCQURoQyxVQUNpQyxLQUF1QztnQkFDdEUsSUFBSSxDQUFDLFlBQVksQ0FBQyxxQkFBcUIsR0FBRyxLQUFLLENBQUM7YUFDakQ7OztXQUFBO1FBRUQsc0JBQ1csd0NBQWE7Ozs7Z0JBRHhCLFVBQ3lCLEtBQStCO2dCQUN0RCxJQUFJLENBQUMsS0FBSyxFQUFFO29CQUNWLE9BQU87aUJBQ1I7Z0JBQ0QsSUFBSSxDQUFDLFlBQVksQ0FBQyxhQUFhLEdBQUcsS0FBSyxDQUFDO2FBQ3pDOzs7V0FBQTtRQUVELHNCQUNXLHVDQUFZOzs7O2dCQUR2QixVQUN3QixLQUE4QjtnQkFDcEQsSUFBSSxDQUFDLFlBQVksQ0FBQyxZQUFZLEdBQUcsS0FBSyxDQUFDO2FBQ3hDOzs7V0FBQTtRQUVELHNCQUNXLDBDQUFlOzs7O2dCQUQxQixVQUMyQixLQUFpQztnQkFDMUQsSUFBSSxDQUFDLFlBQVksQ0FBQyxlQUFlLEdBQUcsS0FBSyxDQUFDO2FBQzNDOzs7V0FBQTs7Ozs7UUFHTSwrQkFBTzs7OztZQURkLFVBQ2UsQ0FBZ0I7O29CQUN2QixFQUFFLHNCQUFxQixDQUFDLENBQUMsTUFBTSxFQUFvQjtnQkFDekQsSUFBSSxDQUFDLFdBQVcsR0FBRyxFQUFFLENBQUMsS0FBSyxDQUFDO2dCQUU1QixJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsRUFBRTtvQkFDcEIsT0FBTztpQkFDUjs7b0JBQ0ssUUFBUSxHQUNaLG9CQUFDLEVBQUUsQ0FBQyxjQUFjLFFBQWdCLENBQUM7c0JBQy9CLG9CQUFDLEVBQUUsQ0FBQyxjQUFjLE1BQWMsSUFBSSxDQUFDLFlBQVksQ0FBQyxNQUFNLENBQUMsTUFBTTswQ0FDOUQsRUFBRSxDQUFDLGNBQWMsR0FBVzs7b0JBQy9CLFVBQVUsR0FBRyxDQUFDO2dCQUNsQixJQUFJLENBQUMsWUFBWSxDQUFDLGlCQUFpQixDQUNqQyxRQUFRLEVBQ1IsVUFBQyxLQUFhLElBQUssUUFBQyxVQUFVLEdBQUcsS0FBSyxJQUFDLENBQ3hDLENBQUM7O2dCQUVGLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxhQUFhLEtBQUssRUFBRSxFQUFFO29CQUN0QyxPQUFPO2lCQUNSO2dCQUNELEVBQUUsQ0FBQyxjQUFjLEdBQUcsRUFBRSxDQUFDLFlBQVk7b0JBQ2pDLElBQUksQ0FBQyxTQUFTLEtBQUssSUFBSTswQkFDbkIsSUFBSSxDQUFDLFNBQVM7MEJBQ2QsUUFBUTs7NkJBRVAsb0JBQUMsQ0FBQyxJQUFTLFNBQVMsS0FBSyx1QkFBdUIsR0FBRyxDQUFDLEdBQUcsVUFBVSxDQUFDLENBQUM7Z0JBQzFFLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDO2FBQ3ZCOzs7O1FBR00sOEJBQU07OztZQURiO2dCQUVFLElBQUksQ0FBQyxZQUFZLENBQUMsaUJBQWlCLEVBQUUsQ0FBQztnQkFDdEMsSUFBSSxDQUFDLE9BQU8sRUFBRSxDQUFDO2FBQ2hCOzs7OztRQUdNLCtCQUFPOzs7O1lBRGQsVUFDZSxDQUE2Qjs7b0JBQ3BDLEVBQUUsc0JBQXFCLENBQUMsQ0FBQyxNQUFNLEVBQW9CO2dCQUV6RCxJQUNFLEVBQUUsS0FBSyxJQUFJO29CQUNYLEVBQUUsQ0FBQyxjQUFjLEtBQUssSUFBSTtvQkFDMUIsRUFBRSxDQUFDLGNBQWMsS0FBSyxFQUFFLENBQUMsWUFBWTtvQkFDckMsRUFBRSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLE1BQU0sQ0FBQyxNQUFNOztvQkFFbkQsb0JBQUMsQ0FBQyxJQUFTLE9BQU8sS0FBSyxFQUFFLEVBQ3pCO29CQUNBLE9BQU87aUJBQ1I7Z0JBQ0QsSUFBSSxJQUFJLENBQUMsWUFBWSxDQUFDLGFBQWEsRUFBRTtvQkFDbkMsSUFBSSxDQUFDLFlBQVksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxjQUFjLENBQUMsT0FBTyxDQUN0RSxRQUFRLEVBQ1IsR0FBRyxDQUNKLENBQUM7aUJBQ0g7Z0JBQ0QsRUFBRSxDQUFDLEtBQUs7b0JBQ04sQ0FBQyxFQUFFLENBQUMsS0FBSyxJQUFJLEVBQUUsQ0FBQyxLQUFLLEtBQUssSUFBSSxDQUFDLFlBQVksQ0FBQyxNQUFNOzBCQUM5QyxJQUFJLENBQUMsWUFBWSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLFdBQVc7MEJBQ3hELEVBQUUsQ0FBQyxLQUFLLENBQUM7O2dCQUVmLElBQ0UsQ0FBQyxvQkFBQyxFQUFFLENBQUMsY0FBYywyQkFBZ0IsRUFBRSxDQUFDLFlBQVksR0FBVztvQkFDN0QsSUFBSSxDQUFDLFlBQVksQ0FBQyxNQUFNLENBQUMsTUFBTSxFQUMvQjtvQkFDQSxFQUFFLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQztvQkFDcEQsT0FBTztpQkFDUjthQUNGOzs7OztRQUdNLGlDQUFTOzs7O1lBRGhCLFVBQ2lCLENBQWdCOztvQkFDekIsRUFBRSxzQkFBcUIsQ0FBQyxDQUFDLE1BQU0sRUFBb0I7Z0JBQ3pELElBQUksQ0FBQyxDQUFDLE9BQU8sS0FBSyxFQUFFLEVBQUU7b0JBQ3BCLENBQUMsQ0FBQyxjQUFjLEVBQUUsQ0FBQztpQkFDcEI7Z0JBQ0QsSUFBSSxDQUFDLENBQUMsT0FBTyxLQUFLLEVBQUUsSUFBSSxDQUFDLENBQUMsT0FBTyxLQUFLLENBQUMsRUFBRTtvQkFDdkMsSUFDRSxvQkFBQyxFQUFFLENBQUMsY0FBYyxPQUFlLElBQUksQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDLE1BQU07d0JBQ2hFLG9CQUFDLEVBQUUsQ0FBQyxZQUFZLE9BQWUsSUFBSSxDQUFDLFlBQVksQ0FBQyxNQUFNLENBQUMsTUFBTSxFQUM5RDt3QkFDQSxDQUFDLENBQUMsY0FBYyxFQUFFLENBQUM7cUJBQ3BCO29CQUNELElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUM7b0JBQ2hCLElBQ0UsQ0FBQyxDQUFDLE9BQU8sS0FBSyxDQUFDO3dCQUNmLEVBQUUsQ0FBQyxjQUFjLEtBQUssQ0FBQzt3QkFDdkIsRUFBRSxDQUFDLFlBQVksS0FBSyxFQUFFLENBQUMsS0FBSyxDQUFDLE1BQU0sRUFDbkM7d0JBQ0EsRUFBRSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLE1BQU0sQ0FBQzt3QkFDcEMsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLE1BQU07OEJBQ3JDLElBQUksQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDLE1BQU07OEJBQy9CLENBQUMsQ0FBQzt3QkFDTixJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDO3FCQUNqQjtpQkFDRjthQUNGOzs7O1FBR00sK0JBQU87OztZQURkO2dCQUVFLElBQUksQ0FBQyxTQUFTLEdBQUcsTUFBTSxDQUFDLGdCQUFnQixDQUFDO2FBQzFDOzs7Ozs7O1FBR00sd0NBQWdCOzs7OztZQUF2QixVQUF3QixVQUFtQjtnQkFDekMsSUFBSSxDQUFDLFlBQVksQ0FBQyxzQkFBc0IsQ0FBQyxVQUFVLEVBQUUsVUFBVSxDQUFDLENBQUM7YUFDbEU7Ozs7OztRQUVPLDZDQUFxQjs7Ozs7WUFBN0IsVUFBOEIsT0FBZTtnQkFBN0MsaUJBd0JDO2dCQXZCQyxRQUNFLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxVQUFVLENBQUM7b0JBQ3hCLE9BQU87eUJBQ0osS0FBSyxDQUFDLEVBQUUsQ0FBQzt5QkFDVCxNQUFNLENBQUMsVUFBQyxLQUFhLEVBQUUsT0FBZSxFQUFFLEtBQWE7d0JBQ3BELEtBQUksQ0FBQyxNQUFNLEdBQUcsT0FBTyxLQUFLLEdBQUcsR0FBRyxLQUFLLEdBQUcsS0FBSSxDQUFDLE1BQU0sQ0FBQzt3QkFFcEQsSUFBSSxPQUFPLEtBQUssR0FBRyxFQUFFOzRCQUNuQixPQUFPLEtBQUksQ0FBQyxZQUFZLENBQUMsZ0JBQWdCLENBQUMsT0FBTyxDQUFDO2tDQUM5QyxLQUFLLEdBQUcsT0FBTztrQ0FDZixLQUFLLENBQUM7eUJBQ1g7d0JBQ0QsS0FBSSxDQUFDLElBQUksR0FBRyxLQUFLLENBQUM7OzRCQUNaLFlBQVksR0FBVyxNQUFNLENBQ2pDLE9BQU8sQ0FBQyxLQUFLLENBQUMsS0FBSSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUUsS0FBSSxDQUFDLElBQUksQ0FBQyxDQUMxQzs7NEJBQ0ssVUFBVSxHQUFXLElBQUksS0FBSyxDQUFDLFlBQVksR0FBRyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQ3pELE9BQU8sQ0FBQyxLQUFJLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUN6Qjt3QkFDRCxPQUFPLEtBQUssR0FBRyxVQUFVLENBQUM7cUJBQzNCLEVBQUUsRUFBRSxDQUFDO29CQUNWLE9BQU8sRUFDUDthQUNIOzs7OztRQUVPLHVDQUFlOzs7O1lBQXZCO2dCQUNFLElBQUksQ0FBQyxZQUFZLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQyxxQkFBcUIsQ0FDM0QsSUFBSSxDQUFDLFVBQVUsQ0FDaEIsQ0FBQzs7b0JBRUksQ0FBQyxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsU0FBUyxDQUNuQyxJQUFJLENBQUMsV0FBVyxFQUNoQixJQUFJLENBQUMsWUFBWSxDQUFDLGNBQWMsQ0FDakM7Z0JBRUQsSUFBSSxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUM7YUFDL0I7O29CQTFPRkMsY0FBUyxTQUFDO3dCQUNULFFBQVEsRUFBRSxRQUFRO3dCQUNsQixTQUFTLEVBQUUsQ0FBQyxXQUFXLENBQUM7cUJBQ3pCOzs7Ozt3REFZSU4sV0FBTSxTQUFDRSxlQUFRO3dCQWxCWCxXQUFXO3dCQURYRyxlQUFTOzs7O3FDQXdCZkUsVUFBSyxTQUFDLE1BQU07d0NBVVpBLFVBQUs7K0JBWUxBLFVBQUs7NkJBUUxBLFVBQUs7NEJBUUxBLFVBQUs7NENBUUxBLFVBQUs7b0NBS0xBLFVBQUs7bUNBUUxBLFVBQUs7c0NBS0xBLFVBQUs7OEJBS0xDLGlCQUFZLFNBQUMsT0FBTyxFQUFFLENBQUMsUUFBUSxDQUFDOzZCQThCaENBLGlCQUFZLFNBQUMsTUFBTTs4QkFNbkJBLGlCQUFZLFNBQUMsT0FBTyxFQUFFLENBQUMsUUFBUSxDQUFDO2dDQWtDaENBLGlCQUFZLFNBQUMsU0FBUyxFQUFFLENBQUMsUUFBUSxDQUFDOzhCQTRCbENBLGlCQUFZLFNBQUMsT0FBTzs7UUFnRHZCLG9CQUFDO0tBM09EOzs7Ozs7QUNOQTtRQVNFLGtCQUEyQixZQUFnQztZQUFoQyxpQkFBWSxHQUFaLFlBQVksQ0FBb0I7U0FBSTs7Ozs7O1FBRXhELDRCQUFTOzs7OztZQUFoQixVQUNFLEtBQXNCLEVBQ3RCLElBQTRDO2dCQUU1QyxJQUFJLENBQUMsS0FBSyxFQUFFO29CQUNWLE9BQU8sRUFBRSxDQUFDO2lCQUNYO2dCQUNELElBQUksT0FBTyxJQUFJLEtBQUssUUFBUSxFQUFFO29CQUM1QixPQUFPLElBQUksQ0FBQyxZQUFZLENBQUMsU0FBUyxDQUFDLEtBQUcsS0FBTyxFQUFFLElBQUksQ0FBQyxDQUFDO2lCQUN0RDtnQkFDRCxPQUFPLElBQUksQ0FBQyxZQUFZLENBQUMsb0JBQW9CLENBQUMsS0FBRyxLQUFPLEVBQUUsSUFBSSxDQUFDLENBQUM7YUFDakU7O29CQWxCRkMsU0FBSSxTQUFDO3dCQUNKLElBQUksRUFBRSxNQUFNO3dCQUNaLElBQUksRUFBRSxJQUFJO3FCQUNYOzs7Ozt3QkFOUSxrQkFBa0I7OztRQXNCM0IsZUFBQztLQW5CRDs7Ozs7OztRQ1NBO1NBZ0NDOzs7OztRQTFCZSwwQkFBTzs7OztZQUFyQixVQUFzQixXQUEyQjtnQkFDL0MsT0FBTztvQkFDTCxRQUFRLEVBQUUsa0JBQWtCO29CQUM1QixTQUFTLEVBQUU7d0JBQ1Q7NEJBQ0UsT0FBTyxFQUFFLFVBQVU7NEJBQ25CLFFBQVEsRUFBRSxXQUFXO3lCQUN0Qjt3QkFDRDs0QkFDRSxPQUFPLEVBQUUsY0FBYzs0QkFDdkIsUUFBUSxFQUFFLGFBQWE7eUJBQ3hCO3dCQUNEOzRCQUNFLE9BQU8sRUFBRSxNQUFNOzRCQUNmLFVBQVUsRUFBRSxjQUFjOzRCQUMxQixJQUFJLEVBQUUsQ0FBQyxjQUFjLEVBQUUsVUFBVSxDQUFDO3lCQUNuQzt3QkFDRCxRQUFRO3FCQUNUO2lCQUNGLENBQUM7YUFDSDs7Ozs7UUFDYSwyQkFBUTs7OztZQUF0QixVQUF1QixXQUEyQjtnQkFDaEQsT0FBTztvQkFDTCxRQUFRLEVBQUUsa0JBQWtCO2lCQUM3QixDQUFDO2FBQ0g7O29CQS9CRkMsYUFBUSxTQUFDO3dCQUNSLFNBQVMsRUFBRSxDQUFDLGtCQUFrQixFQUFFLFFBQVEsQ0FBQzt3QkFDekMsT0FBTyxFQUFFLENBQUMsYUFBYSxFQUFFLFFBQVEsQ0FBQzt3QkFDbEMsWUFBWSxFQUFFLENBQUMsYUFBYSxFQUFFLFFBQVEsQ0FBQztxQkFDeEM7O1FBNEJELHlCQUFDO0tBaENELElBZ0NDOzs7Ozs7O0FBS0QsYUFBZ0IsY0FBYyxDQUM1QixVQUF5QixFQUN6QixXQUFrRDtRQUVsRCxPQUFPLE9BQU8sV0FBVyxLQUFLLFVBQVU7Y0FDcEMsV0FBVyxFQUFFOzJCQUNSLFVBQVUsRUFBSyxXQUFXLENBQUUsQ0FBQztJQUN4QyxDQUFDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OyJ9