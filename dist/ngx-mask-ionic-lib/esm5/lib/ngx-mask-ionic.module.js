/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { config, INITIAL_CONFIG, initialConfig, NEW_CONFIG } from './config';
import { MaskApplierService } from './mask-applier.service';
import { MaskDirective } from './mask.directive';
import { MaskPipe } from './mask.pipe';
import { NgModule } from '@angular/core';
var NgxMaskIonicModule = /** @class */ (function () {
    function NgxMaskIonicModule() {
    }
    /**
     * @param {?=} configValue
     * @return {?}
     */
    NgxMaskIonicModule.forRoot = /**
     * @param {?=} configValue
     * @return {?}
     */
    function (configValue) {
        return {
            ngModule: NgxMaskIonicModule,
            providers: [
                {
                    provide: NEW_CONFIG,
                    useValue: configValue
                },
                {
                    provide: INITIAL_CONFIG,
                    useValue: initialConfig
                },
                {
                    provide: config,
                    useFactory: _configFactory,
                    deps: [INITIAL_CONFIG, NEW_CONFIG]
                },
                MaskPipe
            ]
        };
    };
    /**
     * @param {?=} configValue
     * @return {?}
     */
    NgxMaskIonicModule.forChild = /**
     * @param {?=} configValue
     * @return {?}
     */
    function (configValue) {
        return {
            ngModule: NgxMaskIonicModule
        };
    };
    NgxMaskIonicModule.decorators = [
        { type: NgModule, args: [{
                    providers: [MaskApplierService, MaskPipe],
                    exports: [MaskDirective, MaskPipe],
                    declarations: [MaskDirective, MaskPipe]
                },] }
    ];
    return NgxMaskIonicModule;
}());
export { NgxMaskIonicModule };
/**
 * \@internal
 * @param {?} initConfig
 * @param {?} configValue
 * @return {?}
 */
export function _configFactory(initConfig, configValue) {
    return typeof configValue === 'function'
        ? configValue()
        : tslib_1.__assign({}, initConfig, configValue);
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibmd4LW1hc2staW9uaWMubW9kdWxlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbmd4LW1hc2staW9uaWMvIiwic291cmNlcyI6WyJsaWIvbmd4LW1hc2staW9uaWMubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsT0FBTyxFQUNMLE1BQU0sRUFDTixjQUFjLEVBQ2QsYUFBYSxFQUNiLFVBQVUsRUFFVCxNQUFNLFVBQVUsQ0FBQztBQUNwQixPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSx3QkFBd0IsQ0FBQztBQUM1RCxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sa0JBQWtCLENBQUM7QUFDakQsT0FBTyxFQUFFLFFBQVEsRUFBRSxNQUFNLGFBQWEsQ0FBQztBQUN2QyxPQUFPLEVBQXVCLFFBQVEsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUc5RDtJQUFBO0lBZ0NBLENBQUM7Ozs7O0lBMUJlLDBCQUFPOzs7O0lBQXJCLFVBQXNCLFdBQTJCO1FBQy9DLE9BQU87WUFDTCxRQUFRLEVBQUUsa0JBQWtCO1lBQzVCLFNBQVMsRUFBRTtnQkFDVDtvQkFDRSxPQUFPLEVBQUUsVUFBVTtvQkFDbkIsUUFBUSxFQUFFLFdBQVc7aUJBQ3RCO2dCQUNEO29CQUNFLE9BQU8sRUFBRSxjQUFjO29CQUN2QixRQUFRLEVBQUUsYUFBYTtpQkFDeEI7Z0JBQ0Q7b0JBQ0UsT0FBTyxFQUFFLE1BQU07b0JBQ2YsVUFBVSxFQUFFLGNBQWM7b0JBQzFCLElBQUksRUFBRSxDQUFDLGNBQWMsRUFBRSxVQUFVLENBQUM7aUJBQ25DO2dCQUNELFFBQVE7YUFDVDtTQUNGLENBQUM7SUFDSixDQUFDOzs7OztJQUNhLDJCQUFROzs7O0lBQXRCLFVBQXVCLFdBQTJCO1FBQ2hELE9BQU87WUFDTCxRQUFRLEVBQUUsa0JBQWtCO1NBQzdCLENBQUM7SUFDSixDQUFDOztnQkEvQkYsUUFBUSxTQUFDO29CQUNSLFNBQVMsRUFBRSxDQUFDLGtCQUFrQixFQUFFLFFBQVEsQ0FBQztvQkFDekMsT0FBTyxFQUFFLENBQUMsYUFBYSxFQUFFLFFBQVEsQ0FBQztvQkFDbEMsWUFBWSxFQUFFLENBQUMsYUFBYSxFQUFFLFFBQVEsQ0FBQztpQkFDeEM7O0lBNEJELHlCQUFDO0NBQUEsQUFoQ0QsSUFnQ0M7U0EzQlksa0JBQWtCOzs7Ozs7O0FBZ0MvQixNQUFNLFVBQVUsY0FBYyxDQUM1QixVQUF5QixFQUN6QixXQUFrRDtJQUVsRCxPQUFPLE9BQU8sV0FBVyxLQUFLLFVBQVU7UUFDdEMsQ0FBQyxDQUFDLFdBQVcsRUFBRTtRQUNmLENBQUMsc0JBQU0sVUFBVSxFQUFLLFdBQVcsQ0FBRSxDQUFDO0FBQ3hDLENBQUMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge1xyXG4gIGNvbmZpZyxcclxuICBJTklUSUFMX0NPTkZJRyxcclxuICBpbml0aWFsQ29uZmlnLFxyXG4gIE5FV19DT05GSUcsXHJcbiAgb3B0aW9uc0NvbmZpZ1xyXG4gIH0gZnJvbSAnLi9jb25maWcnO1xyXG5pbXBvcnQgeyBNYXNrQXBwbGllclNlcnZpY2UgfSBmcm9tICcuL21hc2stYXBwbGllci5zZXJ2aWNlJztcclxuaW1wb3J0IHsgTWFza0RpcmVjdGl2ZSB9IGZyb20gJy4vbWFzay5kaXJlY3RpdmUnO1xyXG5pbXBvcnQgeyBNYXNrUGlwZSB9IGZyb20gJy4vbWFzay5waXBlJztcclxuaW1wb3J0IHsgTW9kdWxlV2l0aFByb3ZpZGVycywgTmdNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuXHJcblxyXG5ATmdNb2R1bGUoe1xyXG4gIHByb3ZpZGVyczogW01hc2tBcHBsaWVyU2VydmljZSwgTWFza1BpcGVdLFxyXG4gIGV4cG9ydHM6IFtNYXNrRGlyZWN0aXZlLCBNYXNrUGlwZV0sXHJcbiAgZGVjbGFyYXRpb25zOiBbTWFza0RpcmVjdGl2ZSwgTWFza1BpcGVdXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBOZ3hNYXNrSW9uaWNNb2R1bGUge1xyXG4gIHB1YmxpYyBzdGF0aWMgZm9yUm9vdChjb25maWdWYWx1ZT86IG9wdGlvbnNDb25maWcpOiBNb2R1bGVXaXRoUHJvdmlkZXJzIHtcclxuICAgIHJldHVybiB7XHJcbiAgICAgIG5nTW9kdWxlOiBOZ3hNYXNrSW9uaWNNb2R1bGUsXHJcbiAgICAgIHByb3ZpZGVyczogW1xyXG4gICAgICAgIHtcclxuICAgICAgICAgIHByb3ZpZGU6IE5FV19DT05GSUcsXHJcbiAgICAgICAgICB1c2VWYWx1ZTogY29uZmlnVmFsdWVcclxuICAgICAgICB9LFxyXG4gICAgICAgIHtcclxuICAgICAgICAgIHByb3ZpZGU6IElOSVRJQUxfQ09ORklHLFxyXG4gICAgICAgICAgdXNlVmFsdWU6IGluaXRpYWxDb25maWdcclxuICAgICAgICB9LFxyXG4gICAgICAgIHtcclxuICAgICAgICAgIHByb3ZpZGU6IGNvbmZpZyxcclxuICAgICAgICAgIHVzZUZhY3Rvcnk6IF9jb25maWdGYWN0b3J5LFxyXG4gICAgICAgICAgZGVwczogW0lOSVRJQUxfQ09ORklHLCBORVdfQ09ORklHXVxyXG4gICAgICAgIH0sXHJcbiAgICAgICAgTWFza1BpcGVcclxuICAgICAgXVxyXG4gICAgfTtcclxuICB9XHJcbiAgcHVibGljIHN0YXRpYyBmb3JDaGlsZChjb25maWdWYWx1ZT86IG9wdGlvbnNDb25maWcpOiBNb2R1bGVXaXRoUHJvdmlkZXJzIHtcclxuICAgIHJldHVybiB7XHJcbiAgICAgIG5nTW9kdWxlOiBOZ3hNYXNrSW9uaWNNb2R1bGVcclxuICAgIH07XHJcbiAgfVxyXG59XHJcblxyXG4vKipcclxuICogQGludGVybmFsXHJcbiAqL1xyXG5leHBvcnQgZnVuY3Rpb24gX2NvbmZpZ0ZhY3RvcnkoXHJcbiAgaW5pdENvbmZpZzogb3B0aW9uc0NvbmZpZyxcclxuICBjb25maWdWYWx1ZTogb3B0aW9uc0NvbmZpZyB8ICgoKSA9PiBvcHRpb25zQ29uZmlnKVxyXG4pOiBGdW5jdGlvbiB8IG9wdGlvbnNDb25maWcge1xyXG4gIHJldHVybiB0eXBlb2YgY29uZmlnVmFsdWUgPT09ICdmdW5jdGlvbidcclxuICAgID8gY29uZmlnVmFsdWUoKVxyXG4gICAgOiB7IC4uLmluaXRDb25maWcsIC4uLmNvbmZpZ1ZhbHVlIH07XHJcbn1cclxuIl19