/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { ElementRef, Inject, Injectable, Renderer2 } from '@angular/core';
import { config } from './config';
import { DOCUMENT } from '@angular/common';
import { MaskApplierService } from './mask-applier.service';
import { NgControl } from '@angular/forms';
var MaskService = /** @class */ (function (_super) {
    tslib_1.__extends(MaskService, _super);
    function MaskService(document, _config, _elementRef, _renderer, _ngControl) {
        var _this = _super.call(this, _config) || this;
        _this.document = document;
        _this._config = _config;
        _this._elementRef = _elementRef;
        _this._renderer = _renderer;
        _this._ngControl = _ngControl;
        _this.maskExpression = '';
        _this.isNumberValue = false;
        _this.showMaskTyped = false;
        _this.maskIsShown = '';
        _this.onTouch = function () { };
        _this.setFormElement(_elementRef.nativeElement);
        setTimeout(function () {
            if (_this._formElement.localName !== 'input') {
                /** @type {?} */
                var inputEl = _this._elementRef.nativeElement.querySelector('input');
                if (inputEl != null) {
                    _this.setFormElement(inputEl);
                }
                else {
                    console.warn('mask-service: Could not find Input Element.  Please make sure one is present.');
                }
            }
            _this._ngControl.valueChanges.subscribe(function (value) {
                _this._onControlValueChange(value);
            });
        });
        return _this;
    }
    /**
     * @param {?} el
     * @return {?}
     */
    MaskService.prototype.setFormElement = /**
     * @param {?} el
     * @return {?}
     */
    function (el) {
        this._formElement = el;
    };
    /**
     * @param {?} inputValue
     * @param {?} maskExpression
     * @param {?=} position
     * @param {?=} cb
     * @return {?}
     */
    MaskService.prototype.applyMask = /**
     * @param {?} inputValue
     * @param {?} maskExpression
     * @param {?=} position
     * @param {?=} cb
     * @return {?}
     */
    function (inputValue, maskExpression, position, cb) {
        if (position === void 0) { position = 0; }
        if (cb === void 0) { cb = function () { }; }
        this.maskIsShown = this.showMaskTyped
            ? this.maskExpression.replace(/[0-9]/g, '_')
            : '';
        if (!inputValue && this.showMaskTyped) {
            return this.prefix + this.maskIsShown;
        }
        /** @type {?} */
        var result = _super.prototype.applyMask.call(this, inputValue, maskExpression, position, cb);
        this.unmaskedValue = this.getUnmaskedValue(result);
        return this._applyMaskResult(result);
    };
    /**
     * @param {?=} position
     * @param {?=} cb
     * @return {?}
     */
    MaskService.prototype.applyValueChanges = /**
     * @param {?=} position
     * @param {?=} cb
     * @return {?}
     */
    function (position, cb) {
        if (position === void 0) { position = 0; }
        if (cb === void 0) { cb = function () { }; }
        /** @type {?} */
        var maskedInput = this.applyMask(this._formElement.value, this.maskExpression, position, cb);
        this._formElement.value = maskedInput;
        if (this._formElement === this.document.activeElement) {
            return;
        }
        this.clearIfNotMatchFn();
    };
    /**
     * @return {?}
     */
    MaskService.prototype.showMaskInInput = /**
     * @return {?}
     */
    function () {
        if (this.showMaskTyped) {
            this.maskIsShown = this.maskExpression.replace(/[0-9]/g, '_');
        }
    };
    /**
     * @return {?}
     */
    MaskService.prototype.clearIfNotMatchFn = /**
     * @return {?}
     */
    function () {
        if (this.clearIfNotMatch === true &&
            this.maskExpression.length !== this._formElement.value.length) {
            this.setValue('');
            this.applyMask(this._formElement.value, this.maskExpression);
        }
    };
    /**
     * @param {?} value
     * @return {?}
     */
    MaskService.prototype.setValue = /**
     * @param {?} value
     * @return {?}
     */
    function (value) {
        this.unmaskedValue = this.getUnmaskedValue(value);
        this._ngControl.control.setValue(value);
    };
    /**
     * @param {?} name
     * @param {?} value
     * @return {?}
     */
    MaskService.prototype.setFormElementProperty = /**
     * @param {?} name
     * @param {?} value
     * @return {?}
     */
    function (name, value) {
        if (this._formElement) {
            this._renderer.setProperty(this._formElement, name, value);
        }
    };
    /**
     * @param {?} result
     * @return {?}
     */
    MaskService.prototype.getUnmaskedValue = /**
     * @param {?} result
     * @return {?}
     */
    function (result) {
        /** @type {?} */
        var resultNoSuffixOrPrefix = this._removeSufix(this._removePrefix(result));
        /** @type {?} */
        var changeValue = resultNoSuffixOrPrefix;
        if (Array.isArray(this.dropSpecialCharacters)) {
            changeValue = this._removeMask(resultNoSuffixOrPrefix, this.dropSpecialCharacters);
        }
        else if (this.dropSpecialCharacters) {
            changeValue = this._removeMask(resultNoSuffixOrPrefix, this.maskSpecialCharacters);
            changeValue = this.isNumberValue ? Number(changeValue) : changeValue;
        }
        return changeValue;
    };
    /**
     * @private
     * @param {?} value
     * @param {?} specialCharactersForRemove
     * @return {?}
     */
    MaskService.prototype._removeMask = /**
     * @private
     * @param {?} value
     * @param {?} specialCharactersForRemove
     * @return {?}
     */
    function (value, specialCharactersForRemove) {
        return value
            ? value.replace(this._regExpForRemove(specialCharactersForRemove), '')
            : value;
    };
    /**
     * @private
     * @param {?} value
     * @return {?}
     */
    MaskService.prototype._removePrefix = /**
     * @private
     * @param {?} value
     * @return {?}
     */
    function (value) {
        if (!this.prefix) {
            return value;
        }
        return value ? value.replace(this.prefix, '') : value;
    };
    /**
     * @private
     * @param {?} value
     * @return {?}
     */
    MaskService.prototype._removeSufix = /**
     * @private
     * @param {?} value
     * @return {?}
     */
    function (value) {
        if (!this.sufix) {
            return value;
        }
        return value ? value.replace(this.sufix, '') : value;
    };
    /**
     * @private
     * @param {?} specialCharactersForRemove
     * @return {?}
     */
    MaskService.prototype._regExpForRemove = /**
     * @private
     * @param {?} specialCharactersForRemove
     * @return {?}
     */
    function (specialCharactersForRemove) {
        return new RegExp(specialCharactersForRemove.map(function (item) { return "\\" + item; }).join('|'), 'gi');
    };
    /**
     * @private
     * @param {?} result
     * @return {?}
     */
    MaskService.prototype._applyMaskResult = /**
     * @private
     * @param {?} result
     * @return {?}
     */
    function (result) {
        if (!this.showMaskTyped) {
            return result;
        }
        /** @type {?} */
        var resLen = result.length;
        /** @type {?} */
        var prefNmask = this.prefix + this.maskIsShown;
        /** @type {?} */
        var ifMaskIsShown = prefNmask.slice(resLen);
        return result + ifMaskIsShown;
    };
    /**
     * @private
     * @param {?} value
     * @return {?}
     */
    MaskService.prototype._onControlValueChange = /**
     * @private
     * @param {?} value
     * @return {?}
     */
    function (value) {
        /*
              Because we are no longer working with the ControlValueAccessor (since it doesn't play nice with Ionic).
              We need logic here to track changes made programmatically to the form value.  Specifically changes
              done OUTSIDE of the mask. Since changes done inside the mask may also fire off this method
              we need to do some jiu jitsu to ensure we are ignoring those changes.
            */
        /** @type {?} */
        var newValue = this.getUnmaskedValue(value);
        if (this.unmaskedValue === newValue) {
            return;
        }
        /** @type {?} */
        var unmaskedSubstring = null;
        // This method (value change) fires off before a Keydown or Input event, so we need to subtract
        // the latest change and compare to our previous (known) value.
        if (this.unmaskedValue != null) {
            /** @type {?} */
            var v = this.unmaskedValue.toString();
            unmaskedSubstring = v.substring(0, v.length - 1);
        }
        if (newValue !== unmaskedSubstring) {
            /** @type {?} */
            var nv = newValue != null ? newValue.toString() : null;
            /** @type {?} */
            var v = this.applyMask(nv, this.maskExpression);
            this.setValue(v);
        }
    };
    MaskService.decorators = [
        { type: Injectable }
    ];
    /** @nocollapse */
    MaskService.ctorParameters = function () { return [
        { type: undefined, decorators: [{ type: Inject, args: [DOCUMENT,] }] },
        { type: undefined, decorators: [{ type: Inject, args: [config,] }] },
        { type: ElementRef },
        { type: Renderer2 },
        { type: NgControl }
    ]; };
    return MaskService;
}(MaskApplierService));
export { MaskService };
if (false) {
    /** @type {?} */
    MaskService.prototype.maskExpression;
    /** @type {?} */
    MaskService.prototype.isNumberValue;
    /** @type {?} */
    MaskService.prototype.showMaskTyped;
    /** @type {?} */
    MaskService.prototype.maskIsShown;
    /**
     * @type {?}
     * @private
     */
    MaskService.prototype._formElement;
    /**
     * @type {?}
     * @private
     */
    MaskService.prototype.unmaskedValue;
    /** @type {?} */
    MaskService.prototype.onTouch;
    /**
     * @type {?}
     * @private
     */
    MaskService.prototype.document;
    /**
     * @type {?}
     * @protected
     */
    MaskService.prototype._config;
    /**
     * @type {?}
     * @private
     */
    MaskService.prototype._elementRef;
    /**
     * @type {?}
     * @private
     */
    MaskService.prototype._renderer;
    /**
     * @type {?}
     * @private
     */
    MaskService.prototype._ngControl;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWFzay5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbmd4LW1hc2staW9uaWMvIiwic291cmNlcyI6WyJsaWIvbWFzay5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLEVBQUUsVUFBVSxFQUFFLFNBQVMsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMxRSxPQUFPLEVBQUUsTUFBTSxFQUFXLE1BQU0sVUFBVSxDQUFDO0FBQzNDLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUMzQyxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSx3QkFBd0IsQ0FBQztBQUM1RCxPQUFPLEVBQUUsU0FBUyxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFFM0M7SUFDaUMsdUNBQWtCO0lBUWpELHFCQUU0QixRQUFhLEVBQ2IsT0FBZ0IsRUFDbEMsV0FBdUIsRUFDdkIsU0FBb0IsRUFDcEIsVUFBcUI7UUFOL0IsWUFRRSxrQkFBTSxPQUFPLENBQUMsU0FtQmY7UUF6QjJCLGNBQVEsR0FBUixRQUFRLENBQUs7UUFDYixhQUFPLEdBQVAsT0FBTyxDQUFTO1FBQ2xDLGlCQUFXLEdBQVgsV0FBVyxDQUFZO1FBQ3ZCLGVBQVMsR0FBVCxTQUFTLENBQVc7UUFDcEIsZ0JBQVUsR0FBVixVQUFVLENBQVc7UUFieEIsb0JBQWMsR0FBRyxFQUFFLENBQUM7UUFDcEIsbUJBQWEsR0FBRyxLQUFLLENBQUM7UUFDdEIsbUJBQWEsR0FBRyxLQUFLLENBQUM7UUFDdEIsaUJBQVcsR0FBRyxFQUFFLENBQUM7UUFHakIsYUFBTyxHQUFHLGNBQU8sQ0FBQyxDQUFDO1FBVXhCLEtBQUksQ0FBQyxjQUFjLENBQUMsV0FBVyxDQUFDLGFBQWEsQ0FBQyxDQUFDO1FBRS9DLFVBQVUsQ0FBQztZQUNULElBQUksS0FBSSxDQUFDLFlBQVksQ0FBQyxTQUFTLEtBQUssT0FBTyxFQUFFOztvQkFDckMsT0FBTyxHQUFHLEtBQUksQ0FBQyxXQUFXLENBQUMsYUFBYSxDQUFDLGFBQWEsQ0FBQyxPQUFPLENBQUM7Z0JBQ3JFLElBQUksT0FBTyxJQUFJLElBQUksRUFBRTtvQkFDbkIsS0FBSSxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsQ0FBQztpQkFDOUI7cUJBQU07b0JBQ0wsT0FBTyxDQUFDLElBQUksQ0FDViwrRUFBK0UsQ0FDaEYsQ0FBQztpQkFDSDthQUNGO1lBRUQsS0FBSSxDQUFDLFVBQVUsQ0FBQyxZQUFZLENBQUMsU0FBUyxDQUFDLFVBQUMsS0FBYTtnQkFDbkQsS0FBSSxDQUFDLHFCQUFxQixDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ3BDLENBQUMsQ0FBQyxDQUFDO1FBQ0wsQ0FBQyxDQUFDLENBQUM7O0lBQ0wsQ0FBQzs7Ozs7SUFFTSxvQ0FBYzs7OztJQUFyQixVQUFzQixFQUFvQjtRQUN4QyxJQUFJLENBQUMsWUFBWSxHQUFHLEVBQUUsQ0FBQztJQUN6QixDQUFDOzs7Ozs7OztJQUVNLCtCQUFTOzs7Ozs7O0lBQWhCLFVBQ0UsVUFBa0IsRUFDbEIsY0FBc0IsRUFDdEIsUUFBb0IsRUFDcEIsRUFBdUI7UUFEdkIseUJBQUEsRUFBQSxZQUFvQjtRQUNwQixtQkFBQSxFQUFBLG1CQUFzQixDQUFDO1FBRXZCLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLGFBQWE7WUFDbkMsQ0FBQyxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsT0FBTyxDQUFDLFFBQVEsRUFBRSxHQUFHLENBQUM7WUFDNUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztRQUNQLElBQUksQ0FBQyxVQUFVLElBQUksSUFBSSxDQUFDLGFBQWEsRUFBRTtZQUNyQyxPQUFPLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQztTQUN2Qzs7WUFFSyxNQUFNLEdBQUcsaUJBQU0sU0FBUyxZQUFDLFVBQVUsRUFBRSxjQUFjLEVBQUUsUUFBUSxFQUFFLEVBQUUsQ0FBQztRQUN4RSxJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUVuRCxPQUFPLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUN2QyxDQUFDOzs7Ozs7SUFFTSx1Q0FBaUI7Ozs7O0lBQXhCLFVBQ0UsUUFBb0IsRUFDcEIsRUFBdUI7UUFEdkIseUJBQUEsRUFBQSxZQUFvQjtRQUNwQixtQkFBQSxFQUFBLG1CQUFzQixDQUFDOztZQUVqQixXQUFXLEdBQW9CLElBQUksQ0FBQyxTQUFTLENBQ2pELElBQUksQ0FBQyxZQUFZLENBQUMsS0FBSyxFQUN2QixJQUFJLENBQUMsY0FBYyxFQUNuQixRQUFRLEVBQ1IsRUFBRSxDQUNIO1FBQ0QsSUFBSSxDQUFDLFlBQVksQ0FBQyxLQUFLLEdBQUcsV0FBVyxDQUFDO1FBQ3RDLElBQUksSUFBSSxDQUFDLFlBQVksS0FBSyxJQUFJLENBQUMsUUFBUSxDQUFDLGFBQWEsRUFBRTtZQUNyRCxPQUFPO1NBQ1I7UUFDRCxJQUFJLENBQUMsaUJBQWlCLEVBQUUsQ0FBQztJQUMzQixDQUFDOzs7O0lBRU0scUNBQWU7OztJQUF0QjtRQUNFLElBQUksSUFBSSxDQUFDLGFBQWEsRUFBRTtZQUN0QixJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsT0FBTyxDQUFDLFFBQVEsRUFBRSxHQUFHLENBQUMsQ0FBQztTQUMvRDtJQUNILENBQUM7Ozs7SUFFTSx1Q0FBaUI7OztJQUF4QjtRQUNFLE9BQU8sQ0FBQyxHQUFHLENBQUMsc0JBQXNCLENBQUMsQ0FBQztRQUNwQyxJQUNFLElBQUksQ0FBQyxlQUFlLEtBQUssSUFBSTtZQUM3QixJQUFJLENBQUMsY0FBYyxDQUFDLE1BQU0sS0FBSyxJQUFJLENBQUMsWUFBWSxDQUFDLEtBQUssQ0FBQyxNQUFNLEVBQzdEO1lBQ0EsSUFBSSxDQUFDLFFBQVEsQ0FBQyxFQUFFLENBQUMsQ0FBQztZQUNsQixJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQztTQUM5RDtJQUNILENBQUM7Ozs7O0lBRU0sOEJBQVE7Ozs7SUFBZixVQUFnQixLQUFhO1FBQzNCLElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ2xELElBQUksQ0FBQyxVQUFVLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUMxQyxDQUFDOzs7Ozs7SUFDTSw0Q0FBc0I7Ozs7O0lBQTdCLFVBQThCLElBQVksRUFBRSxLQUF1QjtRQUNqRSxJQUFJLElBQUksQ0FBQyxZQUFZLEVBQUU7WUFDckIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFlBQVksRUFBRSxJQUFJLEVBQUUsS0FBSyxDQUFDLENBQUM7U0FDNUQ7SUFDSCxDQUFDOzs7OztJQUVNLHNDQUFnQjs7OztJQUF2QixVQUF3QixNQUFjOztZQUM5QixzQkFBc0IsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUM5QyxJQUFJLENBQUMsYUFBYSxDQUFDLE1BQU0sQ0FBQyxDQUMzQjs7WUFDRyxXQUFXLEdBQW9CLHNCQUFzQjtRQUV6RCxJQUFJLEtBQUssQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLHFCQUFxQixDQUFDLEVBQUU7WUFDN0MsV0FBVyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQzVCLHNCQUFzQixFQUN0QixJQUFJLENBQUMscUJBQXFCLENBQzNCLENBQUM7U0FDSDthQUFNLElBQUksSUFBSSxDQUFDLHFCQUFxQixFQUFFO1lBQ3JDLFdBQVcsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUM1QixzQkFBc0IsRUFDdEIsSUFBSSxDQUFDLHFCQUFxQixDQUMzQixDQUFDO1lBQ0YsV0FBVyxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUMsV0FBVyxDQUFDO1NBQ3RFO1FBRUQsT0FBTyxXQUFXLENBQUM7SUFDckIsQ0FBQzs7Ozs7OztJQUVPLGlDQUFXOzs7Ozs7SUFBbkIsVUFDRSxLQUFhLEVBQ2IsMEJBQW9DO1FBRXBDLE9BQU8sS0FBSztZQUNWLENBQUMsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQywwQkFBMEIsQ0FBQyxFQUFFLEVBQUUsQ0FBQztZQUN0RSxDQUFDLENBQUMsS0FBSyxDQUFDO0lBQ1osQ0FBQzs7Ozs7O0lBRU8sbUNBQWE7Ozs7O0lBQXJCLFVBQXNCLEtBQWE7UUFDakMsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUU7WUFDaEIsT0FBTyxLQUFLLENBQUM7U0FDZDtRQUNELE9BQU8sS0FBSyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQztJQUN4RCxDQUFDOzs7Ozs7SUFFTyxrQ0FBWTs7Ozs7SUFBcEIsVUFBcUIsS0FBYTtRQUNoQyxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssRUFBRTtZQUNmLE9BQU8sS0FBSyxDQUFDO1NBQ2Q7UUFDRCxPQUFPLEtBQUssQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsS0FBSyxFQUFFLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7SUFDdkQsQ0FBQzs7Ozs7O0lBRU8sc0NBQWdCOzs7OztJQUF4QixVQUF5QiwwQkFBb0M7UUFDM0QsT0FBTyxJQUFJLE1BQU0sQ0FDZiwwQkFBMEIsQ0FBQyxHQUFHLENBQUMsVUFBQyxJQUFZLElBQUssT0FBQSxPQUFLLElBQU0sRUFBWCxDQUFXLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQ3ZFLElBQUksQ0FDTCxDQUFDO0lBQ0osQ0FBQzs7Ozs7O0lBRU8sc0NBQWdCOzs7OztJQUF4QixVQUF5QixNQUFjO1FBQ3JDLElBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxFQUFFO1lBQ3ZCLE9BQU8sTUFBTSxDQUFDO1NBQ2Y7O1lBQ0ssTUFBTSxHQUFXLE1BQU0sQ0FBQyxNQUFNOztZQUM5QixTQUFTLEdBQVcsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsV0FBVzs7WUFDbEQsYUFBYSxHQUFHLFNBQVMsQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDO1FBRTdDLE9BQU8sTUFBTSxHQUFHLGFBQWEsQ0FBQztJQUNoQyxDQUFDOzs7Ozs7SUFFTywyQ0FBcUI7Ozs7O0lBQTdCLFVBQThCLEtBQWE7Ozs7Ozs7O1lBT25DLFFBQVEsR0FBRyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsS0FBSyxDQUFDO1FBRTdDLElBQUksSUFBSSxDQUFDLGFBQWEsS0FBSyxRQUFRLEVBQUU7WUFDbkMsT0FBTztTQUNSOztZQUVHLGlCQUFpQixHQUFXLElBQUk7UUFFcEMsK0ZBQStGO1FBQy9GLCtEQUErRDtRQUMvRCxJQUFJLElBQUksQ0FBQyxhQUFhLElBQUksSUFBSSxFQUFFOztnQkFDeEIsQ0FBQyxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsUUFBUSxFQUFFO1lBQ3ZDLGlCQUFpQixHQUFHLENBQUMsQ0FBQyxTQUFTLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUM7U0FDbEQ7UUFFRCxJQUFJLFFBQVEsS0FBSyxpQkFBaUIsRUFBRTs7Z0JBQzVCLEVBQUUsR0FBRyxRQUFRLElBQUksSUFBSSxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsUUFBUSxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUk7O2dCQUNsRCxDQUFDLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxFQUFFLEVBQUUsSUFBSSxDQUFDLGNBQWMsQ0FBQztZQUNqRCxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDO1NBQ2xCO0lBQ0gsQ0FBQzs7Z0JBbk1GLFVBQVU7Ozs7Z0RBV04sTUFBTSxTQUFDLFFBQVE7Z0RBQ2YsTUFBTSxTQUFDLE1BQU07Z0JBbEJULFVBQVU7Z0JBQXNCLFNBQVM7Z0JBSXpDLFNBQVM7O0lBc01sQixrQkFBQztDQUFBLEFBcE1ELENBQ2lDLGtCQUFrQixHQW1NbEQ7U0FuTVksV0FBVzs7O0lBQ3RCLHFDQUEyQjs7SUFDM0Isb0NBQTZCOztJQUM3QixvQ0FBNkI7O0lBQzdCLGtDQUF3Qjs7Ozs7SUFDeEIsbUNBQXVDOzs7OztJQUN2QyxvQ0FBdUM7O0lBQ3ZDLDhCQUEwQjs7Ozs7SUFHeEIsK0JBQXVDOzs7OztJQUN2Qyw4QkFBMEM7Ozs7O0lBQzFDLGtDQUErQjs7Ozs7SUFDL0IsZ0NBQTRCOzs7OztJQUM1QixpQ0FBNkIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBFbGVtZW50UmVmLCBJbmplY3QsIEluamVjdGFibGUsIFJlbmRlcmVyMiB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBjb25maWcsIElDb25maWcgfSBmcm9tICcuL2NvbmZpZyc7XHJcbmltcG9ydCB7IERPQ1VNRU5UIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcclxuaW1wb3J0IHsgTWFza0FwcGxpZXJTZXJ2aWNlIH0gZnJvbSAnLi9tYXNrLWFwcGxpZXIuc2VydmljZSc7XHJcbmltcG9ydCB7IE5nQ29udHJvbCB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcclxuXHJcbkBJbmplY3RhYmxlKClcclxuZXhwb3J0IGNsYXNzIE1hc2tTZXJ2aWNlIGV4dGVuZHMgTWFza0FwcGxpZXJTZXJ2aWNlIHtcclxuICBwdWJsaWMgbWFza0V4cHJlc3Npb24gPSAnJztcclxuICBwdWJsaWMgaXNOdW1iZXJWYWx1ZSA9IGZhbHNlO1xyXG4gIHB1YmxpYyBzaG93TWFza1R5cGVkID0gZmFsc2U7XHJcbiAgcHVibGljIG1hc2tJc1Nob3duID0gJyc7XHJcbiAgcHJpdmF0ZSBfZm9ybUVsZW1lbnQ6IEhUTUxJbnB1dEVsZW1lbnQ7XHJcbiAgcHJpdmF0ZSB1bm1hc2tlZFZhbHVlOiBzdHJpbmcgfCBudW1iZXI7XHJcbiAgcHVibGljIG9uVG91Y2ggPSAoKSA9PiB7fTtcclxuICBwdWJsaWMgY29uc3RydWN0b3IoXHJcbiAgICAvLyB0c2xpbnQ6ZGlzYWJsZS1uZXh0LWxpbmVcclxuICAgIEBJbmplY3QoRE9DVU1FTlQpIHByaXZhdGUgZG9jdW1lbnQ6IGFueSxcclxuICAgIEBJbmplY3QoY29uZmlnKSBwcm90ZWN0ZWQgX2NvbmZpZzogSUNvbmZpZyxcclxuICAgIHByaXZhdGUgX2VsZW1lbnRSZWY6IEVsZW1lbnRSZWYsXHJcbiAgICBwcml2YXRlIF9yZW5kZXJlcjogUmVuZGVyZXIyLFxyXG4gICAgcHJpdmF0ZSBfbmdDb250cm9sOiBOZ0NvbnRyb2xcclxuICApIHtcclxuICAgIHN1cGVyKF9jb25maWcpO1xyXG4gICAgdGhpcy5zZXRGb3JtRWxlbWVudChfZWxlbWVudFJlZi5uYXRpdmVFbGVtZW50KTtcclxuXHJcbiAgICBzZXRUaW1lb3V0KCgpID0+IHtcclxuICAgICAgaWYgKHRoaXMuX2Zvcm1FbGVtZW50LmxvY2FsTmFtZSAhPT0gJ2lucHV0Jykge1xyXG4gICAgICAgIGNvbnN0IGlucHV0RWwgPSB0aGlzLl9lbGVtZW50UmVmLm5hdGl2ZUVsZW1lbnQucXVlcnlTZWxlY3RvcignaW5wdXQnKTtcclxuICAgICAgICBpZiAoaW5wdXRFbCAhPSBudWxsKSB7XHJcbiAgICAgICAgICB0aGlzLnNldEZvcm1FbGVtZW50KGlucHV0RWwpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICBjb25zb2xlLndhcm4oXHJcbiAgICAgICAgICAgICdtYXNrLXNlcnZpY2U6IENvdWxkIG5vdCBmaW5kIElucHV0IEVsZW1lbnQuICBQbGVhc2UgbWFrZSBzdXJlIG9uZSBpcyBwcmVzZW50LidcclxuICAgICAgICAgICk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcblxyXG4gICAgICB0aGlzLl9uZ0NvbnRyb2wudmFsdWVDaGFuZ2VzLnN1YnNjcmliZSgodmFsdWU6IHN0cmluZykgPT4ge1xyXG4gICAgICAgIHRoaXMuX29uQ29udHJvbFZhbHVlQ2hhbmdlKHZhbHVlKTtcclxuICAgICAgfSk7XHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIHB1YmxpYyBzZXRGb3JtRWxlbWVudChlbDogSFRNTElucHV0RWxlbWVudCkge1xyXG4gICAgdGhpcy5fZm9ybUVsZW1lbnQgPSBlbDtcclxuICB9XHJcblxyXG4gIHB1YmxpYyBhcHBseU1hc2soXHJcbiAgICBpbnB1dFZhbHVlOiBzdHJpbmcsXHJcbiAgICBtYXNrRXhwcmVzc2lvbjogc3RyaW5nLFxyXG4gICAgcG9zaXRpb246IG51bWJlciA9IDAsXHJcbiAgICBjYjogRnVuY3Rpb24gPSAoKSA9PiB7fVxyXG4gICk6IHN0cmluZyB7XHJcbiAgICB0aGlzLm1hc2tJc1Nob3duID0gdGhpcy5zaG93TWFza1R5cGVkXHJcbiAgICAgID8gdGhpcy5tYXNrRXhwcmVzc2lvbi5yZXBsYWNlKC9bMC05XS9nLCAnXycpXHJcbiAgICAgIDogJyc7XHJcbiAgICBpZiAoIWlucHV0VmFsdWUgJiYgdGhpcy5zaG93TWFza1R5cGVkKSB7XHJcbiAgICAgIHJldHVybiB0aGlzLnByZWZpeCArIHRoaXMubWFza0lzU2hvd247XHJcbiAgICB9XHJcblxyXG4gICAgY29uc3QgcmVzdWx0ID0gc3VwZXIuYXBwbHlNYXNrKGlucHV0VmFsdWUsIG1hc2tFeHByZXNzaW9uLCBwb3NpdGlvbiwgY2IpO1xyXG4gICAgdGhpcy51bm1hc2tlZFZhbHVlID0gdGhpcy5nZXRVbm1hc2tlZFZhbHVlKHJlc3VsdCk7XHJcblxyXG4gICAgcmV0dXJuIHRoaXMuX2FwcGx5TWFza1Jlc3VsdChyZXN1bHQpO1xyXG4gIH1cclxuXHJcbiAgcHVibGljIGFwcGx5VmFsdWVDaGFuZ2VzKFxyXG4gICAgcG9zaXRpb246IG51bWJlciA9IDAsXHJcbiAgICBjYjogRnVuY3Rpb24gPSAoKSA9PiB7fVxyXG4gICk6IHZvaWQge1xyXG4gICAgY29uc3QgbWFza2VkSW5wdXQ6IHN0cmluZyB8IG51bWJlciA9IHRoaXMuYXBwbHlNYXNrKFxyXG4gICAgICB0aGlzLl9mb3JtRWxlbWVudC52YWx1ZSxcclxuICAgICAgdGhpcy5tYXNrRXhwcmVzc2lvbixcclxuICAgICAgcG9zaXRpb24sXHJcbiAgICAgIGNiXHJcbiAgICApO1xyXG4gICAgdGhpcy5fZm9ybUVsZW1lbnQudmFsdWUgPSBtYXNrZWRJbnB1dDtcclxuICAgIGlmICh0aGlzLl9mb3JtRWxlbWVudCA9PT0gdGhpcy5kb2N1bWVudC5hY3RpdmVFbGVtZW50KSB7XHJcbiAgICAgIHJldHVybjtcclxuICAgIH1cclxuICAgIHRoaXMuY2xlYXJJZk5vdE1hdGNoRm4oKTtcclxuICB9XHJcblxyXG4gIHB1YmxpYyBzaG93TWFza0luSW5wdXQoKTogdm9pZCB7XHJcbiAgICBpZiAodGhpcy5zaG93TWFza1R5cGVkKSB7XHJcbiAgICAgIHRoaXMubWFza0lzU2hvd24gPSB0aGlzLm1hc2tFeHByZXNzaW9uLnJlcGxhY2UoL1swLTldL2csICdfJyk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgY2xlYXJJZk5vdE1hdGNoRm4oKTogdm9pZCB7XHJcbiAgICBjb25zb2xlLmxvZygnY2xlYXItaWYtbm90LW1hdGNoZWQnKTtcclxuICAgIGlmIChcclxuICAgICAgdGhpcy5jbGVhcklmTm90TWF0Y2ggPT09IHRydWUgJiZcclxuICAgICAgdGhpcy5tYXNrRXhwcmVzc2lvbi5sZW5ndGggIT09IHRoaXMuX2Zvcm1FbGVtZW50LnZhbHVlLmxlbmd0aFxyXG4gICAgKSB7XHJcbiAgICAgIHRoaXMuc2V0VmFsdWUoJycpO1xyXG4gICAgICB0aGlzLmFwcGx5TWFzayh0aGlzLl9mb3JtRWxlbWVudC52YWx1ZSwgdGhpcy5tYXNrRXhwcmVzc2lvbik7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgc2V0VmFsdWUodmFsdWU6IHN0cmluZykge1xyXG4gICAgdGhpcy51bm1hc2tlZFZhbHVlID0gdGhpcy5nZXRVbm1hc2tlZFZhbHVlKHZhbHVlKTtcclxuICAgIHRoaXMuX25nQ29udHJvbC5jb250cm9sLnNldFZhbHVlKHZhbHVlKTtcclxuICB9XHJcbiAgcHVibGljIHNldEZvcm1FbGVtZW50UHJvcGVydHkobmFtZTogc3RyaW5nLCB2YWx1ZTogc3RyaW5nIHwgYm9vbGVhbikge1xyXG4gICAgaWYgKHRoaXMuX2Zvcm1FbGVtZW50KSB7XHJcbiAgICAgIHRoaXMuX3JlbmRlcmVyLnNldFByb3BlcnR5KHRoaXMuX2Zvcm1FbGVtZW50LCBuYW1lLCB2YWx1ZSk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgZ2V0VW5tYXNrZWRWYWx1ZShyZXN1bHQ6IHN0cmluZyk6IHN0cmluZyB8IG51bWJlciB7XHJcbiAgICBjb25zdCByZXN1bHROb1N1ZmZpeE9yUHJlZml4ID0gdGhpcy5fcmVtb3ZlU3VmaXgoXHJcbiAgICAgIHRoaXMuX3JlbW92ZVByZWZpeChyZXN1bHQpXHJcbiAgICApO1xyXG4gICAgbGV0IGNoYW5nZVZhbHVlOiBzdHJpbmcgfCBudW1iZXIgPSByZXN1bHROb1N1ZmZpeE9yUHJlZml4O1xyXG5cclxuICAgIGlmIChBcnJheS5pc0FycmF5KHRoaXMuZHJvcFNwZWNpYWxDaGFyYWN0ZXJzKSkge1xyXG4gICAgICBjaGFuZ2VWYWx1ZSA9IHRoaXMuX3JlbW92ZU1hc2soXHJcbiAgICAgICAgcmVzdWx0Tm9TdWZmaXhPclByZWZpeCxcclxuICAgICAgICB0aGlzLmRyb3BTcGVjaWFsQ2hhcmFjdGVyc1xyXG4gICAgICApO1xyXG4gICAgfSBlbHNlIGlmICh0aGlzLmRyb3BTcGVjaWFsQ2hhcmFjdGVycykge1xyXG4gICAgICBjaGFuZ2VWYWx1ZSA9IHRoaXMuX3JlbW92ZU1hc2soXHJcbiAgICAgICAgcmVzdWx0Tm9TdWZmaXhPclByZWZpeCxcclxuICAgICAgICB0aGlzLm1hc2tTcGVjaWFsQ2hhcmFjdGVyc1xyXG4gICAgICApO1xyXG4gICAgICBjaGFuZ2VWYWx1ZSA9IHRoaXMuaXNOdW1iZXJWYWx1ZSA/IE51bWJlcihjaGFuZ2VWYWx1ZSkgOiBjaGFuZ2VWYWx1ZTtcclxuICAgIH1cclxuXHJcbiAgICByZXR1cm4gY2hhbmdlVmFsdWU7XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIF9yZW1vdmVNYXNrKFxyXG4gICAgdmFsdWU6IHN0cmluZyxcclxuICAgIHNwZWNpYWxDaGFyYWN0ZXJzRm9yUmVtb3ZlOiBzdHJpbmdbXVxyXG4gICk6IHN0cmluZyB7XHJcbiAgICByZXR1cm4gdmFsdWVcclxuICAgICAgPyB2YWx1ZS5yZXBsYWNlKHRoaXMuX3JlZ0V4cEZvclJlbW92ZShzcGVjaWFsQ2hhcmFjdGVyc0ZvclJlbW92ZSksICcnKVxyXG4gICAgICA6IHZhbHVlO1xyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBfcmVtb3ZlUHJlZml4KHZhbHVlOiBzdHJpbmcpOiBzdHJpbmcge1xyXG4gICAgaWYgKCF0aGlzLnByZWZpeCkge1xyXG4gICAgICByZXR1cm4gdmFsdWU7XHJcbiAgICB9XHJcbiAgICByZXR1cm4gdmFsdWUgPyB2YWx1ZS5yZXBsYWNlKHRoaXMucHJlZml4LCAnJykgOiB2YWx1ZTtcclxuICB9XHJcblxyXG4gIHByaXZhdGUgX3JlbW92ZVN1Zml4KHZhbHVlOiBzdHJpbmcpOiBzdHJpbmcge1xyXG4gICAgaWYgKCF0aGlzLnN1Zml4KSB7XHJcbiAgICAgIHJldHVybiB2YWx1ZTtcclxuICAgIH1cclxuICAgIHJldHVybiB2YWx1ZSA/IHZhbHVlLnJlcGxhY2UodGhpcy5zdWZpeCwgJycpIDogdmFsdWU7XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIF9yZWdFeHBGb3JSZW1vdmUoc3BlY2lhbENoYXJhY3RlcnNGb3JSZW1vdmU6IHN0cmluZ1tdKTogUmVnRXhwIHtcclxuICAgIHJldHVybiBuZXcgUmVnRXhwKFxyXG4gICAgICBzcGVjaWFsQ2hhcmFjdGVyc0ZvclJlbW92ZS5tYXAoKGl0ZW06IHN0cmluZykgPT4gYFxcXFwke2l0ZW19YCkuam9pbignfCcpLFxyXG4gICAgICAnZ2knXHJcbiAgICApO1xyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBfYXBwbHlNYXNrUmVzdWx0KHJlc3VsdDogc3RyaW5nKSB7XHJcbiAgICBpZiAoIXRoaXMuc2hvd01hc2tUeXBlZCkge1xyXG4gICAgICByZXR1cm4gcmVzdWx0O1xyXG4gICAgfVxyXG4gICAgY29uc3QgcmVzTGVuOiBudW1iZXIgPSByZXN1bHQubGVuZ3RoO1xyXG4gICAgY29uc3QgcHJlZk5tYXNrOiBzdHJpbmcgPSB0aGlzLnByZWZpeCArIHRoaXMubWFza0lzU2hvd247XHJcbiAgICBjb25zdCBpZk1hc2tJc1Nob3duID0gcHJlZk5tYXNrLnNsaWNlKHJlc0xlbik7XHJcblxyXG4gICAgcmV0dXJuIHJlc3VsdCArIGlmTWFza0lzU2hvd247XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIF9vbkNvbnRyb2xWYWx1ZUNoYW5nZSh2YWx1ZTogc3RyaW5nKSB7XHJcbiAgICAvKlxyXG4gICAgICBCZWNhdXNlIHdlIGFyZSBubyBsb25nZXIgd29ya2luZyB3aXRoIHRoZSBDb250cm9sVmFsdWVBY2Nlc3NvciAoc2luY2UgaXQgZG9lc24ndCBwbGF5IG5pY2Ugd2l0aCBJb25pYykuXHJcbiAgICAgIFdlIG5lZWQgbG9naWMgaGVyZSB0byB0cmFjayBjaGFuZ2VzIG1hZGUgcHJvZ3JhbW1hdGljYWxseSB0byB0aGUgZm9ybSB2YWx1ZS4gIFNwZWNpZmljYWxseSBjaGFuZ2VzXHJcbiAgICAgIGRvbmUgT1VUU0lERSBvZiB0aGUgbWFzay4gU2luY2UgY2hhbmdlcyBkb25lIGluc2lkZSB0aGUgbWFzayBtYXkgYWxzbyBmaXJlIG9mZiB0aGlzIG1ldGhvZFxyXG4gICAgICB3ZSBuZWVkIHRvIGRvIHNvbWUgaml1IGppdHN1IHRvIGVuc3VyZSB3ZSBhcmUgaWdub3JpbmcgdGhvc2UgY2hhbmdlcy5cclxuICAgICovXHJcbiAgICBjb25zdCBuZXdWYWx1ZSA9IHRoaXMuZ2V0VW5tYXNrZWRWYWx1ZSh2YWx1ZSk7XHJcblxyXG4gICAgaWYgKHRoaXMudW5tYXNrZWRWYWx1ZSA9PT0gbmV3VmFsdWUpIHtcclxuICAgICAgcmV0dXJuO1xyXG4gICAgfVxyXG5cclxuICAgIGxldCB1bm1hc2tlZFN1YnN0cmluZzogc3RyaW5nID0gbnVsbDtcclxuXHJcbiAgICAvLyBUaGlzIG1ldGhvZCAodmFsdWUgY2hhbmdlKSBmaXJlcyBvZmYgYmVmb3JlIGEgS2V5ZG93biBvciBJbnB1dCBldmVudCwgc28gd2UgbmVlZCB0byBzdWJ0cmFjdFxyXG4gICAgLy8gdGhlIGxhdGVzdCBjaGFuZ2UgYW5kIGNvbXBhcmUgdG8gb3VyIHByZXZpb3VzIChrbm93bikgdmFsdWUuXHJcbiAgICBpZiAodGhpcy51bm1hc2tlZFZhbHVlICE9IG51bGwpIHtcclxuICAgICAgY29uc3QgdiA9IHRoaXMudW5tYXNrZWRWYWx1ZS50b1N0cmluZygpO1xyXG4gICAgICB1bm1hc2tlZFN1YnN0cmluZyA9IHYuc3Vic3RyaW5nKDAsIHYubGVuZ3RoIC0gMSk7XHJcbiAgICB9XHJcblxyXG4gICAgaWYgKG5ld1ZhbHVlICE9PSB1bm1hc2tlZFN1YnN0cmluZykge1xyXG4gICAgICBjb25zdCBudiA9IG5ld1ZhbHVlICE9IG51bGwgPyBuZXdWYWx1ZS50b1N0cmluZygpIDogbnVsbDtcclxuICAgICAgY29uc3QgdiA9IHRoaXMuYXBwbHlNYXNrKG52LCB0aGlzLm1hc2tFeHByZXNzaW9uKTtcclxuICAgICAgdGhpcy5zZXRWYWx1ZSh2KTtcclxuICAgIH1cclxuICB9XHJcbn1cclxuIl19