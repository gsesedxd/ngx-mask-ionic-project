/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
export { config, NEW_CONFIG, INITIAL_CONFIG, initialConfig } from './lib/config';
export { MaskDirective } from './lib/mask.directive';
export { MaskService } from './lib/mask.service';
export { _configFactory, NgxMaskIonicModule } from './lib/ngx-mask-ionic.module';
export { MaskPipe } from './lib/mask.pipe';
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHVibGljX2FwaS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL25neC1tYXNrLWlvbmljLyIsInNvdXJjZXMiOlsicHVibGljX2FwaS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsa0VBQWMsY0FBYyxDQUFDO0FBQzdCLDhCQUFjLHNCQUFzQixDQUFDO0FBQ3JDLDRCQUFjLG9CQUFvQixDQUFDO0FBQ25DLG1EQUFjLDZCQUE2QixDQUFDO0FBQzVDLHlCQUFjLGlCQUFpQixDQUFDIiwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0ICogZnJvbSAnLi9saWIvY29uZmlnJztcbmV4cG9ydCAqIGZyb20gJy4vbGliL21hc2suZGlyZWN0aXZlJztcbmV4cG9ydCAqIGZyb20gJy4vbGliL21hc2suc2VydmljZSc7XG5leHBvcnQgKiBmcm9tICcuL2xpYi9uZ3gtbWFzay1pb25pYy5tb2R1bGUnO1xuZXhwb3J0ICogZnJvbSAnLi9saWIvbWFzay5waXBlJztcbiJdfQ==